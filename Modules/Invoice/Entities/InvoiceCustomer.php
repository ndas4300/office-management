<?php

namespace Modules\Invoice\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceCustomer extends Model
{
    use HasFactory;
 
    protected $fillable = [
        'name',
        'address',
        'mobile',
        'email',
        'contact_person',
        'contact_person_email',
        'contact_person_mobile',
    ];

    public function invoice()
    {
        return $this->morphOne(Invoice::class,'people');
    }
}
