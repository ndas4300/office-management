<?php

namespace Modules\Invoice\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'category_id',
        'price',
        'subscription_period',
        'is_active'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function sale()
    {
        return $this->morphOne(Sale::class,'salable');
    }
    public function purchase()
    {
        return $this->morphOne(Purchase::class,'purchasable');
    }

}
