<?php

namespace Modules\Invoice\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasFactory;

    protected $fillable = [
        'salable_id',
        'salable_type',
        'invoice_id',
        'price',
        'qty',
        'tax',
        'discount',
        'total',
        'billing_cycle_start',
        'billing_cycle_end',
        'service_expired',
    ];

    public function salable()
    {
        return $this->morphTo();
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }
}
