<?php

namespace Modules\Invoice\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    use HasFactory;

    protected $fillable = [
        'purchasable_id',
        'purchasable_type',
        'invoice_id',
        'price',
        'qty',
        'tax',
        'discount',
        'total',
        'billing_cycle_start',
        'billing_cycle_end',
        'service_expired',
    ];

    public function purchasable()
    {
        return $this->morphTo();
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }
}
