<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::prefix('invoice')->group(function() {
//    Route::get('/', 'InvoiceController@index');
//});

use Modules\Invoice\Http\Controllers\InvoiceCustomerController;
use Modules\Invoice\Http\Controllers\InvoiceSupplierController;

Route::prefix('admin')->middleware(['auth', 'verified'])->group(function () {
        // Invoice
        Route::resource('category', CategoryController::class);
        Route::resource('product', ProductController::class);
        Route::resource('service', ServiceController::class);
        Route::resource('invoice', InvoiceController::class)->except('delete', 'update');
        Route::post('invoice-search', [InvoiceController::class, 'search'])->name('invoice.search');
        // Sale
        Route::resource('sale', SaleController::class);
        Route::get('sale/getProduct/{id}', [SaleController::class, 'getProduct'])->name('sale.getProduct');
        Route::get('sale/getService/{id}', [SaleController::class, 'getService'])->name('sale.getService');
        Route::post('sale/addToCart', [SaleController::class, 'addToCart'])->name('sale.addToCart');
        Route::get('get-cart-sale', [SaleController::class, 'getCart'])->name('sale.getCart');
        Route::patch('update-cart-sale', [SaleController::class, 'updateCart'])->name('sale.updateCart');
        Route::post('remove-sale', [SaleController::class, 'remove'])->name('sale.remove');
        Route::post('checkout-sale', [SaleController::class, 'checkout'])->name('sale.checkout');
        Route::get('sale/invoice/{id}', [SaleController::class, 'saleInvoice'])->name('sale.saleInvoice');
        Route::get('clear-sale-cart', [SaleController::class, 'clearCart'])->name('sale.clearCart');
        // Sale

        // Sale
        Route::resource('purchase', PurchaseController::class);
        Route::get('purchase/getProduct/{id}', [PurchaseController::class, 'getProduct'])->name('purchase.getProduct');
        Route::get('purchase/getService/{id}', [PurchaseController::class, 'getService'])->name('purchase.getService');
        Route::post('purchase/addToCart', [PurchaseController::class, 'addToCart'])->name('purchase.addToCart');
        Route::get('get-cart-purchase', [PurchaseController::class, 'getCart'])->name('purchase.getCart');
        Route::patch('update-cart-purchase', [PurchaseController::class, 'updateCart'])->name('purchase.updateCart');
        Route::post('remove-purchase', [PurchaseController::class, 'remove'])->name('purchase.remove');
        Route::post('checkout-purchase', [PurchaseController::class, 'checkout'])->name('purchase.checkout');
        Route::get('purchase/invoice/{id}', [PurchaseController::class, 'purchaseInvoice'])->name('purchase.purchaseInvoice');
        Route::get('clear-purchase-cart', [PurchaseController::class, 'clearCart'])->name('purchase.clearCart');
        // Sale

        // Invoice Customer
        Route::get('invoice-customer', [InvoiceCustomerController::class, 'index'])->name('invoiceCustomer.index');
        Route::post('invoice-customer', [InvoiceCustomerController::class, 'store'])->name('invoiceCustomer.store');
        Route::put('invoice-customer/{invoiceCustomer}', [InvoiceCustomerController::class, 'update'])->name('invoiceCustomer.update');
        Route::delete('invoice-customer/{invoiceCustomer}', [InvoiceCustomerController::class, 'destroy'])->name('invoiceCustomer.destroy');
        Route::get('invoice-customer/create', [InvoiceCustomerController::class, 'create'])->name('invoiceCustomer.create');
        Route::get('invoice-customer/{invoiceCustomer}/edit', [InvoiceCustomerController::class, 'edit'])->name('invoiceCustomer.edit');
        // Invoice Customer

        // Invoice Supplier
        Route::get('invoice-supplier', [InvoiceSupplierController::class, 'index'])->name('invoiceSupplier.index');
        Route::post('invoice-supplier', [InvoiceSupplierController::class, 'store'])->name('invoiceSupplier.store');
        Route::put('invoice-supplier/{invoiceSupplier}', [InvoiceSupplierController::class, 'update'])->name('invoiceSupplier.update');
        Route::delete('invoice-supplier/{invoiceSupplier}', [InvoiceSupplierController::class, 'destroy'])->name('invoiceSupplier.destroy');
        Route::get('invoice-supplier/create', [InvoiceSupplierController::class, 'create'])->name('invoiceSupplier.create');
        Route::get('invoice-supplier/{invoiceSupplier}/edit', [InvoiceSupplierController::class, 'edit'])->name('invoiceSupplier.edit');
        // Invoice Supplier

        // Invoice
    });