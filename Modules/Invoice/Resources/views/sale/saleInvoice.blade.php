@extends('invoice::layouts.master')

@section('title', 'Sale|WPM')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Invoice</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Invoice</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="invoice p-3 mb-3">

                        <div class="row">
                            <div class="col-10">
                                <h4>
                                    {{-- <i class="fas fa-globe"></i> Web Point Ltd. --}}
                                </h4>
                            </div>
                            <div class="col-2">
                                <h4><small>Date: {{ $invoice->created_at->format('d-m-Y') }} </small></h4>

                            </div>

                        </div>

                        <div class="row d-flex justify-content-between invoice-info">
                            <div class="col-sm-4 invoice-col">
                                To
                                <address>
                                    <strong>{{ $invoice->people->name }}</strong><br>
                                    {{ $invoice->people->address }}<br>
                                    Phone: {{ $invoice->people->mobile }}<br>
                                    Email: {{ $invoice->people->email }}
                                </address>
                            </div>

                            <div class="col-2 invoice-col">

                                <b>Invoice {{ $invoice->invoice_no }}</b><br>
                                <br>
                                {{-- <b>Order ID:</b> 4F3S8J<br>
                                    <b>Payment Due:</b> 2/22/2014<br>
                                    <b>Account:</b> 968-34567 --}}

                            </div>

                        </div>


                        <div class="row">
                            <div class="col-12 table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            {{-- <th>Qty</th> --}}
                                            <th>Product</th>
                                            <th>Quantity</th>
                                            <th>Rate</th>
                                            {{-- <th>Tax</th> --}}
                                            <th>Discount</th>
                                            <th>Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($invoice->sales as $key => $sale)
                                            <tr>
                                                {{-- <td>1</td> --}}
                                                <td>{{ $sale->salable->name }}</td>
                                                <td>{{ $sale->qty }}</td>
                                                <td>{{ $sale->price }}</td>
                                                {{-- <td>
                                                    @if ($sale->tax == null || $sale->tax == 0)
                                                        $ 0.00
                                                    @else
                                                    ${{$sale->tax}}
                                                    @endif
                                                </td> --}}
                                                <td>
                                                    @if ($sale->discount == null || $sale->discount == 0)
                                                         0.00
                                                    @else
                                                        ${{ $sale->discount }}
                                                    @endif
                                                </td>
                                                <td>{{ $sale->total }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-6">
                                {{-- <p class="lead">Payment Methods:</p>
                                <img src="{{ asset('lte') }}/dist/img/credit/visa.png" alt="Visa">
                                <img src="{{ asset('lte') }}/dist/img/credit/mastercard.png" alt="Mastercard">
                                <img src="{{ asset('lte') }}/dist/img/credit/american-express.png" alt="American Express">
                                <img src="{{ asset('lte') }}/dist/img/credit/paypal2.png" alt="Paypal">
                                <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
                                    Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya
                                    handango imeem
                                    plugg
                                    dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                                </p> --}}
                            </div>

                            <div class="col-6">
{{--                                <p class="lead">Amount Due 2/22/2014</p>--}}
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <th style="width:50%">Subtotal:</th>
                                                <td>{{ $invoice->sub_total }}</td>
                                            </tr>
                                            <tr>
                                                <th>Tax </th>
                                                <td>{{ $invoice->tax }}</td>
                                            </tr>
                                            <tr>
                                                <th>Discount:</th>
                                                <td>{{ $invoice->discount }}</td>
                                            </tr>
                                            <tr>
                                                <th>Total:</th>
                                                <td>{{ $invoice->total }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>


                        <div class="row no-print">
                            <div class="col-12">
                                <button class="btn btn-default" id="print"><i class="fas fa-print"></i> Print</button>
                                {{-- <a href="invoice-print.html" rel="noopener" target="_blank" class="btn btn-default"><i
                                            class="fas fa-print"></i> Print</a> --}}
                                <button type="button" class="btn btn-success float-right"><i
                                        class="far fa-credit-card"></i> Submit
                                    Payment
                                </button>
                                {{-- <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                                        <i class="fas fa-download"></i> Generate PDF
                                    </button> --}}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection

@push('script')
    <script>
        $('#print').click(function() {
            // console.log('kjhdaskljfh');
            window.print();
        });
    </script>
@endpush

@push('style')
    <style>
        @media print {
            body {
                font-size: 30px;
            }

        }
    </style>
@endpush
