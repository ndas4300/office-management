{{-- {{dd(session()->all())}} --}}
<table class="table table-hover table-condensed">
    <thead>
        <tr>
            <th >Product</th>
            <th >Price</th>
            <th >Quantity</th>
            <th >Discount</th>
            <th  class="text-center">Subtotal</th>
            <th ></th>
        </tr>
    </thead>
    <tbody>
        @php $total = 0 @endphp
        @if (session('cart'))
            @foreach (session('cart') as $id => $details)
                @php $total += $details['rate'] * $details['qty'] @endphp
                <tr data-id="{{ $id }}">
                    <td data-th="Product">
                        <div class="row">
                            {{-- <div class="col-sm-3 hidden-xs"><img src="{{ $details['image'] }}"
                                    width="100" height="100" class="img-responsive" /></div> --}}
                            <div class="col-sm-9">
                                <h5 class="nomargin">{{ $details['name'] }}</h5>
                            </div>
                        </div>
                    </td>
                    <td data-th="rate">${{ $details['rate'] }}</td>
                    <td data-th="qty">{{ $details['qty'] }}
                    <td data-th="discount">${{ $details['discount'] }}
                        {{-- <input type="number"  id="updateCart"  value=""
                            class="form-control qty update-cart" /> --}}
                    </td>
                    <td data-th="Subtotal" class="text-center subtotal">
                        ${{ $details['rate'] * $details['qty'] - $details['discount'] }}</td>
                    <td class="actions" data-th="{{ $id }}">
                        <button class="btn btn-danger btn-sm remove-from-cart" data-id="{{ $id }}"
                            id="remove"><i class="fas fa-trash"></i></button>
                    </td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>
<br>
<br>
<br>
<br>
<br>

<div class="row d-flex justify-content-end  ">

    <div class="col-md-6">

        <div class="col-md-12">
            <table class="table table-bordered table-hover" id="tab_logic_total">
                <tbody>
                    @php $total = 0 @endphp
                    @if (session('cart'))
                        @foreach (session('cart') as $id => $details)
                            @php $total += (($details['rate'] * $details['qty'])-$details['discount']) @endphp
                        @endforeach
                    @endif
                    <tr>
                        <th class="text-center">Total</th>
                        <td class="text-center"><input type="number" name='total' placeholder='0.00'
                                value="{{ $total }}" class="form-control" id="total" readonly /></td>
                    </tr>
                    <tr>
                        <th class="text-center">Tax</th>
                        <td class="text-center">
                            <div class="input-group mb-2 mb-sm-0">
                                <input type="text" class="form-control" id="tax" placeholder="0">
                                {{-- <div class="input-group-addon">%</div> --}}
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-center">Discount</th>
                        <td class="text-center"><input type="text" name='invoice_discount' id="invoice_discount"
                                placeholder='0.00'  value="0"  class="form-control" /></td>
                    </tr>
                    <tr>
                        <th class="text-center">Grand Total</th>
                        <td class="text-center"><input type="number" name='grand_total' value="{{ $total }}"
                                id="grand_total" placeholder='0.00' class="form-control" readonly /></td>
                    </tr>

                </tbody>

            </table>

        <div class="text-right m-2">
            <button id="clear_cart" type="button" class="btn btn-warning"> <i
                    class="fas fa-eraser"></i> Clear Cart</button>
        <button class="btn btn-success" id="checkout"><i class="fas fa-shopping-cart"></i>
            Checkout</button>
        </div>
        </div>

    </div>
</div>

<script>

        $('#clear_cart').click(function () {
            $.ajax({
                url: "{{route('sale.clearCart')}}",
                type: "GET",
                success : function(e){
                    $("#cart").html(e);
                }
            });
        });

    $('#checkout').click(function () {
        var total = $('#total').val();
        var tax = $('#tax').val();
        var invoice_discount = $('#invoice_discount').val();
        var grand_total = $('#grand_total').val();
        var people_id = $('#customer').val();
        // var people_type = 'App\Models\Invoice\InvoiceCustomer';
        $.ajax({
            url: "{{route('sale.checkout')}}",
            type: "POST",
            data:{
                _token: "{{csrf_token()}}",
                // people_type:        people_type,
                people_id:          people_id,
                total :             total,
                tax :               tax,
                invoice_discount :  invoice_discount,
                grand_total :       grand_total,
            },
            success: (function(data){

                // console.log(data);

                window.location=data;
            }),
            error: function (error) {
                console.log(error);
            }
        });
    });





    $(".remove-from-cart").click(function(e) {
        e.preventDefault();

        var ele = $(this).data("id");
        console.log(ele);
        // $(this).parents('tr').remove();

        // if (confirm("Are you sure want to remove?")) {
        $.ajax({
            url: '{{ route('sale.remove') }}',
            method: "post",
            data: {
                _token: '{{ csrf_token() }}',
                id: ele
            }
        }).done(function(e) {
            console.log(e);
            $("#cart").html(e);
        });
        // }
    });

    $('#tax').keyup(function() {
        var tax = $(this).val();
        var total = $('#total').val();
        var invoice_discount = $('#invoice_discount').val();
        var grand_total = parseFloat(total) + parseFloat(tax) - parseInt(invoice_discount);
        $('#grand_total').val(grand_total);

    });
    $('#invoice_discount').keyup(function() {
        var invoice_discount = $(this).val();
        console.log(invoice_discount);
        var total = $('#total').val();
        var tax = $('#tax').val();
        var grand_total = parseInt(total) - parseInt(invoice_discount) + parseFloat(tax);
        $('#grand_total').val(grand_total);

    });
</script>
