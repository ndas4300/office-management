<div class="row d-flex justify-content-center">
    <div class="col-md-4">
            <div class="form-group">
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    {{ Form::label('name', 'Name*:') }}
                    {{ Form::text('name', null, ['placeholder' => 'Enter Invoice Supplier Name', 'class' => 'form-control', 'required' => 'required']) }}
                    <small class="text-danger">{{ $errors->first('name') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                {{ Form::label('mobile', 'Phone Number*:') }}
                {{ Form::text('mobile', null, ['class' => 'form-control', 'placeholder' => 'Enter Phone Number', 'required' => 'required']) }}
                <small class="text-danger">{{ $errors->first('mobile') }}</small>
            </div>


            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                {{ Form::label('address', 'Address*:') }}
                {{ Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Enter Address', 'required' => 'required']) }}
                <small class="text-danger">{{ $errors->first('address') }}</small>
            </div>


            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                {{ Form::label('email', 'Email address*:') }}
                {{ Form::email('email', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'eg: foo@bar.com']) }}
                <small class="text-danger">{{ $errors->first('email') }}</small>
            </div>

    </div>
    <div class="col-md-4">

            <div class="form-group{{ $errors->has('contact_person') ? ' has-error' : '' }}">
                {{ Form::label('contact_person', 'Contact Person') }}
                {{ Form::text('contact_person', null, ['class' => 'form-control', 'placeholder' => 'Enter Contact Person Name']) }}
                <small class="text-danger">{{ $errors->first('contact_person') }}</small>
            </div>

            <div class="form-group{{ $errors->has('contact_person_mobile') ? ' has-error' : '' }}">
                {{ Form::label('contact_person_mobile', 'Contact Person Mobile Number') }}
                {{ Form::text('contact_person_mobile', null, ['class' => 'form-control', 'placeholder' =>'Enter Contact Person Mobile Number']) }}
                <small class="text-danger">{{ $errors->first('contact_person_mobile') }}</small>
            </div>



            <div class="form-group{{ $errors->has('contact_person_email') ? ' has-error' : '' }}">
                {{ Form::label('contact_person_email', 'Contact Person Email address') }}
                {{ Form::email('contact_person_email', null, ['class' => 'form-control', 'placeholder' => 'eg: foo@bar.com']) }}
                <small class="text-danger">{{ $errors->first('contact_person_email') }}</small>
            </div>

    </div>
</div>

<div class="row  d-flex justify-content-center">
    <div class="col-md-3 ">
        <button type="submit" class="btn btn-success mt-2 ml-2" > Create</button>
        <a href="{{ URL::previous() }}" class="btn btn-danger mt-2 ml-2" >Cancel</a>
    </div>
</div>
