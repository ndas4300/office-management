<div class="row">
    <div class="col-4"></div>
    <div class="col-4">
        <div class="form-group">
            <label for="name">Name*:</label>
            {{ Form::text('name',null,['class'=>'form-control','placeholder'=>'Enter Product Name Here:']) }}
        <small class="text-danger">{{ $errors->first('name') }}</small>
        </div>
    </div>
    <div class="col-4"></div>
    <div class="col-4"></div>
    <div class="col-4">
        <div class="form-group">
            <label for="price">Price*:</label>
            {{ Form::text('price',null,['class'=>'form-control','placeholder'=>'Enter Price Here:']) }}
        <small class="text-danger">{{ $errors->first('price') }}</small>
        </div>
    </div>
    <div class="col-4"></div>
    <div class="col-4"></div>
    <div class="col-4">
        <div class="form-group">
            <label for="category_id">Category:</label>
            {{ Form::select('category_id',$categories,null,['class'=>'form-control','placeholder'=>'Select Category Here:']) }}
        <small class="text-danger">{{ $errors->first('category_id') }}</small>
        </div>
    </div>
    <div class="col-4"></div>
    <div class="col-4"></div>
    <div class="col-4">
        <div class="form-group">
            <label for="is_active">Is active*:</label>
            <input type="checkbox" name="is_active" value="1" class="flat-red" @isset($products->is_active)
                {{ $products->is_active == null ? '' : 'checked' }}
            @endisset  checked>
        </div>  
    </div>
    <div class="col-4"></div>
</div>
<div class="row">
    <div class="col-4"></div>
    <div class="col-4">
        <button type="submit" class="btn btn-success" > Create</button>
        <a href="{{ URL::previous() }}" class="btn btn-danger">Cancel</a>
    </div>
    <div class="col-4"></div>
</div>

