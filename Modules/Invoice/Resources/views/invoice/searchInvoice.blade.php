<table id="example2" class="table table-bordered table-hover text-center">
    <thead>
        <tr>
            <th>{{ __('Sl') }} </th>
            <th>{{ __('Invoice No.') }} </th>
            <th>{{ __('Total') }} </th>
            <th>{{ __('Invoice Type') }} </th>
            <th>{{ __('Action') }} </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($invoices as $key => $data)
            <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $data->invoice_no }}</td>
                <td>{{ $data->total }} </td>
                <td> <span class="badge badge-secondary">{{ $data->model }}</span> </td>
                <td>
                    {{ Form::open([
                        'route' => ['invoice.destroy', $data->id],
                        'method' => 'delete',
                        'onsubmit' => 'return
                                                                                                                                                            confirmDelete()',
                    ]) }}
                    <a href="{{ route('invoice.show', $data->id) }}"
                        class="btn btn-success btn-sm"><i class="fas fa-eye"></i></a>
                    <button type="submit" class="btn btn-danger btn-sm"><i
                            class="fas fa-trash"></i></button>
                    {{ Form::close() }}
                </td>
            </tr>
        @endforeach
    </tbody>
</table>