@extends('invoice::layouts.master')

@section('title', 'Invoice|WPM')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Invoice') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">{{ __('Settings') }}</a></li>
                        <li class="breadcrumb-item active">{{ __('categorys') }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <!-- ***/Chart of Accounts page inner Content Start-->
    <section class="content">
        <div class="container-fluid">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mx-auto pull-right">
                                <div class="input-group mb-3">
                                    {{ Form::text('q',null,['class'=>'form-control','id'=>'q','placeholder'=>__('Enter Invoice No./ Invoice Total')]) }}
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon2"><span
                                                class="fas fa-search"></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="card">
                        <div class="card-body" id="card-body">
                            <table id="example2" class="table table-bordered table-hover text-center">
                                <thead>
                                    <tr>
                                        <th>{{ __('Sl') }} </th>
                                        <th>{{ __('Invoice No.') }} </th>
                                        <th>{{ __('Total') }} </th>
                                        <th>{{ __('Invoice Type') }} </th>
                                        <th>{{ __('Action') }} </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($invoices as $key => $data)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{ $data->invoice_no }}</td>
                                            <td>{{ $data->total }} </td>
                                            <td> <span class="badge badge-secondary">{{ $data->model }}</span> </td>
                                            <td>
                                                {{ Form::open([
                                                    'route' => ['invoice.destroy', $data->id],
                                                    'method' => 'delete',
                                                    'onsubmit' => 'return
                                                                                                                                                                                        confirmDelete()',
                                                ]) }}
                                                <a href="{{ route('invoice.show', $data->id) }}"
                                                    class="btn btn-success btn-sm"><i class="fas fa-eye"></i></a>
                                                <button type="submit" class="btn btn-danger btn-sm"><i
                                                        class="fas fa-trash"></i></button>
                                                {{ Form::close() }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="row" style="margin-top: 10px">
                                <div class="col-sm-12 col-md-9">
                                    {{-- <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">
                                    Showing 0 to 0 of 0 entries</div> --}}
                                </div>
                                <div class="col-sm-12 col-md-3">
                                    {{ $invoices->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script>
        function confirmDelete() {
            let x = confirm('Are you sure you want to delete this account head?');
            return !!x;
        }
    </script>
    <script>
        $(function() {
            $("#example1").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>       
    <script>
    $("#q").keyup(function () {
        var q = $(this).val();
        console.log(q);
        var csrf = "{{ @csrf_token() }}"
        $.ajax({
            url: "{{ route('invoice.search') }}",
            data: {
                _token: csrf,
                q: q
            },
            type: "POST",
        }).done(function (e) {
            console.log(e);
            $("#card-body").html(e);
        })
    })
</script>
@stop
