<div class="row d-flex justify-content-between">
    <div class="col-md-4">
        <div class="card card-body">
            <div class="form-group{{ $errors->has('product') ? ' has-error' : '' }} m-0">
                {{ Form::label('product', 'Product*') }}
                {{ Form::select('product', $products, null, ['class' => 'form-control', 'placeholder' => 'Select Product', 'id' => 'product']) }}
                <small class="text-danger">{{ $errors->first('product') }}</small>
            </div>
            {{ Form::hidden('model', 'Modules\Invoice\Entities\Product', ['id' => 'model']) }}
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('rate') ? ' has-error' : '' }} m-0">
                        {{ Form::label('rate', 'Rate*') }}
                        {{ Form::text('rate', null, ['class' => 'form-control', 'id' => 'rate']) }}
                        <small class="text-danger">{{ $errors->first('rate') }}</small>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('qty') ? ' has-error' : '' }} m-0">
                        {{ Form::label('qty', 'Quantity*') }}
                        {{ Form::text('qty', null, ['class' => 'form-control', 'id' => 'qty', 'placeholder' => '0']) }}
                        <small class="text-danger">{{ $errors->first('qty') }}</small>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('discount') ? ' has-error' : '' }} m-0">
                        {{ Form::label('discount', 'Discount') }}
                        {{ Form::text('discount', null, ['class' => 'form-control', 'id' => 'discount']) }}
                        <small class="text-danger">{{ $errors->first('discount') }}</small>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('sub_total') ? ' has-error' : '' }} m-0">
                        {{ Form::label('sub_total', 'Sub Total*') }}
                        {{ Form::text('sub_total', null, ['class' => 'form-control', 'id' => 'sub_total', 'placeholder' => 'Enter Sub Total', 'readonly' => 'readonly']) }}
                        <small class="text-danger">{{ $errors->first('sub_total') }}</small>
                    </div>
                </div>
            </div>
            <div class="form-group mt-5">
                <p class="btn-holder">
                    <button class="btn btn-primary btn-block text-center" id="addToCart"
                            type="submit"> Add to cart
                    </button>
                </p>
            </div>

        </div>
        <div class="card card-body">
            <div class="form-group{{ $errors->has('service') ? ' has-error' : '' }} m-0">
                {{ Form::label('service', 'Service*') }}
                {{ Form::select('service', $services, null, ['class' => 'form-control', 'placeholder' => 'Select Product', 'id' => 'service']) }}
                <small class="text-danger">{{ $errors->first('service') }}</small>
            </div>
            {{ Form::hidden('model', 'Modules\Invoice\Entities\Service', ['id' => 'model1']) }}

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('rate') ? ' has-error' : '' }} m-0">
                        {{ Form::label('rate', 'Rate*') }}
                        {{ Form::text('rate', null, ['class' => 'form-control', 'id' => 'rate1']) }}
                        <small class="text-danger">{{ $errors->first('rate') }}</small>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('qty') ? ' has-error' : '' }} m-0">
                        {{ Form::label('qty', 'Quantity*') }}
                        {{ Form::text('qty', null, ['class' => 'form-control', 'id' => 'qty1', 'placeholder' => '0']) }}
                        <small class="text-danger">{{ $errors->first('qty') }}</small>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('start') ? ' has-error' : '' }}">
                        {{ Form::label('start', 'Start Time') }}
                        {{ Form::date('start', null, ['class' => 'form-control', 'id' => 'start']) }}
                        <small class="text-danger">{{ $errors->first('start') }}</small>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('end') ? ' has-error' : '' }}">
                        {{ Form::label('end', 'End Time') }}
                        {{ Form::date('end', null, ['class' => 'form-control', 'id' => 'end']) }}
                        <small class="text-danger">{{ $errors->first('end') }}</small>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('discount') ? ' has-error' : '' }} m-0">
                        {{ Form::label('discount', 'Discount') }}
                        {{ Form::text('discount', null, ['class' => 'form-control', 'id' => 'discount1']) }}
                        <small class="text-danger">{{ $errors->first('discount') }}</small>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('sub_total') ? ' has-error' : '' }} m-0">
                        {{ Form::label('sub_total', 'Sub Total*') }}
                        {{ Form::text('sub_total', null, ['class' => 'form-control', 'id' => 'sub_total1', 'placeholder' => 'Enter Sub Total', 'readonly' => 'readonly']) }}
                        <small class="text-danger">{{ $errors->first('sub_total') }}</small>
                    </div>
                </div>

            </div>

            <div class="form-group mt-5">
                <p class="btn-holder">
                    <button class="btn btn-primary btn-block text-center" id="addToCart1"
                            type="submit"> Add to cart
                    </button>
                </p>
            </div>

        </div>
    </div>
    <div class="col-md-8 ">
        <div class="card card-body">

            <div class="row d-flex justify-content-between">
                <div class="col-sm-6 invoice-col">
                    <div class="form-group{{ $errors->has('supplier') ? ' has-error' : '' }}">
                        {{ Form::label('supplier', 'Supplier') }}
                        <div class="row">
                            <div class="col-sm-9">{{ Form::select('supplier', $suppliers, null, ['id' => 'supplier', 'class' => 'form-control select2']) }}
                                <small class="text-danger">{{ $errors->first('supplier') }}</small>
                            </div>
                            <div class="col-sm-3">
                                <button type="button" class="btn btn-primary " data-toggle="modal"
                                        data-target="#createSupplier">
                                    <i class="fas fa-user-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 invoice-col">

                    <b>Date:</b>
                    <br>
                    <h4> {{Carbon\Carbon::now()->format('d-m-Y')}}</h4>
                </div>

            </div>
        </div>
        <div class="card card-body" id="cart">


            <table class="table table-hover table-condensed ">
                <thead>
                <tr>
                    <th>Product</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Discount</th>
                    <th class="text-center">Subtotal</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @php $total = 0 @endphp
                @if (session('cart'))
                    @foreach (session('cart') as $id => $details)
                        @php $total += $details['rate'] * $details['qty'] @endphp
                        <tr data-id="{{ $id }}">
                            <td data-th="Product">
                                <div class="row">
                                    {{-- <div class="col-sm-3 hidden-xs"><img src="{{ $details['image'] }}"
                                            width="100" height="100" class="img-responsive" /></div> --}}
                                    <div class="col-sm-9">
                                        <h5 class="nomargin">{{ $details['name'] }}</h5>
                                    </div>
                                </div>
                            </td>
                            <td data-th="rate">${{ $details['rate'] }}</td>
                            <td data-th="qty">{{ $details['qty'] }}
                            <td data-th="discount">${{ $details['discount'] }}
                                {{-- <input type="number"  id="updateCart"  value=""
                                    class="form-control qty update-cart" /> --}}
                            </td>
                            <td data-th="Subtotal" class="text-center subtotal">
                                ${{ $details['rate'] * $details['qty'] - $details['discount'] }}</td>
                            <td class="actions" data-th="">
                                <button class="btn btn-danger btn-sm remove-from-cart"
                                        data-id="{{ $id }}"><i class="fas fa-trash"></i></button>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
                {{-- <tfoot>
                    <tr>
                        <td colspan="5" class="text-right">
                            <h3><strong>Total ${{ $total }}</strong></h3>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" class="text-right">
                            <a href="{{ url('/') }}" class="btn btn-warning"><i class="fa fa-angle-left"></i>
                                Continue Shopping</a>
                            <button class="btn btn-success">Checkout</button>
                        </td>
                    </tr>
                </tfoot> --}}
            </table>
            <br>
            <br>
            <br>
            <br>
            <br>

            <div class="row d-flex justify-content-end">
                <div class="col-md-6">

                    <div class="col-md-12">
                        <table class="table table-bordered table-hover" id="tab_logic_total">
                            <tbody>
                            @php $total = 0 @endphp
                            @if (session('cart'))
                                @foreach (session('cart') as $id => $details)
                                    @php $total += (($details['rate'] * $details['qty'])-$details['discount']) @endphp
                                @endforeach
                            @endif
                            <tr>
                                <th class="text-center">Total</th>
                                <td class="text-center"><input type="number" name='total' placeholder='0.00'
                                                               value="{{ $total }}" class="form-control" id="total"
                                                               readonly/>
                                </td>
                            </tr>
                            <tr>
                                <th class="text-center">Tax</th>
                                <td class="text-center">
                                    <div class="input-group mb-2 mb-sm-0">
                                        <input type="number" class="form-control" id="tax"
                                               placeholder="0">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th class="text-center">Discount</th>
                                <td class="text-center"><input type="text" name='invoice_discount'
                                                               id="invoice_discount" placeholder='0.00' value="0"
                                                               class="form-control"/></td>
                            </tr>
                            <tr>
                                <th class="text-center">Grand Total</th>
                                <td class="text-center"><input type="number" name='grand_total'
                                                               value="{{ $total }}" id="grand_total" placeholder='0.00'
                                                               class="form-control" readonly/></td>
                            </tr>

                            </tbody>
                        </table>
                        <div class="text-right m-2">
                            <button id="clear_cart" type="button" class="btn btn-warning"><i
                                        class="fas fa-eraser"></i> Clear Cart
                            </button>
                            <button class="btn btn-success" id="checkout"><i class="fas fa-shopping-cart"></i>
                                Checkout
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Supplier Modal -->
<div class="modal fade" id="createSupplier" tabindex="-1" role="dialog" aria-labelledby="createSupplierTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            {{ Form::open(['method' => 'POST', 'route' => 'invoiceSupplier.store', 'class' => 'form-horizontal']) }}
            <div class="modal-header">
                <h4 class="modal-title primary" id="createSupplierTitle"> Create
                    Supplier</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row d-flex justify-content-center">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter Supplier Name']) }}
                            <small class="text-danger">{{ $errors->first('name') }}</small>
                        </div>
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            {{ Form::label('address', 'Address') }}
                            {{ Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Enter Address']) }}
                            <small class="text-danger">{{ $errors->first('address') }}</small>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                            {{ Form::label('mobile', 'Phone Number') }}
                            {{ Form::text('mobile', null, ['class' => 'form-control', 'placeholder' => 'Enter Phone Number']) }}
                            <small class="text-danger">{{ $errors->first('mobile') }}</small>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            {{ Form::label('email', 'Email address') }}
                            {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'eg: foo@bar.com']) }}
                            <small class="text-danger">{{ $errors->first('email') }}</small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{ Form::submit('Add Supplier', ['class' => 'btn btn-primary']) }}
                {{-- <button type="button" type="submit" class="btn btn-primary">Save
                    changes</button> --}}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
<!-- Customer Modal -->
<div class="modal fade" id="createCustomer" tabindex="-1" role="dialog" aria-labelledby="createCustomerTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            {{ Form::open(['method' => 'POST', 'route' => 'invoiceCustomer.store', 'class' => 'form-horizontal']) }}
            <div class="modal-header">
                <h4 class="modal-title primary" id="createCustomerTitle"> Create
                    Customer</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row d-flex justify-content-center">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter Customer Name']) }}
                            <small class="text-danger">{{ $errors->first('name') }}</small>
                        </div>
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            {{ Form::label('address', 'Address') }}
                            {{ Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Enter Address']) }}
                            <small class="text-danger">{{ $errors->first('address') }}</small>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                            {{ Form::label('mobile', 'Phone Number') }}
                            {{ Form::text('mobile', null, ['class' => 'form-control', 'placeholder' => 'Enter Phone Number']) }}
                            <small class="text-danger">{{ $errors->first('mobile') }}</small>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            {{ Form::label('email', 'Email address') }}
                            {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'eg: foo@bar.com']) }}
                            <small class="text-danger">{{ $errors->first('email') }}</small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{ Form::submit('Add Customer', ['class' => 'btn btn-primary']) }}
                {{-- <button type="button" type="submit" class="btn btn-primary">Save
                            changes</button> --}}
            </div>

            {{ Form::close() }}
        </div>
    </div>
</div>

{{-- <div class="row d-flex justify-content-center">
    <div class="col-md-12">
    </div>
</div>

<div class="row d-flex justify-content-between">
    <div class="col-md-6">
    </div>
    <div class="col-md-4">
        <div class="card card-body">
            <div class="pull-right col-md-12">
                <table class="table table-bordered table-hover" id="tab_logic_total">
                    <tbody>
                        <tr>
                            <th class="text-center">Total</th>
                            <td class="text-center"><input type="number" name='sub_total' placeholder='0.00'
                                value="{{session('cart')}}"    class="form-control" id="total" readonly /></td>
                        </tr>
                        <tr>
                            <th class="text-center">CGST</th>
                            <td class="text-center">
                                <div class="input-group mb-2 mb-sm-0">
                                    <input type="number" class="form-control" id="tax1" placeholder="0">
                                    <div class="input-group-addon">%</div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center">CGST Tax Amount</th>
                            <td class="text-center"><input type="number" name='tax_amount' id="tax_amount"
                                    placeholder='0.00' class="form-control" readonly /></td>
                        </tr>
                        <tr>
                            <th class="text-center">Grand Total</th>
                            <td class="text-center"><input type="number" name='total_amount' id="total_amount"
                                    placeholder='0.00' class="form-control" readonly /></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> --}}

@section('script')
    <script>

        $('#clear_cart').click(function () {
            $.ajax({
                url: "{{route('purchase.clearCart')}}",
                type: "GET",
                success: function (e) {
                    $("#cart").html(e);
                }
            });
        });

        $('#checkout').click(function () {
            var total = $('#total').val();
            var tax = $('#tax').val();
            var invoice_discount = $('#invoice_discount').val();
            var grand_total = $('#grand_total').val();
            var people_id = $('#supplier').val();
            // var people_type = "App\Models\Invoice\InvoiceCustomer";
            $.ajax({
                url: "{{route('purchase.checkout')}}",
                type: "POST",
                data: {
                    _token: "{{csrf_token()}}",
                    // people_type:        people_type,
                    people_id: people_id,
                    total: total,
                    tax: tax,
                    invoice_discount: invoice_discount,
                    grand_total: grand_total,
                },
                success: (function (data) {
                    // console.log(data);
                    window.location = data;
                }),
                error: function (error) {
                    console.log(error);
                }
            });
        });

        $('#tax').keyup(function () {
            var tax = $(this).val();
            var total = $('#total').val();
            var invoice_discount = $('#invoice_discount').val();
            var grand_total = parseFloat(total) + parseFloat(tax) - parseInt(invoice_discount);
            $('#grand_total').val(grand_total);

        });
        $('#invoice_discount').keyup(function () {
            var invoice_discount = $(this).val();
            console.log(invoice_discount);
            var total = $('#total').val();
            var tax = $('#tax').val();
            var grand_total = parseInt(total) - parseInt(invoice_discount) + parseFloat(tax);
            $('#grand_total').val(grand_total);

        });

        $(".remove-from-cart").click(function (e) {
            e.preventDefault();

            var ele = $(this).data("id");
            console.log(ele);
            // $(this).parents('tr').remove();

            // if (confirm("Are you sure want to remove?")) {
            $.ajax({
                url: '{{ route('purchase.remove') }}',
                method: "Post",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: ele
                }
            }).done(function (e) {
                console.log(e);
                $("#cart").html(e);
            });
            // }
        });

        // Left Side Product
        $("#addToCart").click(function (e) {
            e.preventDefault();
            var product = $('#product').val();
            var rate = $('#rate').val();
            var sub_total = $('#sub_total').val();
            var qty = $('#qty').val();
            var discount = $('#discount').val();
            var model = $('#model').val();
            console.log(product);
            // console.log(q);
            var csrf = "{{ @csrf_token() }}"
            $.ajax({
                url: "{{ url('admin/purchase/addToCart') }}",
                data: {
                    _token: csrf,
                    product: product,
                    rate: rate,
                    discount: discount,
                    sub_total: sub_total,
                    qty: qty,
                    model: model,
                },
                type: "POST",
            }).done(function (e) {
                console.log(e);
                $("#cart").html(e);
                $('#sub_total').val('');
                $('#qty').val('');
                $('#discount').val('');
                $('#product').val('');
                $('#rate').val('');

            });
        })

        $('#product').change(function () {
            var id = $(this).val();
            $.ajax({
                url: '{{ url('admin/purchase/getProduct') }}/' + id,
                type: 'get',
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    $('#rate').val(data.price);
                }
            });
        });
        $('#updateCart').change(function (e) {
            e.preventDefault();

            var ele = $(this);

            $.ajax({
                url: '{{ route('purchase.updateCart') }}',
                method: "patch",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: ele.parents("tr").attr("data-id"),
                    qty: ele.parents("tr").find(".qty").val()
                }
            }).done(function (e) {
                console.log(e);
                $("#cart").html(e);
            });
        });

        $('#qty').keyup(function () {
            var qty = $(this).val();
            console.log(qty);
            var rate = $('#rate').val();
            var sub_total = qty * rate;
            $('#sub_total').val(sub_total);
        });

        $('#discount').keyup(function () {
            var discount = $(this).val();
            var total = $('#rate').val() * $('#qty').val();
            var sub_total = parseInt(total) - parseInt(discount);
            $('#sub_total').val(sub_total);
        });

        // Left Side Product


        // Left Side Service

        $("#addToCart1").click(function (e) {
            // alert('hi');
            e.preventDefault();
            var service = $('#service').val();
            var rate = $('#rate1').val();
            var sub_total = $('#sub_total1').val();
            var qty = $('#qty1').val();
            var discount = $('#discount1').val();
            var start = $('#start').val();
            var end = $('#end').val();
            var model = $('#model1').val();
            console.log(rate);
            // console.log(q);
            var csrf = "{{ @csrf_token() }}"
            $.ajax({
                url: "{{ url('admin/purchase/addToCart') }}",
                data: {
                    _token: csrf,
                    product: service,
                    rate: rate,
                    discount: discount,
                    sub_total: sub_total,
                    qty: qty,
                    start: start,
                    end: end,
                    model: model,
                },
                type: "POST",
            }).done(function (e) {
                console.log(e);
                $("#cart").html(e);
                $('#sub_total1').val('');
                $('#qty1').val('');
                $('#discount1').val('');
                $('#service').val('');
                $('#rate1').val('');
                $('#start').val('');
                $('#end').val('');

            });
        })

        $('#service').change(function () {
            var id = $(this).val();
            $.ajax({
                url: '{{ url('admin/purchase/getService') }}/' + id,
                type: 'get',
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    $('#rate1').val(data.price);
                }
            });
        });

        $('#qty1').keyup(function () {
            var qty = $(this).val();
            console.log(qty);
            var rate = $('#rate1').val();
            var sub_total = qty * rate;
            $('#sub_total1').val(sub_total);
        });

        $('#discount1').keyup(function () {
            var discount = $(this).val();
            var total = $('#rate1').val() * $('#qty1').val();
            var sub_total = parseInt(total) - parseInt(discount);
            $('#sub_total1').val(sub_total);
        });
        // Left Side Service
    </script>
    <script>
        $(function () {
            $('.select2').select2(
                theme
        :
            'classic',
        )
            ;
        });
    </script>
@endsection
