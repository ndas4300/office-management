<?php

namespace Modules\Invoice\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Invoice\Entities\Invoice;
use Modules\Invoice\Entities\InvoiceSupplier;
use Modules\Invoice\Entities\Product;
use Modules\Invoice\Entities\Purchase;
use Modules\Invoice\Entities\Service;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $purchases = Purchase::paginate(10);
        return view('invoice::purchase.index',compact('purchases'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // session()->forget('cart');
        $products = Product::pluck('name', 'id');
        $services = Service::pluck('name', 'id');

        $suppliers = InvoiceSupplier::pluck('name', 'id');
        return view('invoice::purchase.create', compact('products','services','suppliers'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */ 

    public function clearCart()
    {
        session()->forget('cart');
        return redirect()->route('purchase.getCart');
    }
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name'                 => ['required', 'string', 'max:255'],
            'address'              => ['required', 'string', 'max:255'],
            'mobile'               => ['required', 'string'],
            'email'                => ['required', 'string', 'max:255'],
            'contact_person'       => ['nullable', 'string', 'max:255'],
            'contact_person_email' => ['nullable', 'string', 'max:255'],
        ]);

        Purchase::create($validated);

        return redirect()->route('purchase.index')->with('success', 'Purchase Created Successfully.');
    }

    public function checkout(Request $request)
    {
        // return ($request->all);
        // $invoice_no='#0022'.$invoice-id;
        do {
            $invoice_no = random_int(100000, 999999);
        } while (Invoice::where("invoice_no", "=", $invoice_no)->first());
        
        $invoice=Invoice::create([
            'people_type'   => 'Modules\Invoice\Entities\InvoiceCustomer',
            'people_id'     => $request->people_id,
            'sub_total'     => $request->total,
            'tax'           => $request->tax,
            'discount'      => $request->invoice_discount,
            'total'         => $request->grand_total,
            'model'         => 'purchase',
            'invoice_no'    => '#'.$invoice_no,
        ]);
        $carts = session()->get('cart');

        foreach ($carts as $key => $cart) {

            Purchase::create([
                'purchasable_type'     => $cart['type'],
                'purchasable_id'       => $cart['id'],
                'invoice_id'           => $invoice->id,
                'price'                => $cart['rate'],
                'tax'                  => '0.00',
                'qty'                  => $cart['qty'],
                'discount'             => $cart['discount'],
                'total'                => $cart['total'],
                'billing_cycle_start'  => $cart['billing_cycle_start'],
                'billing_cycle_end'    => $cart['billing_cycle_end'],
                ]
            );
        }
        session()->forget('cart');

        $route= route('purchase.purchaseInvoice',$invoice->id);
        return response()->json($route);
    }

    public function getProduct($id)
    {
        $product = Product::findOrFail($id);
        return response()->json($product);
    }
    
    public function getService($id)
    {
        $service = Service::findOrFail($id);
        return response()->json($service);
    }
    /**
     * Display the specified resource.
     *
     * @param  \Modules\Invoice\Entities\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */

    public function addToCart(Request $request)
    {
        // return ($request->model);

        if ($request->model == "App\Models\Invoice\Product") {
            $data = Product::findOrFail($request->product);
            $model = 'Modules\Invoice\Entities\Product';
        }
        if ($request->model == "App\\Models\\Invoice\\Service") {
            $data = Service::findOrFail($request->product);
            $model = 'Modules\Invoice\Entities\Service';
        }
        
        $cart = session()->get('cart', []);
        $cart[$request->product.$data->name] = [
            "name"                      => $data->name,
            "id"                        => $data->id,
            "type"                      => $model,
            "rate"                      => $request->rate,
            "qty"                       => $request->qty,
            "discount"                  => $request->discount,
            "total"                     => $request->sub_total,
            "billing_cycle_start"       => $request->start,
            "billing_cycle_end"         => $request->end,
        ];
        // return $cart;
        session()->put('cart', $cart);
        // return session()->get('cart');

        return redirect()->route('purchase.getCart')->with('success', 'Product added to cart successfully!');
    }

    public function getCart()
    {
        // dd('sdfasdf');
        // $cart= session()->get('cart');
        // $total = 0;
        // for ($i=0; $i < (count($cart)); $i++) { 
        //     $total += $cart[$i]['sub_total'];
        // }

        // session()->put('total', $total);

        return view('invoice::purchase.cart');
    }

    public function updateCart(Request $request)
    {
        // dd('dsflkjsad');
        if($request->id && $request->quantity){
            $cart = session()->get('cart');
            $cart[$request->id]["quantity"] = $request->quantity;
            session()->put('cart', $cart);
        }
        return view('invoice::purchase.updateCart');
    }

    public function remove(Request $request)
    {
        if($request->id) {
            $cart = session()->get('cart');
            if(isset($cart[$request->id])) {
                unset($cart[$request->id]);
                session()->put('cart', $cart);
            }
            // return ('hello');
            return redirect()->route('purchase.getCart');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function purchaseInvoice($id)
    {
        $invoice=Invoice::findOrFail($id);
        // dd($invoice);
        return view('invoice::purchase.purchaseInvoice',compact('invoice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase $purchase)
    {
        return view('invoice::purchase.edit', compact('purchase'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchase $purchase)
    {
        $validated = $request->validate([
            'name'                 => ['required', 'string', 'max:255'],
            'address'              => ['required', 'string', 'max:255'],
            'mobile'               => ['required', 'string'],
            'email'                => ['required', 'string', 'max:255'],
            'contact_person'       => ['nullable', 'string', 'max:255'],
            'contact_person_email' => ['nullable', 'string', 'max:255'],
        ]);

        $purchase->update($validated);

        return redirect()->route('purchase.index')->with('success', 'Purchase updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function destroy(Purchase $purchase)
    {
        $purchase->delete();
        return back()->with('delete', 'Purchase deleted successfully.');
    }

    // public function status(Purchase $purchase)
    // {
    //     if ($purchase->is_active==0) {
    //         $purchase->is_active ='1';
    //     }
    //     else{
    //         $purchase->is_active ='0';
    //     }

    //     $purchase->update();

    //     return redirect()->route('purchase.index')->with('success', 'Status updated successfully.');
    // }
}