<?php

namespace Modules\Invoice\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Invoice\Entities\InvoiceSupplier;

class InvoiceSupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoiceSuppliers = InvoiceSupplier::paginate(10);
        return view('invoice::invoiceSupplier.index', compact('invoiceSuppliers'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        return view('invoice::invoiceSupplier.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name'                  => ['required', 'string', 'max:255'],
            'address'               => ['required', 'string', 'max:255'],
            'mobile'                => ['required', 'string'],
            'email'                 => ['required', 'string', 'max:255'],
            'contact_person'        => ['nullable', 'string', 'max:255'],
            'contact_person_email'  => ['nullable', 'string', 'max:255'],
            'contact_person_mobile' => ['nullable', 'string'],
        ]);

        InvoiceSupplier::create($validated);

        return redirect()->back()->with('success', 'InvoiceSupplier Created Successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InvoiceSupplier  $invoiceSupplier
     * @return \Illuminate\Http\Response
     */
    public function show(InvoiceSupplier $invoiceSupplier)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InvoiceSupplier  $invoiceSupplier
     * @return \Illuminate\Http\Response
     */
    public function edit(InvoiceSupplier $invoiceSupplier)
    {
        return view('invoice::invoiceSupplier.edit', compact('invoiceSupplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\InvoiceSupplier  $invoiceSupplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InvoiceSupplier $invoiceSupplier)
    {
        $validated = $request->validate([
            'name'                  => ['required', 'string', 'max:255'],
            'address'               => ['required', 'string', 'max:255'],
            'mobile'                => ['required', 'string'],
            'email'                 => ['required', 'string', 'max:255'],
            'contact_person'        => ['nullable', 'string', 'max:255'],
            'contact_person_email'  => ['nullable', 'string', 'max:255'],
            'contact_person_mobile' => ['nullable', 'string'],
        ]);

        $invoiceSupplier->update($validated);

        return redirect()->route('invoiceSupplier.index')->with('success', 'InvoiceSupplier updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InvoiceSupplier  $invoiceSupplier
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvoiceSupplier $invoiceSupplier)
    {
        $invoiceSupplier->delete();
        return back()->with('delete', 'InvoiceSupplier deleted successfully.');
    }

    // public function status(InvoiceSupplier $invoiceSupplier)
    // {
    //     if ($invoiceSupplier->is_active==0) {
    //         $invoiceSupplier->is_active ='1';
    //     }
    //     else{
    //         $invoiceSupplier->is_active ='0';
    //     }

    //     $invoiceSupplier->update();

    //     return redirect()->route('invoiceSupplier.index')->with('success', 'Status updated successfully.');
    // }
}

