<?php

namespace Modules\Invoice\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Invoice\Entities\InvoiceCustomer;

class InvoiceCustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoiceCustomers = InvoiceCustomer::paginate(10);
        return view('invoice::invoiceCustomer.index', compact('invoiceCustomers'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        return view('invoice::invoiceCustomer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name'                  => ['required', 'string', 'max:255'],
            'address'               => ['required', 'string', 'max:255'],
            'mobile'                => ['required', 'string'],
            'email'                 => ['required', 'string', 'max:255'],
            'contact_person'        => ['nullable', 'string', 'max:255'],
            'contact_person_email'  => ['nullable', 'string', 'max:255'],
            'contact_person_mobile' => ['nullable', 'string'],
        ]);

        InvoiceCustomer::create($validated);

        return redirect()->back()->with('success', 'InvoiceCustomer Created Successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InvoiceCustomer  $invoiceCustomer
     * @return \Illuminate\Http\Response
     */
    public function show(InvoiceCustomer $invoiceCustomer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InvoiceCustomer  $invoiceCustomer
     * @return \Illuminate\Http\Response
     */
    public function edit(InvoiceCustomer $invoiceCustomer)
    {
        return view('invoice::invoiceCustomer.edit', compact('invoiceCustomer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\InvoiceCustomer  $invoiceCustomer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InvoiceCustomer $invoiceCustomer)
    {
        $validated = $request->validate([
            'name'                  => ['required', 'string', 'max:255'],
            'address'               => ['required', 'string', 'max:255'],
            'mobile'                => ['required', 'string'],
            'email'                 => ['required', 'string', 'max:255'],
            'contact_person'        => ['nullable', 'string', 'max:255'],
            'contact_person_email'  => ['nullable', 'string', 'max:255'],
            'contact_person_mobile' => ['nullable', 'string'],
        ]);

        $invoiceCustomer->update($validated);

        return redirect()->route('invoiceCustomer.index')->with('success', 'InvoiceCustomer updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InvoiceCustomer  $invoiceCustomer
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvoiceCustomer $invoiceCustomer)
    {
        $invoiceCustomer->delete();
        return back()->with('delete', 'InvoiceCustomer deleted successfully.');
    }

    // public function status(InvoiceCustomer $invoiceCustomer)
    // {
    //     if ($invoiceCustomer->is_active==0) {
    //         $invoiceCustomer->is_active ='1';
    //     }
    //     else{
    //         $invoiceCustomer->is_active ='0';
    //     }

    //     $invoiceCustomer->update();

    //     return redirect()->route('invoiceCustomer.index')->with('success', 'Status updated successfully.');
    // }
}
