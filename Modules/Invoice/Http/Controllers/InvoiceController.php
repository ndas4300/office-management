<?php

namespace Modules\Invoice\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Invoice\Entities\Invoice;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $invoices = Invoice::paginate(10);
        return view('invoice::invoice.index',compact('invoices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \Modules\Invoice\Entities\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        return view('invoice::invoice.show',compact('invoice'));
    }

    public function search(Request $request)
    {
        // dd('asdkjaslkj');
        $invoices = Invoice::query()
            ->where('invoice_no','like','%'.$request->get('q').'%')
            ->orWhere('total','like','%'.$request->get('q').'%')
            ->paginate(10);

        return view('invoice::invoice.searchInvoice',compact('invoices'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Modules\Invoice\Entities\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Modules\Invoice\Entities\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Modules\Invoice\Entities\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        
        $invoice->delete();
        return back()->with('delete', 'Invoice deleted successfully.');
    }
}
