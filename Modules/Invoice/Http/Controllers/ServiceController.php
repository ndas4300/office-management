<?php

namespace Modules\Invoice\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Invoice\Entities\Category;
use Modules\Invoice\Entities\Service;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::paginate(10);
        return view('invoice::service.index', compact('services'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $categories = Category::pluck('name','id');
        return view('invoice::service.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name'                  => ['required', 'string', 'max:255'],
            'category_id'           => ['nullable','integer'],
            'price'                 => ['required','string'],
            'subscription_period'   => ['required','integer'],
            'is_active'             => ['boolean'],
        ]);

        Service::create($validated);

        return redirect()->route('service.index')->with('success', 'Service Created Successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        $categories = Category::pluck('name','id');
        return view('invoice::service.edit', compact('service','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        $validated = $request->validate([
            'name'                  => ['required', 'string', 'max:255'],
            'category_id'           => ['nullable','integer'],
            'price'                 => ['required','string'],
            'subscription_period'   => ['required','integer'],
            'is_active'             => ['boolean'],
        ]);

        if ($request->is_active==null) {
            $validated['is_active'] = '0';
        }

        $service->update($validated);

        return redirect()->route('service.index')->with('success', 'Service updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        $service->delete();
        return back()->with('delete', 'Service deleted successfully.');
    }

    public function status(Service $service)
    {
        if ($service->is_active==0) {
            $service->is_active ='1';
        }
        else{
            $service->is_active ='0';
        }

        $service->update();

        return redirect()->route('service.index')->with('success', 'Status updated successfully.');
    }
}
