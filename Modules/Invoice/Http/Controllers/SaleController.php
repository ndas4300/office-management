<?php

namespace Modules\Invoice\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Invoice\Entities\Invoice;
use Modules\Invoice\Entities\InvoiceCustomer;
use Modules\Invoice\Entities\Product;
use Modules\Invoice\Entities\Sale;
use Modules\Invoice\Entities\Service;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sales = Sale::paginate(10);
        return view('invoice::sale.index',compact('sales'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // session()->forget('cart');
        $products = Product::pluck('name', 'id');
        $services = Service::pluck('name', 'id');

        $customers = InvoiceCustomer::pluck('name', 'id');  
        return view('invoice::sale.create', compact('products','services','customers'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function clearCart()
    {
        session()->forget('cart');
        return redirect()->route('sale.getCart');  
    }

    // public function store(Request $request)
    // {
    //     $validated = $request->validate([
    //         'name'                 => ['required', 'string', 'max:255'],
    //         'address'              => ['required', 'string', 'max:255'],
    //         'mobile'               => ['required', 'string'],
    //         'email'                => ['required', 'string', 'max:255'],
    //         'contact_person'       => ['nullable', 'string', 'max:255'],
    //         'contact_person_email' => ['nullable', 'string', 'max:255'],
    //     ]);

    //     Sale::create($validated);

    //     return redirect()->route('sale.index')->with('success', 'Sale Created Successfully.');
    // }

    public function checkout(Request $request)
    {
        // return ($request->all);
        do {
            $invoice_no = random_int(100000, 999999);
        } while (Invoice::where("invoice_no", "=", $invoice_no)->first());
        
        $invoice=Invoice::create([
            'people_type'   => 'Modules\Invoice\Entities\InvoiceCustomer',
            'people_id'     => $request->people_id,
            'sub_total'     => $request->total,
            'tax'           => $request->tax,
            'discount'      => $request->invoice_discount,
            'total'         => $request->grand_total,
            'model'         => 'sale',
            'invoice_no'    => '#'.$invoice_no,
        ]);
        $carts = session()->get('cart');

        foreach ($carts as $key => $cart) {

            Sale::create([
                'salable_type'         => $cart['type'],
                'salable_id'           => $cart['id'],
                'invoice_id'           => $invoice->id,
                'price'                => $cart['rate'],
                'tax'                  => '0.00',
                'qty'                  => $cart['qty'],
                'discount'             => $cart['discount'],
                'total'                => $cart['total'],
                'billing_cycle_start'  => $cart['billing_cycle_start'],
                'billing_cycle_end'    => $cart['billing_cycle_end'],
                
            ]);
        }
        session()->forget('cart');

        $route= route('sale.saleInvoice',$invoice->id);
        return response()->json($route);
    }

    public function getProduct($id)
    {
        $product = Product::findOrFail($id);
        return response()->json($product);
    }
    
    public function getService($id)
    {
        $service = Service::findOrFail($id);
        return response()->json($service);
    }
    /**
     * Display the specified resource.
     *
     * @param  \Modules\Invoice\Entities\Sale  $sale
     * @return \Illuminate\Http\Response
     */

    public function addToCart(Request $request)
    {
        // return ($request->model);

        if ($request->model == "App\Models\Invoice\Product") {
            $data = Product::findOrFail($request->product);
            $model = 'Modules\Invoice\Entities\Product';
        }
        if ($request->model == "App\\Models\\Invoice\\Service") {
            $data = Service::findOrFail($request->product);
            $model = 'Modules\Invoice\Entities\Service';
        }
        
        $cart = session()->get('cart', []);
        $cart[$request->product.$data->name] = [
            "name"                      => $data->name,
            "id"                        => $data->id,
            "type"                      => $model,
            "rate"                      => $request->rate,
            "qty"                       => $request->qty,
            "discount"                  => $request->discount,
            "total"                     => $request->sub_total,
            "billing_cycle_start"       => $request->start,
            "billing_cycle_end"         => $request->end,
        ];
        // return $cart;
        session()->put('cart', $cart);
        // return session()->get('cart');

        return redirect()->route('sale.getCart')->with('success', 'Product added to cart successfully!');
    }

    public function getCart()
    {
        return view('invoice::sale.cart');
    }

    public function updateCart(Request $request)
    {
        // dd('dsflkjsad');
        if($request->id && $request->quantity){
            $cart = session()->get('cart');
            $cart[$request->id]["quantity"] = $request->quantity;
            session()->put('cart', $cart);
        }
        return view('invoice::sale.updateCart');
    }

    public function remove(Request $request)
    {
        if($request->id) {
            $cart = session()->get('cart');
            if(isset($cart[$request->id])) {
                unset($cart[$request->id]);
                session()->put('cart', $cart);
            }
            // return ('hello');
            return redirect()->route('sale.getCart');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function saleInvoice($id)
    {
        $invoice=Invoice::findOrFail($id);
        // dd($invoice);
        return view('invoice::sale.saleInvoice',compact('invoice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function edit(Sale $sale)
    {
        return view('invoice::sale.edit', compact('sale'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sale $sale)
    {
        $validated = $request->validate([
            'name'                 => ['required', 'string', 'max:255'],
            'address'              => ['required', 'string', 'max:255'],
            'mobile'               => ['required', 'string'],
            'email'                => ['required', 'string', 'max:255'],
            'contact_person'       => ['nullable', 'string', 'max:255'],
            'contact_person_email' => ['nullable', 'string', 'max:255'],
        ]);

        $sale->update($validated);

        return redirect()->route('sale.index')->with('success', 'Sale updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sale $sale)
    {
        $sale->delete();
        return back()->with('delete', 'Sale deleted successfully.');
    }

    // public function status(Sale $sale)
    // {
    //     if ($sale->is_active==0) {
    //         $sale->is_active ='1';
    //     }
    //     else{
    //         $sale->is_active ='0';
    //     }

    //     $sale->update();

    //     return redirect()->route('sale.index')->with('success', 'Status updated successfully.');
    // }
}
