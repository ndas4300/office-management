<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::prefix('officesetup')->group(function() {
//    Route::get('/', 'OfficeSetupController@index');
//});




Route::group(['middleware'=>'auth','prefix'=>'admin'],function () {
    //Route for office setup starts here
    //route for shift
    Route::resource('shift',ShiftController::class);
    //route for designation
    Route::resource('designation',DesignationController::class);
    Route::put('designation/check-status/{id}',[DesignationController::class,'status']);

    //route for department
    Route::resource('department',DepartmentController::class);
    Route::put('department/check-status/{id}',[DepartmentController::class,'status']);
    //route for status
    Route::resource('status',StatusController::class);
    Route::put('status/check-status/{id}',[StatusController::class,'status']);

    //route for calender
    Route::resource('calendar',CalenderController::class);
    Route::put('calendar/check-status/{id}',[CalenderController::class,'status']);
    //route for calender events
    Route::resource('calendar-event',CalendarEventController::class);
    Route::put('calendar-event/is-holiday/{id}',[CalendarEventController::class,'isHoliday']);
    Route::put('calendar-event/sms/{id}',[CalendarEventController::class,'sms']);
    Route::put('calendar-event/email/{id}',[CalendarEventController::class,'email']);
    Route::put('calendar-event/is-active/{id}',[CalendarEventController::class,'status']);
    Route::post('calendar-event/search',[ CalendarEventController::class ,'search'])->name('calendarEvent.search');


});
