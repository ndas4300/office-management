<?php

namespace Modules\OfficeSetup\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\OfficeSetup\Entities\CalendarEvent;
use Modules\OfficeSetup\Entities\Calender;

class CalendarEventController extends Controller
{
    public function index()
    {
        $calendarEvent = CalendarEvent::query()->orderBy('start','asc')->paginate(10);
        return view('officesetup::calendarEvent.index',compact('calendarEvent'));
    }

    public function search(Request $request)
    {
        // dd('asdkjaslkj');
        $calendarEvents = CalendarEvent::query()
            ->where('name','like','%'.$request->get('q').'%')
            ->orWhere('start','like','%'.$request->get('q').'%')
            ->orWhere('end','like','%'.$request->get('q').'%')
            ->paginate(10);

        return view('officesetup::calendarEvent.searchCalendarEvent',compact('calendarEvents'));
    }

    public function create()
    {
        $calendars = Calender::query()->pluck('name','id');
        return view('officesetup::calendarEvent.add',compact('calendars'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'calendar_id' => 'required',
            'description' => 'required',
            'start' => 'required',
            'is_holiday' => 'required',
            'sms' => 'required',
            'email' => 'required',
            'is_active' => 'required'
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        CalendarEvent::query()->create($request->all());
        return redirect('admin/calendar-event')->with('success', 'Event added Successfully!');
    }

    public function edit($id)
    {
        $calendarEvent= CalendarEvent::query()->findOrFail($id);
        $calendars = Calender::query()->pluck('name','id');
        return view('officesetup::calendarEvent.edit',compact('calendarEvent','calendars'));
    }

    public function update($id, Request $request)
    {
        $validator = $request->validate([
            'name' => 'required',
            'calendar_id' => 'required',
            'description' => 'required',
            'start' => 'required',
            'is_holiday' => 'nullable',
            'sms' => 'nullable',
            'email' => 'nullable',
            'is_active' => 'nullable'
        ]);
        if (!$request->has('is_holiday')) {
            $validator['is_holiday']=$request->is_holiday= '0';
        }
        if (!$request->has('sms')) {
            $validator['sms']=$request->sms= '0';
        }
        if (!$request->has('email')) {
            $validator['email']=$request->email= '0';
        }
        if (!$request->has('is_active')) {
            $validator['is_active']=$request->is_active= '0';
        }
        // dd($validator);
        $calendarEvent = CalendarEvent::query()->findOrFail($id);


        $calendarEvent->update($validator);
        return redirect('admin/calendar-event')->with('success', 'Event Updated Successfully!');
    }

    public function destroy($id)
    {
        $calendarEvent = CalendarEvent::query()->findOrFail($id);
        $calendarEvent->delete();
        return redirect('admin/calendar-event')->with('success', 'Event Deleted Successfully!');
    }

    public function isHoliday($id): \Illuminate\Http\RedirectResponse
    {

        $calendarEvent = CalendarEvent::query()->findOrFail($id);
        if($calendarEvent->is_holiday == 1)
        {
            $calendarEvent->is_holiday = 0;
        }else{
            $calendarEvent->is_holiday = 1;
        }
        $calendarEvent->save();

        return redirect('admin/calendar-event')->with('success','Status Changed Successfully');
    }

    public function sms($id): \Illuminate\Http\RedirectResponse
    {

        $calendarEvent = CalendarEvent::query()->findOrFail($id);
        if($calendarEvent->sms == 1)
        {
            $calendarEvent->sms = 0;
        }else{
            $calendarEvent->sms = 1;
        }
        $calendarEvent->save();

        return redirect('admin/calendar-event')->with('success','Status Changed Successfully');
    }

    public function email($id): \Illuminate\Http\RedirectResponse
    {

        $calendarEvent = CalendarEvent::query()->findOrFail($id);
        if($calendarEvent->email == 1)
        {
            $calendarEvent->email = 0;
        }else{
            $calendarEvent->email = 1;
        }
        $calendarEvent->save();

        return redirect('admin/calendar-event')->with('success','Status Changed Successfully');
    }

    public function status($id): \Illuminate\Http\RedirectResponse
    {

        $calendarEvent = CalendarEvent::query()->findOrFail($id);
        if($calendarEvent->is_active == 1)
        {
            $calendarEvent->is_active = 0;
        }else{
            $calendarEvent->is_active = 1;
        }
        $calendarEvent->save();

        return redirect('admin/calendar-event')->with('success','Status Changed Successfully');
    }
}
