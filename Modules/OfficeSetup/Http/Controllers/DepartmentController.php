<?php

namespace Modules\OfficeSetup\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\OfficeSetup\Entities\Department;

class DepartmentController extends Controller
{
    public function index()
    {
        $departments = Department::query()->paginate(10);
        return view('officesetup::department.index',compact('departments'));
    }

    public function create()
    {
        return view('officesetup::department.add');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required'
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        Department::query()->create($request->all());
        return redirect('admin/department')->with('success', 'Department added Successfully!');
    }

    public function edit($id)
    {
        $department = Department::query()->findOrFail($id);
        return view('officesetup::department.edit',compact('department'));
    }

    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required'
        ]);
        $department = Department::query()->findOrFail($id);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $department->update($request->all());
        return redirect('admin/department')->with('success', 'Department Updated Successfully!');
    }

    public function destroy($id)
    {
        $department = Department::query()->findOrFail($id);
        $department->delete();
        return redirect('admin/department')->with('success', 'Department Deleted Successfully!');
    }

    public function status($id): \Illuminate\Http\RedirectResponse
    {

        $department = Department::query()->findOrFail($id);
        if($department->is_active == 1)
        {
            $department->is_active = 0;
        }else{
            $department->is_active = 1;
        }
        $department->save();

        return redirect('admin/department')->with('success','Status Change Successfully');
    }
}
