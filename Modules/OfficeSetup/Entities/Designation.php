<?php

namespace Modules\OfficeSetup\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\Hrm\Entities\EmployeeOfficial;

class Designation extends Model
{
    use HasFactory;
    protected $fillable=['name','display_name','is_active'];


    public function officials(){
        return $this->hasMany(EmployeeOfficial::class);
    }
}
