<?php

namespace Modules\OfficeSetup\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\Hrm\Entities\EmployeeOfficial;

class Calender extends Model
{
    use HasFactory;
    protected $table = 'calendars';
    protected $fillable=['name','description','status'];

    public function officials(){
        return $this->hasMany(EmployeeOfficial::class);
    }
}
