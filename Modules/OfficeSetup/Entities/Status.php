<?php

namespace Modules\OfficeSetup\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\Hrm\Entities\EmployeeStatus;

class Status extends Model
{
    use HasFactory;
    protected $table='statuses';
    protected $fillable=['name','description','is_active'];


    public function statuses(){
        return $this->hasMany(EmployeeStatus::class);
    }
}
