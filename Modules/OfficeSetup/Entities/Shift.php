<?php

namespace Modules\OfficeSetup\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\Hrm\Entities\EmployeeOfficial;

class Shift extends Model
{
    use HasFactory;

    // protected $dates = ['in','out'];

    protected $fillable=['name','in','out','grace','description'];

    public function officials(){
        return $this->hasMany(EmployeeOfficial::class);
    }
}
