<table id="example2" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>{{ __('Sl') }}</th>
            <th>{{ __('Name') }}</th>
            <th>{{ __('Calendar Name') }}</th>
            <th>{{ __('Description') }}</th>
            <th>{{ __('Start') }}</th>
            <th>{{ __('End') }}</th>
            <th>{{ __('Is Holiday') }}</th>
            <th>{{ __('Sms') }}</th>
            <th>{{ __('Email') }}</th>
            <th>{{ __('Status') }}</th>
            <th>{{ __('Action') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($calendarEvents as $key => $data)
        <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $data->name }}</td>
            <td>{{ $data->calendar->name }}</td>
            <td>{{ $data->description }}</td>
            <td>{{ $data->start }}</td>
            <td>{{ $data->end }}</td>
            <td>
                {{
                Form::open(['method'=>'PUT','url'=>['admin/calendar-event/is-holiday/'.$data->id],'style'=>'display:inline'])
                }}
                @if($data->is_holiday == 1)
                {{ Form::submit('Yes',['class'=>'btn btn-success btn-sm']) }}
                @else
                {{ Form::submit('No',['class'=>'btn btn-danger btn-sm']) }}
                @endif
                {{ Form::close() }}
            </td>
            <td>
                {{
                Form::open(['method'=>'PUT','url'=>['admin/calendar-event/sms/'.$data->id],'style'=>'display:inline'])
                }}
                @if($data->sms == 1)
                {{ Form::submit('Yes',['class'=>'btn btn-success btn-sm']) }}
                @else
                {{ Form::submit('No',['class'=>'btn btn-danger btn-sm']) }}
                @endif
                {{ Form::close() }}
            </td>
            <td>
                {{
                Form::open(['method'=>'PUT','url'=>['admin/calendar-event/email/'.$data->id],'style'=>'display:inline'])
                }}
                @if($data->email == 1)
                {{ Form::submit('Yes',['class'=>'btn btn-success btn-sm']) }}
                @else
                {{ Form::submit('No',['class'=>'btn btn-danger btn-sm']) }}
                @endif
                {{ Form::close() }}
            </td>
            <td>
                {{
                Form::open(['method'=>'PUT','url'=>['admin/calendar-event/is-active/'.$data->id],'style'=>'display:inline'])
                }}
                @if($data->is_active == 1)
                {{ Form::submit('Active',['class'=>'btn btn-success btn-sm']) }}
                @else
                {{ Form::submit('In Active',['class'=>'btn btn-danger btn-sm']) }}
                @endif
                {{ Form::close() }}
            </td>
            <td>
                {{
                Form::open(['url'=>['admin/calendar-event',$data->id],'method'=>'delete','onsubmit'=>'return
                confirmDelete()']) }}
                <a href="{{ route('calendar-event.edit',$data->id) }}"
                    class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a>
                <button type="submit" class="btn btn-danger btn-sm"><i
                        class="fas fa-trash"></i></button>
                {{ Form::close() }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>