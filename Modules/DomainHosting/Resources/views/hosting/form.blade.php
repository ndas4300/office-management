<div class="row">
    <div class="col">
        <div class="form-group">
            <label for="name">Name*:</label>
            {{ Form::text('name',null,['class'=>'form-control ','placeholder'=>'Enter Hosting Name']) }}
        <small class="text-danger">{{ $errors->first('name') }}</small>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="provider_id">Provider*:</label>
            {{ Form::select('provider_id',$providers,$hosting->provider_id ?? null,['class'=>'form-control select2','placeholder'=>'Select a provider']) }}
        <small class="text-danger">{{ $errors->first('provider_id') }}</small>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="package">Package*:</label>
            {{ Form::text('package',null,['class'=>'form-control','placeholder'=>'Enter Package']) }}
        <small class="text-danger">{{ $errors->first('package') }}</small>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="form-group">
            <label for="amount">Amount*:</label>
            {{ Form::text('amount',null,['class'=>'form-control','placeholder'=>'Enter Amount']) }}
        <small class="text-danger">{{ $errors->first('amount') }}</small>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="registration_date">Registration Date*:</label>
            {{ Form::date('registration_date',null,['class'=>'form-control']) }}
        <small class="text-danger">{{ $errors->first('registration_date') }}</small>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="expire_date">Expire Date*:</label>
            {{ Form::date('expire_date',null,['class'=>'form-control ']) }}
        <small class="text-danger">{{ $errors->first('expire_date') }}</small>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="form-group">
            <label for="note">Note*:</label>
            {{ Form::textarea('note',null,['class'=>'form-control','placeholder'=>'Enter Note Here:']) }}
        <small class="text-danger">{{ $errors->first('note') }}</small>
        </div>
    </div>
</div>

<div class="col">
    <button type="submit" class="btn btn-success" > Create</button>
    <a href="{{ URL::previous() }}" class="btn btn-danger">Cancel</a>
</div>


    @section('script')
        <script>
            $(function () {
                $('.select2').select2();
            });
        </script>
@stop
