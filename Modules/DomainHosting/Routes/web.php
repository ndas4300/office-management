<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::prefix('domainhosting')->group(function() {
//    Route::get('/', 'DomainHostingController@index');
//});

//Route for domain-hosting starts here


use Modules\DomainHosting\Http\Controllers\DomainHostingController;

Route::group(['middleware'=>'auth','prefix'=>'admin'],function () {

    Route::get('domain-hosting', [DomainHostingController::class, 'index'])->name('domain-hosting');
    //route for provider
    Route::resource('provider', ProviderController::class);
    //route for domain
    Route::resource('domain', DomainController::class);
    //route for hosting
    Route::resource('hosting', HostingController::class);
    //route for customer
    Route::resource('customer', CustomerController::class);
    //route for customer search
    Route::post('customer-search', [CustomerController::class, 'search'])->name('customer.search');
    //route for customer hosting
    Route::resource('customer-hosting', CustomerHostingController::class);
    Route::put('customer-hosting/check-status/{id}', [CustomerHostingController::class, 'status']);
    //route for customer domain
    Route::resource('customer-domain', CustomerDomainController::class);
    Route::put('customer-domain/check-status/{id}', [CustomerDomainController::class, 'status']);
    //route for name server
    Route::resource('name-server', NameServerController::class);
    Route::put('name-server/check-status/{id}', [NameServerController::class, 'status']);
    //route for renews
    Route::resource('renew', RenewController::class);
    //Route for domain-hosting ends here
});