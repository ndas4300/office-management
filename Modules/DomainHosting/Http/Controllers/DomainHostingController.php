<?php

namespace Modules\DomainHosting\Http\Controllers;

use App\Http\Controllers\Controller;
use Modules\DomainHosting\Entities\CustomerDomain;
use Modules\DomainHosting\Entities\Hosting;


class DomainHostingController extends Controller
{
    public function index()
    {
        $customers = CustomerDomain::query()
            ->take(5)
            ->with('domain')
            ->orderBy('expire_date','DESC')
            ->get();

        $hosting = Hosting::query()
            ->take(5)
            ->orderBy('expire_date','DESC')
            ->get();

        return view('domainhosting::dashboard',compact('customers','hosting'));
    }
}
