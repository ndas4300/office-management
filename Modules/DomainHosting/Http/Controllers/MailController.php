<?php

namespace Modules\DomainHosting\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\NotificationMail;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function index()
    {
        $mailData = [
            'title' => 'Mail from Ni07',
            'url' => 'niloydas.net'
        ];

        Mail::to('nishatchowdhury73@gmail.com')->send(new NotificationMail($mailData));
    }
}
