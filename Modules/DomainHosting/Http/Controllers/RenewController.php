<?php

namespace Modules\DomainHosting\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\DomainHosting\Entities\Renew;

class RenewController extends Controller
{
    public function index()
    {
        $renew = Renew::query()->paginate(10);
        return view('domainhosting::renew.index',compact('renew'));
    }

    public function create()
    {
        return view('domainhosting::renew.add');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'renewable_id' => 'required',
            'renewable_type' => 'required',
            'date' => 'required',
            'amount' => 'required'
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        Renew::query()->create($request->all());
        return redirect('admin/renew')->with('success', 'Data added Successfully!');
    }

    public function edit($id)
    {
        $renew = Renew::query()->findOrFail($id);
        return view('domainhosting::renew.edit',compact('renew'));
    }

    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(),[
            'renewable_id' => 'required',
            'renewable_type' => 'required',
            'date' => 'required',
            'amount' => 'required'
        ]);
        $renew = Renew::query()->findOrFail($id);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $renew->update($request->all());
        return redirect('admin/renew')->with('success', 'Renew Updated Successfully!');
    }

    public function destroy($id)
    {
        $renew = Renew::query()->findOrFail($id);
        $renew->delete();
        return redirect('admin/renew')->with('success', 'Data Deleted Successfully!');
    }
}
