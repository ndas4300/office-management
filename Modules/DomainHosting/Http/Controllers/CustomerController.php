<?php

namespace Modules\DomainHosting\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\DomainHosting\Entities\Customer;
use function redirect;
use function view;

class CustomerController extends Controller
{
    public function index()
    {
        $customers = Customer::query()->paginate(10);
        return view('domainhosting::customer.index',compact('customers'));
    }

    public function search(Request $request)
    {
        $customers = Customer::query()
            ->where('name','like','%'.$request->get('q').'%')
            ->paginate(10);

        return view('domainhosting::customer.searchCustomer',compact('customers'));
    }

    public function create()
    {
        return view('domainhosting::customer.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'address' => 'required',
            'mobile' => 'required',
            'contact_person' => 'required',
            'contact_person_phone' => 'required'
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        Customer::query()->create($request->all());
        return redirect('admin/customer')->with('success', 'Customer added!');
    }

    public function edit($id)
    {
        $customer = Customer::query()->findOrFail($id);
        return view('domainhosting::customer.edit',compact('customer'));
    }

    public function update($id, Request $request)
    {
        // dd($request->all());
        $customer = Customer::query()->findOrFail($id);
        $customer->update($request->all());
        return redirect('admin/customer')->with('success', 'Customer Updated!');
    }

    public function destroy($id)
    {
        $customer = Customer::query()->findOrFail($id);
        $customer->delete();
        return redirect('admin/customer')->with('success', 'Customer Deleted!');
    }
}
