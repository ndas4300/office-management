<?php

namespace Modules\DomainHosting\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\DomainHosting\Entities\Provider;

class ProviderController extends Controller
{

    public function index()
    {
        $providers = Provider::query()->paginate(10);
        return view('domainhosting::provider.index',compact('providers'));
    }

    public function create()
    {
        return view('domainhosting::provider.add-provider');
    }

    public function store(Request $request)
    {
        Provider::query()->create($request->all());
        return redirect('admin/provider')->with('success', 'Provider added!');
    }

    public function edit($id)
    {
        $provider = Provider::query()->findOrFail($id);
        return view('domainhosting::provider.edit',compact('provider'));
    }

    public function update($id, Request $request)
    {
        $provider = Provider::query()->findOrFail($id);
        $provider->update($request->all());
        return redirect('admin/provider')->with('success', 'Provider Updated!');
    }

    public function destroy($id)
    {
        $provider = Provider::query()->findOrFail($id);
        $provider->delete();
        return redirect('admin/provider')->with('success', 'Provider Deleted!');
    }
}
