<?php

namespace Modules\DomainHosting\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\DomainHosting\Entities\Hosting;
use Modules\DomainHosting\Entities\Provider;

class HostingController extends Controller
{
    public function index()
    {
        $hosting = Hosting::query()->paginate(10);
        return view('domainhosting::hosting.index',compact('hosting'));
    }

    public function create()
    {
        $providers = Provider::query()->pluck('name','id');
        return view('domainhosting::hosting.add-hosting',compact('providers'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'provider_id' => 'required',
            'package' => 'required',
            'amount' => 'required',
            'registration_date' => 'required',
            'expire_date' => 'required',
            'note' => 'required'
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        Hosting::query()->create($request->all());
        return redirect('admin/hosting')->with('success', 'Hosting added!');
    }

    public function edit($id)
    {
        $providers = Provider::query()->pluck('name','id');
        $hosting = Hosting::query()->findOrFail($id);
        return view('domainhosting::hosting.edit',compact('hosting','providers'));
    }

    public function update($id, Request $request)
    {
        $hosting = Hosting::query()->findOrFail($id);
        $hosting->update($request->all());
        return redirect('admin/hosting')->with('success', 'Hosting Updated!');
    }

    public function destroy($id)
    {
        $hosting = Hosting::query()->findOrFail($id);
        $hosting->delete();
        return redirect('admin/hosting')->with('success', 'Hosting Deleted!');
    }
}
