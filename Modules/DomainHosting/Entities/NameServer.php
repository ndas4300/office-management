<?php

namespace Modules\DomainHosting\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NameServer extends Model
{
    use HasFactory;

    protected $fillable = ['name','ip','is_active'];
}
