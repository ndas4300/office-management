<?php

namespace Modules\Accounts\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CoaGreatGrandparent extends Model
{
    use HasFactory;

    protected $fillable = ['name'];
}
