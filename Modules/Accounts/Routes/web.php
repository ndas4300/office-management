<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//Route::prefix('accounts')->group(function() {
//    Route::get('/', 'AccountsController@index');
//});

use Modules\Accounts\Http\Controllers\AccountingController;
use Modules\Accounts\Http\Controllers\ChartOfAccountController;
use Modules\Accounts\Http\Controllers\JournalController;

Route::prefix('admin')->middleware(['auth', 'verified'])->group(function () {
    //Route for accounts starts here
    Route::get('coa',[ChartOfAccountController::class,'index'])->name('coa.index');
    Route::get('coa/create',[ChartOfAccountController::class,'create'])->name('coa.create');
    Route::post('coa/store',[ChartOfAccountController::class,'store'])->name('coa.store');
    Route::get('coa/edit/{id}',[ChartOfAccountController::class,'edit'])->name('coa.edit');
    Route::patch('coa/update/{id}',[ChartOfAccountController::class,'update'])->name('coa.update');
    Route::delete('coa/destroy/{id}',[ChartOfAccountController::class,'destroy'])->name('coa.destroy');
    Route::post('coa/status',[ChartOfAccountController::class,'isEnabled'])->name('coa.isEnabled');
    Route::post('coa',[ChartOfAccountController::class,'list'])->name('coa.list');

    // journal routes starts here
    Route::resource('journals', JournalController::class)->middleware('auth');
    Route::get('journal/classic',[JournalController::class,'classic'])->name('journals.classic');
    Route::get('cash-book',[AccountingController::class],'cashBook')->name('accounts.cash-book');
    Route::post('cash-book-settings',[AccountingController::class,'cashBookSetting'])->name('accounts.cashBookSetting');
    Route::get('ledger',[AccountingController::class,'ledger'])->name('accounts.ledger');
    Route::get('trial-balance',[AccountingController::class,'trialBalance'])->name('accounts.trialBalance');
    Route::get('profit-n-loss',[AccountingController::class,'profitNLoss'])->name('accounts.profitNLoss');
    Route::get('balance-sheet',[AccountingController::class,'balanceSheet'])->name('accounts.balanceSheet');
    // journal routes ends here

    //Playlist routes starts here
    Route::get('playlists',[PlaylistController::class,'index'])->name('playlists.index');
    Route::post('playlist/store',[PlaylistController::class,'store'])->name('playlists.store');
    Route::get('playlist/show/{id}',[PlaylistController::class,'show'])->name('playlists.show');
    Route::delete('playlist/destroy/{id}',[PlaylistController::class,'destroy'])->name('playlists.destroy');
    //Playlist routes Ends here

    //Communication Routes starts here
    Route::get('communication/quick',[CommunicationController::class,'quick'])->name('communication.quick');
    Route::get('communication/student',[CommunicationController::class,'student'])->name('communication.student');
    Route::get('communication/staff',[CommunicationController::class,'staff'])->name('communication.staff');
    Route::get('communication/history',[CommunicationController::class,'history'])->name('communication.history');

    Route::post('communication/send',[CommunicationController::class,'send']);
    Route::post('communication/quick/send',[CommunicationController::class,'quickSend']);
    //End Communication Route
});