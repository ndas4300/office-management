<?php

namespace Modules\Accounts\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Accounts\Entities\CommunicationSetting;

class CommunicationSettingController extends Controller
{

//    public function index()
//    {
//        return view('hrm::communication.apiSetting.apiSetting');
//    }
//
//    public function store(Request $request)
//    {
//        CommunicationSetting::query()->create($request->all());
//        return redirect('admin/communication/apiSetting');
//    }

    public function index()
    {
        $apiData = CommunicationSetting::query()->first();
        //dd($apiData);
        return view('hrm::communication.api-settings',compact('apiData'));
    }

    public function update(Request $request)
    {
        $data = CommunicationSetting::query()->first();
        $data->update($request->all());
        return redirect('admin/communication/apiSetting')->with('success','Updated successfully');

    }

//    public function destroy($id)
//    {
//        $data = CommunicationSetting::query()->findOrFail($id);
//        $data->delete();
//        return redirect('admin/communication/apiSetting');
//    }
}
