<?php

namespace Modules\Hrm\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\Hrm\Entities\Attendance;
use Modules\Hrm\Entities\EarnLeave;
use Modules\Hrm\Entities\Employee;
use Modules\Hrm\Entities\Leave;
use Modules\Hrm\Entities\LeaveCategory;

class LeaveController extends Controller
{
    public function index()
    {
        $leaves = Leave::query()->orderBy('leave_from')->paginate(10);
        $employee = Employee::query()->pluck('employee_no','id');
        $leaveCategories = LeaveCategory::query()->pluck('name','id');
        return view('hrm::leave-management.leave.index',compact('leaves','leaveCategories','employee'));
    }

    public function create()
    {
        $employee = Employee::query()->pluck('name','id');
        $leaveCategories = LeaveCategory::query()->pluck('name','id');
        $users=User::pluck('name','id');
        return view('hrm::leave-management.leave.add',compact('employee','leaveCategories','users'));
    }
    public function search(Request $request)
    {
        $leaves = Leave::query()
            ->where('leave_from','like','%'.$request->get('q').'%')
            ->where('leave_to','like','%'.$request->get('q').'%')
            ->whereHas('employee', function($q) use ($request){
                    $q->where('name','like','%'.$request->get('q').'%');
                })
            ->paginate(10);

        return view('hrm::leave-management.leave.searchLeave',compact('leaves'));
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'employee_id'       => ['required','integer'],
            'leave_category_id' => ['required','integer'],
            'leave_from'        => ['required','date'],
            'leave_to'          => ['required','date'],
            'approved_by'       => ['required','integer'],
        ]);
        $from= new Carbon($request->leave_from);
        $to= new Carbon($request->leave_to);
        $days= $from->diffInDays($to) +1;
        $validated['days'] = $days;
        $validated['date'] = today();
        $leave=Leave::create($validated);


        Attendance::query()
                ->where('employee_id', $request->employee_id)
                ->whereBetween('date',  [$request->leave_from,$request->leave_to])
                ->update(['attendance_status_id' => 7]);

        if ($leave) {
            $earnLeave=EarnLeave::where('employee_id',$request->employee_id)->orderBy('id','Desc')->first();
            if ($earnLeave){
                $remaining= $earnLeave->balance-$days;
                $earnLeave->balance= $remaining;
                $earnLeave->update();
            }
        }
        return redirect('admin/leave')->with('success', 'Leave added Successfully!');
    }

    public function edit(Leave $leave)
    {
        $employees = Employee::pluck('name','id');
        $leaveCategories = LeaveCategory::pluck('name','id');
        $users = User::pluck('name','id');
        return view('hrm::leave-management.leave.edit',compact('users','leave','employee','leaveCategories'));
    }

    public function update(Request $request, Leave $leave)
    {
        $validated = $request->validate([
            'employee_id'       => ['required','integer'],
            'leave_category_id' => ['required','integer'],
            'leave_from'        => ['required','date'],
            'leave_to'          => ['required','date'],
            'approved_by'       => ['required','integer'],
        ]);
        $from= new Carbon($request->leave_from);
        $to= new Carbon($request->leave_to);
        $days= $from->diffInDays($to)+1;
        $validated['days'] = $days;
        $leave->update($validated);

        Attendance::query()
        ->where('employee_id', $request->employee_id)
        ->whereBetween('date',  [$request->leave_from,$request->leave_to])
        ->update(['attendance_status_id' => 7]);

        if ($leave) {
            $earnLeave=EarnLeave::where('employee_id',$request->employee_id)->orderBy('id','Desc')->first();
            if ($earnLeave){
                $remaining= $earnLeave->balance-$days;
                $earnLeave->balance= $remaining;
                $earnLeave->update();
            }
        }
        return redirect('admin/leave')->with('success', 'Leave Updated Successfully!');
    }

    public function destroy(Leave $leave)
    {
        $leave->delete();
        return redirect('admin/leave')->with('success', 'Leave Deleted Successfully!');
    }
}
