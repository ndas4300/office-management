<?php

namespace Modules\Hrm\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Hrm\Entities\AcademicResult;
use Modules\Hrm\Entities\BloodGroup;
use Modules\Hrm\Entities\Country;
use Modules\Hrm\Entities\Day;
use Modules\Hrm\Entities\Education;
use Modules\Hrm\Entities\Employee;
use Modules\Hrm\Entities\EmployeeAcademic;
use Modules\Hrm\Entities\EmployeeAddress;
use Modules\Hrm\Entities\EmployeeExperience;
use Modules\Hrm\Entities\EmployeeOfficial;
use Modules\Hrm\Entities\EmployeeStatus;
use Modules\Hrm\Entities\EmployeeTraining;
use Modules\Hrm\Entities\Gender;
use Modules\Hrm\Entities\MaritalStatus;
use Modules\Hrm\Entities\Religion;
use Modules\Hrm\Entities\WeeklyOff;
use Modules\OfficeSetup\Entities\Calender;
use Modules\OfficeSetup\Entities\Department;
use Modules\OfficeSetup\Entities\Designation;
use Modules\OfficeSetup\Entities\Shift;
use Modules\OfficeSetup\Entities\Status;

class EmployeeController extends Controller
{

    public function search(Request $request)
    {
        // dd('asdkjaslkj');
        $employees = Employee::query()
            ->where('name', 'like', '%' . $request->get('q') . '%')
            ->orWhere('employee_no', 'like', '%' . $request->get('q') . '%')
            ->paginate(10);

        return view('hrm::employee-management.searchEmployee', compact('employees'));
    }

    public function index()
    {
        $employees = Employee::query()->paginate(10);
        return view('hrm::employee-management.index', compact('employees'));
    }

    public function create()
    {
        $days = Day::query()->pluck('name', 'id');
        $education = Education::query()->pluck('name', 'id');
        $academicResult = AcademicResult::query()->pluck('name', 'id');
        $countries = Country::query()->pluck('name', 'id');
        $gender = Gender::query()->pluck('name', 'id');
        $maritalStatus = MaritalStatus::query()->pluck('name', 'id');
        $bloodGroup = BloodGroup::query()->pluck('name', 'id');
        $religion = Religion::query()->pluck('name', 'id');
        $departments = Department::query()->pluck('name', 'id');
        $designations = Designation::query()->pluck('name', 'id');
        $shifts = Shift::query()->pluck('name', 'id');
        $calendars = Calender::query()->pluck('name', 'id');
        $status = Status::query()->pluck('name', 'id');
        return view('hrm::employee-management.add', compact(
            'gender',
            'maritalStatus',
            'bloodGroup',
            'religion',
            'countries',
            'education',
            'academicResult',
            'days',
            'departments',
            'designations',
            'shifts',
            'calendars',
            'status'
        ));
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'employee_no' => 'required',
            'name' => 'required',
            'bn_name' => 'required',
            'dob' => 'required',
            'marital_status_id' => 'required',
            'blood_group_id' => 'required',
        ]);

            if ($request->has('image')) {
                $data['image'] = uploadFile($request->file('image'), 'employee/image');
            }
            if ($request->has('nid')) {
                $data['nid'] = uploadFile($request->file('nid'), 'employee/nid');
            }
            if ($request->has('nid_back')) {
                $data['nid_back'] = uploadFile($request->file('nid_back'), 'employee/nid_back');
            }
            
            
            
            $employee=Employee::create($data);

        EmployeeOfficial::create([
            'employee_id' => $employee->id,
            'department_id' => $request->department_id,
            'designation_id' => $request->designation_id,
            'shift_id' => $request->shift_id,
            'calender_code_id' => $request->calender_code_id,
            'joining_date' => $request->joining_date,
            'gross' => $request->gross
        ]);

        EmployeeAddress::create([
            'employee_id' => $employee->id,
            'pr_address_line_one' => $request->pr_address_line_one,
            'pr_address_line_two' => $request->pr_address_line_two,
            'pr_phone_one' => $request->pr_phone_one,
            'pr_phone_two' => $request->pr_phone_two,
            'pr_email' => $request->pr_email,
            'pr_village' => $request->pr_village,
            'pr_police_station' => $request->pr_police_station,
            'pr_post_office' => $request->pr_post_office,
            'pr_city_id' => $request->pr_city_id,
            'pr_country_id' => $request->pr_country_id,
            'pa_address_line_one' => $request->pa_address_line_one,
            'pa_address_line_two' => $request->pa_address_line_two,
            'pa_phone_one' => $request->pa_phone_one,
            'pa_phone_two' => $request->pa_phone_two,
            'pa_email' => $request->pa_email,
            'pa_village' => $request->pa_village,
            'pa_police_station' => $request->pa_police_station,
            'pa_post_office' => $request->pa_post_office,
            'pa_city_id' => $request->pa_city_id,
            'pa_country_id' => $request->pa_country_id
        ]);

        EmployeeAcademic::create([
            'employee_id' => $employee->id,
            'education_id' => $request->education_id,
            'degree_id' => $request->degree_id,
            'major' => $request->major,
            'institute' => $request->institute,
            'result_id' => $request->result_id,
            'marks' => $request->marks,
            'year' => $request->year,
            'duration' => $request->duration,
            'achievement' => $request->achievement
        ]);

        EmployeeExperience::create([
            'employee_id' => $employee->id,
            'company' => $request->company,
            'business' => $request->business,
            'designation' => $request->designation,
            'area_of_experience' => $request->area_of_experience,
            'experience_location' => $request->experience_location,
            'experience_start' => $request->experience_start,
            'experience_end' => $request->experience_end
        ]);

        EmployeeStatus::create([
            'employee_id' => $employee->id,
            'status_id' => $request->status_id,
            'date' => $request->date,
            'description' => $request->description,
        ]);

        if ($request->has('training_title')){
            EmployeeTraining::create([
                'employee_id' => $employee->id,
                'training_title' => $request->training_title,
                'topics_covered' => $request->topics_covered,
                'training_institute' => $request->training_institute,
                'training_year' => $request->training_year,
                'training_duration' => $request->training_duration,
                'training_location' => $request->training_location,
                'training_country_id' => $request->training_country_id
            ]);
        }

        $employee->days()->attach($request->day_id);


        return redirect('admin/employee')->with('success', 'Employee Information added Successfully!');
    }

    public function show($id)
    {
        $employee = Employee::query()->findOrFail($id);
        $employeeOfficial = EmployeeOfficial::query()->where('employee_id', $id)->get();
        $employeeAddress = EmployeeAddress::query()->where('employee_id', $id)->get();
        $employeeAcademic = EmployeeAcademic::query()->where('employee_id', $id)->get();
        $employeeExperience = EmployeeExperience::query()->where('employee_id', $id)->get();

        $employeeStatus = EmployeeStatus::query()->where('employee_id', $id)->get();
        $employeeTraining = EmployeeTraining::query()->where('employee_id', $id)->get();
        $employeeOff = WeeklyOff::query()->where('employee_id', $id)->get();
        //        dd($employee);
        return view('hrm::employee-management.employee', compact(
            'employee',
            'employeeOfficial',
            'employeeAddress',
            'employeeAcademic',
            'employeeExperience',
            'employeeStatus',
            'employeeTraining',
            'employeeOff'
        ));
    }

    public function edit($id)
    {
        $employee = Employee::query()->findOrFail($id);
        $employeeOfficial = EmployeeOfficial::query()->where('employee_id', $id)->first();
        $employeeAddress = EmployeeAddress::query()->where('employee_id', $id)->first();
        $employeeAcademic = EmployeeAcademic::query()->where('employee_id', $id)->first();
        $employeeExperience = EmployeeExperience::query()->where('employee_id', $id)->first();
        $employeeStatus = EmployeeStatus::query()->where('employee_id', $id)->first();
        $employeeTraining = EmployeeTraining::query()->where('employee_id', $id)->first();
        $employeeOff = WeeklyOff::query()->where('employee_id', $id)->first();

        $gender = Gender::query()->pluck('name', 'id');
        $maritalStatus = MaritalStatus::query()->pluck('name', 'id');
        $bloodGroup = BloodGroup::query()->pluck('name', 'id');
        $religion = Religion::query()->pluck('name', 'id');
        $countries = Country::query()->pluck('name', 'id');
        $departments = Department::query()->pluck('name', 'id');
        $designations = Designation::query()->pluck('name', 'id');
        $shifts = Shift::query()->pluck('name', 'id');
        $calendars = Calender::query()->pluck('name', 'id');
        $status = Status::query()->pluck('name', 'id');
        $education = Education::query()->pluck('name', 'id');
        $academicResult = AcademicResult::query()->pluck('name', 'id');
        $days = Day::query()->pluck('name', 'id');
        // dd($employee->nid);

        return view(
            'hrm::employee-management.edit-employee',
            compact(
                'employee',
                'employeeOfficial',
                'employeeAddress',
                'employeeAcademic',
                'employeeExperience',
                'employeeStatus',
                'employeeTraining',
                'employeeOff',
                'gender',
                'maritalStatus',
                'bloodGroup',
                'religion',
                'countries',
                'departments',
                'designations',
                'shifts',
                'calendars',
                'status',
                'education',
                'academicResult',
                'days'
            )
        );
    }

    public function update(Employee $employee, Request $request)
    {
        //    dd($request->all());
        $data = $request->validate([
            'employee_no' => 'required',
            'name' => 'required',
            'bn_name' => 'required',
            'dob' => 'required',
            'marital_status_id' => 'required',
            'blood_group_id' => 'required',
        ]);

            if ($request->has('image')) {
                $data['image'] = uploadFile($request->file('image'), 'employee/image');
            }
            if ($request->has('nid')) {
                $data['nid'] = uploadFile($request->file('nid'), 'employee/nid');
            }
            if ($request->has('nid_back')) {
                $data['nid_back'] = uploadFile($request->file('nid_back'), 'employee/nid_back');
            }
            
            
            
            $employee->update($data);

        //        dd($employee);

        EmployeeOfficial::query()->where('employee_id', $employee->id)->update([
            'employee_id' => $employee->id,
            'department_id' => $request->department_id,
            'designation_id' => $request->designation_id,
            'shift_id' => $request->shift_id,
            'calender_code_id' => $request->calender_code_id,
            'joining_date' => $request->joining_date,
            'gross' => $request->gross
        ]);
        $add = EmployeeAddress::query()->where('employee_id', $employee->id)->update([
            'employee_id' => $employee->id,
            'pr_address_line_one' => $request->pr_address_line_one,
            'pr_address_line_two' => $request->pr_address_line_two,
            'pr_phone_one' => $request->pr_phone_one,
            'pr_phone_two' => $request->pr_phone_two,
            'pr_email' => $request->pr_email,
            'pr_village' => $request->pr_village,
            'pr_police_station' => $request->pr_police_station,
            'pr_post_office' => $request->pr_post_office,
            'pr_city_id' => $request->pr_city_id,
            'pr_country_id' => $request->pr_country_id,
            'pa_address_line_one' => $request->pa_address_line_one,
            'pa_address_line_two' => $request->pa_address_line_two,
            'pa_phone_one' => $request->pa_phone_one,
            'pa_phone_two' => $request->pa_phone_two,
            'pa_email' => $request->pa_email,
            'pa_village' => $request->pa_village,
            'pa_police_station' => $request->pa_police_station,
            'pa_post_office' => $request->pa_post_office,
            'pa_city_id' => $request->pa_city_id,
            'pa_country_id' => $request->pa_country_id
        ]);
        //        dd($add);

        EmployeeAcademic::query()->where('employee_id', $employee->id)->update([
            'employee_id' => $employee->id,
            'education_id' => $request->education_id,
            'degree_id' => $request->degree_id,
            'major' => $request->major,
            'institute' => $request->institute,
            'result_id' => $request->result_id,
            'marks' => $request->marks,
            'year' => $request->year,
            'duration' => $request->duration,
            'achievement' => $request->achievement
        ]);

        EmployeeExperience::query()->where('employee_id', $employee->id)->update([
            'employee_id' => $employee->id,
            'company' => $request->company,
            'business' => $request->business,
            'designation' => $request->designation,
            'area_of_experience' => $request->area_of_experience,
            'experience_location' => $request->experience_location,
            'experience_start' => $request->experience_start,
            'experience_end' => $request->experience_end
        ]);

        EmployeeStatus::query()->where('employee_id', $employee->id)->update([
            'employee_id' => $employee->id,
            'status_id' => $request->status_id,
            'date' => $request->date,
            'description' => $request->description,
        ]);

        if ($request->has('training_title')) {
            EmployeeTraining::query()->where('employee_id', $employee->id)->update([
                'employee_id' => $employee->id,
                'training_title' => $request->training_title,
                'topics_covered' => $request->topics_covered,
                'training_institute' => $request->training_institute,
                'training_year' => $request->training_year,
                'training_duration' => $request->training_duration,
                'training_location' => $request->training_location,
                'training_country_id' => $request->training_country_id
            ]);
        }

        $employee->days()->sync($request->day_id);

        return redirect()->route('employee.index')->with('success', 'Employee Updated Successfully!');
    }

    public function destroy($id)
    {
        $employee = Employee::query()->findOrFail($id);
        $employee->delete();
        return redirect()->back()->with('success', 'Employee Deleted Successfully!');
    }
}
