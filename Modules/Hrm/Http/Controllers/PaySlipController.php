<?php

namespace Modules\Hrm\Http\Controllers;

use App\Http\Controllers\Controller;
use Modules\Accounts\Entities\PaySlip;
use Illuminate\Http\Request;
use Modules\Hrm\Entities\Attendance;
use Modules\Hrm\Entities\Employee;

class PaySlipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees=Employee::query()
        ->whereHas('statuses', function ($q) {
            $q->where('status_id', 1);
        })->get();
        return view('hrm::paySlip.paySlip',compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function generate(Request $request)
    {
        $month=now()->subMonth()->format('m');
        $year=now()->format('Y');
        $employees=Employee::whereIn('id',$request->some_key)->get();
//        $employees=Employee::with(['attendances'])->whereIn('id',$request->some_key)
//            ->whereHas('attendances', function($q) use ($month,$year){
//                $q->whereYear('date',$year)
//                    ->whereMonth('date',  $month)
//                    ->orderBy('date','asc');
//        })->get();

        $dayOfMonth = cal_days_in_month(null,$month,$year);

        $attendence = Attendance::whereYear('date',$year)
            ->whereMonth('date',$month)
            ->orderBy('date','asc')
            ->get();
        $days_attend=$attendence->whereNotIn('attendance_status_id',[2,5,6,7]);
        $workingDays=$attendence->whereNotIn('attendance_status_id',[5,6]);

        $late               = $attendence->where('attendance_status_id',3);
        $absent             = $attendence->where('attendance_status_id',2);
        $earlyLeave         = $attendence->where('attendance_status_id',4);
        $present            = $attendence->where('attendance_status_id',1);
        $holiday            = $attendence->where('attendance_status_id',5);
        $weeklyOff          = $attendence->where('attendance_status_id',6);
        $leave              = $attendence->where('attendance_status_id',7);
        $earlyLeaveLate     = $attendence->where('attendance_status_id',8);
        return view('hrm::paySlip.data',compact('employees','dayOfMonth','workingDays','days_attend','late','absent','earlyLeave','present','holiday','weeklyOff','leave','earlyLeaveLate'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PaySlip  $paySlip
     * @return \Illuminate\Http\Response
     */
    public function show(PaySlip $paySlip)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PaySlip  $paySlip
     * @return \Illuminate\Http\Response
     */
    public function edit(PaySlip $paySlip)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PaySlip  $paySlip
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaySlip $paySlip)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PaySlip  $paySlip
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaySlip $paySlip)
    {
        //
    }
}
