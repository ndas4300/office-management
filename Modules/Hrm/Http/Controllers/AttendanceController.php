<?php

namespace Modules\Hrm\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Modules\Hrm\Entities\Attendance;
use Modules\Hrm\Entities\Employee;
use Modules\Hrm\Entities\Leave;
use Modules\Hrm\Entities\RawAttendance;
use Modules\OfficeSetup\Entities\CalendarEvent;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // For Single Date
        $i = 20;

        $today = \Carbon\Carbon::parse('2022-06-' . $i);
        // For Single Date

        // $today = Carbon::today();
        //$dayOfMonth = cal_days_in_month(null,'4',2022);
        // for ($i = 1; $i < 31; $i++) {

        // dd($today);
        $employees = Employee::whereHas('statuses', function ($q) {
            $q->where('status_id', 1);
        })->whereHas('officials', function ($q) use ($today) {
            $q->whereDate('joining_date', '<=', $today);
        })->get();
        // dd($employees);

        foreach ($employees as $employee) {
            $rawData = RawAttendance::query()
                ->where('access_date', $today->format('Y-m-d'))
                ->where('registration_id', $employee->employee_no)
                ->get();

            // dd($rawData->count());
            // dd($rawData->isEmpty());

            if ($rawData->isEmpty()) { // record not exists in raw database
                // Checking for leave if any
                // dd('empty');
                $min = null;
                $max = null;
                $leave = Leave::query()
                    ->where('employee_id', $employee->id)
                    ->where('leave_from', '<=', $today->format('Y-m-d'))
                    ->where('leave_to', '>=', $today->format('Y-m-d'))
                    ->exists();

                // Checking of holiday if any
                $calendarId = $employee->officials->calendar_id;
                // dd($today->format('Y-m-d'));
                $holiday = CalendarEvent::query()
                    ->where('calendar_id', $calendarId)
                    ->where('start', '<=', $today->format('Y-m-d'))
                    ->where('end', '>=', $today->format('Y-m-d'))
                    ->where('is_holiday', 1)
                    ->exists();
                // dd($holiday);

                // Weekly Off
                foreach ($employee->days as $key => $weeklyOff) {
                    if ($weeklyOff = $weeklyOff->id == $today->format('N')) {
                        break;
                    }
                }
                // dd($holiday);
                // echo $holiday;

                if ($holiday) {
                    $attendanceStatus = '5';
                } elseif ($leave) {
                    $attendanceStatus = '7';
                } elseif ($weeklyOff == true) {
                    $attendanceStatus = '6';
                } else {
                    $attendanceStatus = '2';
                }
            } else {
                // dd('not empty');
                $min = $rawData->min('access_time')->format('H:i:s');
                $max = $rawData->max('access_time')->format('H:i:s');

                $shift = $employee->officials->first()->shift;
                $shiftIn =  Carbon::parse($shift->in)->addMinutes($shift->grace)->format('H:i:s');
                $shiftOut = Carbon::parse($shift->out)->format('H:i:s');
                // dd($min <= $shiftIn && $max <= $shiftOut);

                if ($min >= $shiftIn && $max <= $shiftOut) {
                    $attendanceStatus = '8';
                } elseif ($min <= $shiftIn && $max <= $shiftOut) {
                    $attendanceStatus = '4';
                } elseif ($min <= $shiftIn) {
                    $attendanceStatus = '1';
                } elseif ($min > $shiftIn) {
                    $attendanceStatus = '3';
                }
            }

            // $min = $rawData->min('access_time') ?? '00.00';
            // $max = $rawData->max('access_time') ?? '00.00';
            // // dd($max);
            // $shift = $employee->officials->first()->shift;
            // $shiftIn =  Carbon::parse($shift->in)->addMinutes($shift->grace)->format('h:i');
            // $shiftOut = $shift->out;

            // // Weekly Off
            // foreach ($employee->days as $key => $weeklyOff) {
            //     if ($weeklyOff = $weeklyOff->id == $today->format('N')) {
            //         break;
            //     }
            // }
            // // Holiday
            // $calendarId = $employee->officials->calendar_id;
            // $holiday = CalendarEvent::query()
            //     ->where('calendar_id', $calendarId)
            //     ->where('start', '<=', $today->format('Y-m-d'))
            //     ->where('end', '>=', $today->format('Y-m-d'))
            //     ->where('is_holiday', 1)
            //     ->exists();

            // // Leave Management
            // $leave = Leave::query()
            //     ->where('employee_id', $employee->id)
            //     ->where('leave_from', '<=', $today->format('Y-m-d'))
            //     ->where('leave_to', '>=', $today->format('Y-m-d'))
            //     ->exists();

            // // Attendance Status
            // if ($leave) {
            //     $attendanceStatus = '7';
            // } elseif ($min == '00.00') {
            //     $attendanceStatus = '2';
            // } elseif ($min <= $shiftIn && $max <= $shiftOut) {
            //     $attendanceStatus = '4';
            // } elseif ($min > $shiftIn) {
            //     $attendanceStatus = '3';
            // } elseif ($min <= $shiftIn) {
            //     $attendanceStatus = '1';
            // } elseif ($holiday) {
            //     $attendanceStatus = 5;
            // } elseif ($weeklyOff == true) {
            //     $attendanceStatus = 6;
            // }

            // $attendanceWeeklyOffs = DB::table('employee_weekly_off')->where('employee_id', $employee->id)->get();
            // foreach ($attendanceWeeklyOffs as $attendanceWeeklyOff) {
            //     AttendanceWeeklyOff::create([
            //         'day_id' => $attendanceWeeklyOff->day_id,
            //         'employee_id' => $attendanceWeeklyOff->employee_id,
            //     ]);
            // }

            $leaveCategory = Leave::query()
                ->where('employee_id', $employee->id)
                ->where('leave_from', '<=', $today->format('Y-m-d'))
                ->where('leave_to', '>=', $today->format('Y-m-d'))
                ->pluck('leave_category_id');

            $data = [
                'employee_id' => $employee->id,
                'date' => $today->format('Y-m-d'),
                'in_time' => $min,
                'out_time' => $max,
                'department_id' => $employee->officials->first()->department_id,
                'designation_id' => $employee->officials->first()->designation_id,
                'leave_category_id' => $leaveCategory,
                'attendance_status_id' => $attendanceStatus,
                'weekly_off_id' => $employee->officials->first()->weekly_off_id,
            ];

            $attendanceExists = Attendance::query()
                ->where('employee_id', $employee->id)
                ->where('date', $today->format('Y-m-d'))
                ->exists();

            if ($attendanceExists) {
                $attendance = Attendance::query()
                    ->where('employee_id', $employee->id)
                    ->where('date', $today->format('Y-m-d'))
                    ->first();

                $attendance->update($data);
            } else {
                Attendance::create($data);
            }
        }

        // }


        $attendances = Attendance::query()->orderBy('date')->with(['attendancestatus'])->paginate(10);
        return view('hrm::attendance-management.attendance.index', compact('attendances'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        // $startDate = today()->subWeek();
        $startDate = "2022-03-10";
        // $endDate = today();
        $endDate = "2022-03-10";

        $rawAccessId = RawAttendance::query()->where('access_id', '<>', null)->exists();

        // if ($rawAccessId) {
        //     $accessId = RawAttendance::query()->max('access_id');
        // } else {
        //     $accessId = "00000000";
        // }
        $accessId = "00000000";
        $data = array(
            "operation" => "fetch_log",
            "auth_user" => "webpoint",
            "auth_code" => "3efd234cefa324567a342deafd32672",
            "start_date" => $startDate,
            "end_date" => $endDate,
            "start_time" => "00:00:00",
            "end_time" => "23:59:59",
            "access_id" => "$accessId"
        );
        $datapayload = json_encode($data);
        $client = new Client();
        $response = $client->request('POST', "https://rumytechnologies.com/rams/json_api", ['body' => $datapayload]);
        $body = $response->getBody();
        $replace_syntax = str_replace('{"log":', "", $body);

        $replace_syntax = substr($replace_syntax, 0, -1);
        $responseBody = json_decode($replace_syntax);
        // dd($responseBody);

        foreach ($responseBody as $data) {
            RawAttendance::query()->create([
                'unit_name'         => $data->unit_name,
                'registration_id'   => $data->registration_id,
                'access_id'         => $data->access_id,
                'department'        => $data->department,
                'access_time'       => $data->access_time,
                'access_date'       => $data->access_date,
                'user_name'         => $data->user_name,
                'unit_id'           => $data->unit_id,
                'card'              => $data->card,
            ]);
        }
        dd('done');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \Modules\Hrm\Entities\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function show(Attendance $attendance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Modules\Hrm\Entities\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function edit(Attendance $attendance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Modules\Hrm\Entities\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attendance $attendance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Modules\Hrm\Entities\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attendance $attendance)
    {
        //
    }
}
