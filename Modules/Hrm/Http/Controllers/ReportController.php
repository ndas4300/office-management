<?php

namespace Modules\Hrm\Http\Controllers;

use App\Http\Controllers\Controller;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Modules\Hrm\Entities\Attendance;
use Modules\Hrm\Entities\Employee;
use Modules\OfficeSetup\Entities\Department;

class ReportController extends Controller
{
    protected function createpdf($vw,$ppdf){

    }
    public function get_monthlyview(){

        $year = Attendance::query()
            ->select(DB::raw('year(date) as year'))
            ->groupBy(DB::raw('year(date)'))
            ->pluck('year')
            ->toArray();
        $employee = Employee::query()->whereHas('statuses', function($q){
            $q->where('status_id', 1);
        })->pluck('name','id');

        $month = [
            1 => 'January',
            2 => 'February',
            3 => 'March',
            4 => 'April',
            5 => 'May',
            6 => 'June',
            7 => 'July',
            8 => 'August',
            9 => 'September',
            10 => 'October',
            11 => 'November',
            12 => 'December'
        ];
        $qr =null;
        return view('hrm::attendance-management.report.monthlyreport',compact('year','month','employee','qr'));
    }
    public function get_monthdata(Request $request){
        $year = Attendance::query()
            ->select(DB::raw('year(date) as year'))
            ->groupBy(DB::raw('year(date)'))
            ->pluck('year')
            ->toArray();
        $employee = Employee::query()->pluck('name','id');
        $month = [
            1 => 'January',
            2 => 'February',
            3 => 'March',
            4 => 'April',
            5 => 'May',
            6 => 'June',
            7 => 'July',
            8 => 'August',
            9 => 'September',
            10 => 'October',
            11 => 'November',
            12 => 'December'
        ];

        $employee_data = Employee::where('id',$request->employee_id)->get();
        $attendence = Attendance::whereYear('date',$request->year)
            ->whereMonth('date',$request->month)
            ->where('employee_id',$request->employee_id)
            ->orderBy('date','asc')
            ->get();
        $qr =[
            'emp_id'  => $request->employee_id,
            'year'    => $request->year,
            'month'   => $request->month
        ];



        $late               = $attendence->where('attendance_status_id',3)->count();
        $absent             = $attendence->where('attendance_status_id',2)->count();
        $earlyLeave         = $attendence->where('attendance_status_id',4)->count();
        $present            = $attendence->where('attendance_status_id',1)->count();
        $holiday            = $attendence->where('attendance_status_id',5)->count();
        $weeklyOff          = $attendence->where('attendance_status_id',6)->count();
        $leave              = $attendence->where('attendance_status_id',7)->count();
        $earlyLeaveLate     = $attendence->where('attendance_status_id',8)->count();

        return view('hrm::attendance-management.report.monthlyreport',compact('year','month','employee','employee_data',
        'attendence','qr','late','absent','earlyLeave','present','holiday','weeklyOff','leave','earlyLeaveLate'));
    }

    public function get_monthlypdf(Request $request){
        $employee_data  = Employee::where('id',$request->attn['emp_id'])->get();
        $attendence     = Attendance::whereYear('date',$request->attn['year'])->whereMonth('date',$request->attn['month'])->where('employee_id',$request->attn['emp_id'])->get();

        view()->share('employee_data', $employee_data);
        view()->share('attendence', $attendence);
        $pdf = PDF::loadView('hrm::attendance-management.report.pdf.monthlypdf')->setPaper('a4', 'portrait');
        return $pdf->download('monthly_report' . date('ymdhis') . '.pdf');
    }


    public function getemp_id(Request $request){
        Log::info($request->value);
    }
    public function get_dailyintimeview()
    {
        $qr =null;
        $department = Department::query()->pluck('name','id');
//        dd($department);
//        $section = Section::query()->pluck('name','id');
//        $line = Line::query()->pluck('name','id');
        return view('dashboard.report.dailyinview',compact('department','qr')) ;
    }



    public function get_dailyintimedata(Request $request){
        $department = Department::query()->pluck('name','id');
        $attendance = Attendance::query()->where('department_id',$request->department)->where('section_id',$request->section)->where('line_id',$request->line)->get();
        $qr =[
            'department'    =>$request->department,
            'section'       =>$request->section,
            'line'          =>$request->line
        ];
        return view('dashboard.report.dailyinview',compact('department','section','line','attendance','qr'));
    }



    public function get_datelypdf(Request $request){
        $attendence = Attendance::where('date',$request->attn['date'])->get();

        view()->share('attendence', $attendence);
        $pdf = PDF::loadView('hrm::attendance-management.report.pdf.datelypdf')->setPaper('a4', 'portrait');
        return $pdf->download('dately_report' . date('ymdhis') . '.pdf');
    }


    public function get_dailyview(){
        $qr =null;
        return view('hrm::attendance-management.report.datelyview',compact('qr'));
    }

    public function get_dailydata(Request $request){
        $attendence = Attendance::whereDate('date',$request->date)->get();
        $qr =[
            'date'=>$request->date
        ];

        
        $late               = $attendence->where('attendance_status_id',3)->count();
        $absent             = $attendence->where('attendance_status_id',2)->count();
        $earlyLeave         = $attendence->where('attendance_status_id',4)->count();
        $present            = $attendence->where('attendance_status_id',1)->count();
        $holiday            = $attendence->where('attendance_status_id',5)->count();
        $weeklyOff          = $attendence->where('attendance_status_id',6)->count();
        $leave              = $attendence->where('attendance_status_id',7)->count();
        $earlyLeaveLate     = $attendence->where('attendance_status_id',8)->count();


        return view('hrm::attendance-management.report.datelyview',compact('attendence','qr',
        'late','absent','earlyLeave','present','holiday','weeklyOff','leave','earlyLeaveLate'));
    }

}
