<?php

namespace Modules\Hrm\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\Hrm\Entities\Day;

class DayController extends Controller
{
    public function index()
    {   
        
        $days = Day::query()->paginate(10);
        return view('hrm::leave-management.day.index',compact('days'));
    }

    public function create()
    {
        return view('hrm::leave-management.day.add');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required'
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        Day::query()->create($request->all());
        return redirect('admin/day')->with('success', 'Day added Successfully!');
    }

    public function edit($id)
    {
        $day = Day::query()->findOrFail($id);
        return view('hrm::leave-management.day.edit',compact('day'));
    }

    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required'
        ]);
        $day= Day::query()->findOrFail($id);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $day->update($request->all());
        return redirect('admin/day')->with('success', 'Day Updated Successfully!');
    }

    public function destroy($id)
    {
        $day = Day::query()->findOrFail($id);
        $day->delete();
        return redirect('admin/day')->with('success', 'Day Deleted Successfully!');
    }

}
