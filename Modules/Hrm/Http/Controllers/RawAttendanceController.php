<?php

namespace Modules\Hrm\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Hrm\Entities\Attendance;
use Modules\Hrm\Entities\RawAttendance;

class RawAttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rawAttendances=RawAttendance::query()->paginate(10);
        return view('hrm::attendance-management.rawAttendance.index',compact('rawAttendances'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $attendances=Attendance::query()->paginate(10);
        return view('hrm::attendance-management.attendance.index',compact('attendances'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \Modules\Hrm\Entities\RawAttendance  $rawAttendance
     * @return \Illuminate\Http\Response
     */
    public function show(RawAttendance $rawAttendance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Modules\Hrm\Entities\RawAttendance  $rawAttendance
     * @return \Illuminate\Http\Response
     */
    public function edit(RawAttendance $rawAttendance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Modules\Hrm\Entities\RawAttendance  $rawAttendance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RawAttendance $rawAttendance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Modules\Hrm\Entities\RawAttendance  $rawAttendance
     * @return \Illuminate\Http\Response
     */
    public function destroy(RawAttendance $rawAttendance)
    {
        //
    }
}
