<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RawAttendance extends Model
{
    use HasFactory;

    protected $dates = ['access_time'];

    protected $fillable = [
        'unit_name',
        'registration_id',
        'access_id',
        'department',
        'access_time',
        'access_date',
        'user_name',
        'unit_id',
        'card',
    ];

//    protected $dates = ['access_date'];
}
