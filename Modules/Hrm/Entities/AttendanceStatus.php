<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttendanceStatus extends Model
{
    use HasFactory;
    
    protected $table='attendance_statuses';

    protected $fillable = [
        'name',
        'code'
    ];

    public function attendance(){
        return $this->belongsTo(Attendance::class);
    }
}
