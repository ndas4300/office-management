@extends('hrm::layouts.master')

@section('title','Attendance ')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{__('Attendance ')}}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">{{ __('Settings') }}</a></li>
                        <li class="breadcrumb-item active">{{__('Attendance ')}}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <!-- ***/Chart of Accounts page inner Content Start-->
    <section class="content">
        <div class="container-fluid">
            @if (session('attendances'))
                <div class="alert alert-success">
                    {{ session('attendances') }}
                </div>
            @endif
            <div class="row">
                <div class="col-12">
                    <div class="card">

                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>{{ __('Sl') }}</th>
                                    <th>{{ __('Name') }}</th>
                                    <th>{{ __('Date') }}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th>{{ __('In Time') }}</th>
                                    <th>{{ __('Out Time') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($attendances as $key => $attendance)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $attendance->employee->name }}</td>
                                        <td>{{ $attendance->date }}</td>
                                        <td>{{ $attendance->attendancestatus->name ?? '' }}    </td>
                                        <td>
                                            @if($attendance->manual_in_time || $attendance->manual_in_time != 0)
                                                {{$attendance->manual_in_time}}
                                            @else
                                                {{$attendance->in_time}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($attendance->manual_out_time || $attendance->manual_out_time != 0)
                                                {{$attendance->manual_out_time}}
                                            @else
                                                {{$attendance->out_time}}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="row d-flex mt-2 ">
                                <div class="d-flex justify-content-center">
                                    {{ $attendances->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop

@section('script')
    <script>
        function confirmDelete(){
            let x = confirm('Are you sure you want to delete this account head?');
            return !!x;
        }
    </script>
@stop

