@extends('hrm::layouts.master')

@section('title', 'Monthly Report|WPM')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Monthly attendance report') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('Home') }}</a></li>
                        <li class="breadcrumb-item active">{{ __('Department') }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row d-flex justify-content-center">
                <div class="col-md-12">
                    <div class="card-header shadow p-3 mb-5 bg-white rounded no_print">
                        {{ Form::open(['route' => 'get_monthdata']) }}
                        <div class="row">
                            <div class="col-sm-3 mb-1">
                                {{-- {{ Form::text('emp_name', null, ['id'=>'emp_name','class' =>
                            'form-control','required'=>'required']) }} --}}
                                {{ Form::select('employee_id', $employee, $qr['emp_id'] ?? '', ['id' => 'employee', 'class' => 'form-control select2']) }}
                            </div>
                            <div class="col-sm-3 mb-1">
                                {{ Form::select('year', array_combine($year, $year), $qr['year'] ?? '', ['id' => 'year', 'class' => 'form-control select2']) }}
                            </div>
                            <div class="col-sm-3 mb-1">
                                {{ Form::select('month', $month, $qr['month'] ?? '', ['id' => 'month', 'class' => 'form-control select2']) }}
                            </div>
                            <div class="col-sm-3 mb-1">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <button class="btn btn-primary" style="background-color: #1bc1f3;" type="submit"><i
                                                class="fas fa-search text-white"></i></button>
                                    </div>
                                    <div class="col-sm-6 d-flex justify-content-end">
                                        <a href="{{ route('monthlypdf', ['attn' => $qr]) }}"
                                            class="btn btn-secondary pb-0 mr-1"><i class="fas fa-file-pdf"></i></a>
                                        <a class="btn btn-info pb-0" onclick="window.print()"><i
                                                class="fas fa-print"></i></a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        {{ Form::close() }}
                    </div>
                    <div class="d-none print">
                        <h2>Monthly attendance report</h2>
                        <hr />
                    </div>
                    <div class="print">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card card-body">
                                    <div class="row">
                                        <div class="col-4">
                                            <div class="row">
                                                <div class="col-sm-6"><b> Employee Id :</b></div>
                                                <div class="col-sm-6">
                                                    @if (isset($employee_data))
                                                        {{ $employee_data[0]?->employee_no }}
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6"><b>Name :</b> </div>
                                                <div class="col-sm-6">
                                                    @if (isset($employee_data))
                                                        {{ $employee_data[0]?->name }}
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6"><b>Designation :</b></div>
                                                <div class="col-sm-6">
                                                    @if (isset($employee_data))
                                                        {{ $employee_data[0]?->officials?->designation?->name }}
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6"><b>Department :</b></div>
                                                <div class="col-sm-6">
                                                    @if (isset($employee_data))
                                                        {{ $employee_data[0]?->officials?->department?->name }}
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="row">
                                                <div class="col-sm-6"><b>Present :</b></div>
                                                <div class="col-sm-6">
                                                    {{ $present ?? null }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6"><b>Late :</b></div>
                                                <div class="col-sm-6">
                                                    {{ $late ?? null }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6"><b>Absent :</b></div>
                                                <div class="col-sm-6">
                                                    {{ $absent ?? null }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6"><b>Early Leave :</b></div>
                                                <div class="col-sm-6">
                                                    {{ $earlyLeave ?? null }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="row">
                                                <div class="col-sm-6"><b>Weekly Off :</b></div>
                                                <div class="col-sm-6">
                                                    {{ $weeklyOff ?? null }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6"><b>Holiday :</b></div>
                                                <div class="col-sm-6">
                                                    {{ $holiday ?? null }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6"><b>Leave :</b></div>
                                                <div class="col-sm-6">
                                                    {{ $leave ?? null }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6"><b>Late & Early Leave :</b></div>
                                                <div class="col-sm-6">
                                                    {{ $earlyLeaveLate ?? null }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <div class="d-flex justify-content-center">
                                                @if (isset($employee_data))
                                                    <img class=" rounded" src="{{ setImage($employee_data[0]?->image) }}"
                                                        height="80rem" width="80rem" alt="">
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card card-body">
                                    <div class="text-center">
                                        <h1>
                                            @if (isset($attendence) && count($attendence) > 0)
                                                {{ \Carbon\Carbon::parse($attendence[0]->date)->format('F') }}
                                                {{ \Carbon\Carbon::parse($attendence[0]->date)->format('Y') }}
                                            @else
                                                Month Year
                                            @endif
                                        </h1>
                                    </div>
                                    <div class="row">
                                        <div class="table-responsive">

                                            <table class="table text-center">
                                                <thead>
                                                    <tr>
                                                        <th class="text-left" scope="col">Date</th>
                                                        <th scope="col">Status</th>
                                                        <th scope="col">In Time</th>
                                                        <th scope="col">Out Time</th>
                                                        {{-- <th scope="col">Total Hour</th> --}}
                                                        {{-- <th scope="col">OT</th> --}}
                                                        {{-- <th scope="col">Extra OT</th> --}}
                                                        {{-- <th scope="col">Status</th> --}}
                                                        {{-- <th scope="col">Note</th> --}}
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if (isset($attendence) && $attendence != [])
                                                        @foreach ($attendence as $data)
                                                            <tr>
                                                                <th class="text-left" scope="row">{{ $data->date }}
                                                                </th>
                                                                <td>{{ $data->attendancestatus->name ?? '' }}</td>
                                                                <td>

                                                                    @if ($data->manual_in_time || $data->manual_in_time != 0)
                                                                        {{ \Carbon\Carbon::createFromFormat('H:i:s', $data->manual_in_time)->format('h:i a') }}
                                                                    @elseif($data->in_time)
                                                                        {{ \Carbon\Carbon::createFromFormat('H:i:s', $data->in_time)->format('h:i a') }}
                                                                    @else
                                                                        -
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if ($data->manual_out_time || $data->manual_out_time != 0)
                                                                        {{ \Carbon\Carbon::createFromFormat('H:i:s', $data->manual_out_time)->format('h:i a') }}
                                                                    @elseif($data->in_time)
                                                                        {{ \Carbon\Carbon::createFromFormat('H:i:s', $data->out_time)->format('h:i a') }}
                                                                    @else
                                                                        -
                                                                    @endif
                                                                </td>
                                                                {{-- <td>{{\Carbon\Carbon::parse($data->out_time)->floatDiffInHours(
                                                        $data->in_time)}} </td> --}}
                                                                {{-- <td>{{$data->ot}}</td> --}}
                                                                {{-- <td>{{$data->extra_ot}}</td> --}}
                                                                {{-- <td>{{$data->attendancestatus[0]->name}}</td> --}}
                                                                {{-- <td>@mdo</td> --}}
                                                            </tr>
                                                        @endforeach
                                                    @endif

                                                </tbody>
                                            </table>
                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                    <!-- END tabs -->
                </div>
            </div>
        </div>
    </section>
@endsection

@push('script')
    <script>
        /* ajax call for emp id */
        $(document).ready(function() {
            $('#emp_name').on('keyup', function() {
                var value = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ route('getemp_id') }}",
                    data: {
                        'value': value
                    },
                    success: function(data) {
                        // console.log(data);
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        //  alert("Status: " + textStatus); alert("Error: " + errorThrown);
                    }
                });
            });
        })

        // Show the first tab and hide the rest
        $('#tabs-nav li:first-child').addClass('active');
        $('.tab-content').hide();
        $('.tab-content:first').show();

        // Click function
        $('#tabs-nav li').click(function() {
            $('#tabs-nav li').removeClass('active');
            $(this).addClass('active');
            $('.tab-content').hide();

            var activeTab = $(this).find('a').attr('href');
            $(activeTab).fadeIn();
            return false;
        });
    </script>
@endpush


@push('style')
    <style>
        @media print {

            .no_print {
                display: none;
            }

            .print {
                display: block !important;
            }
        }

        .center {
            text-align: center;
        }
    </style>
@endpush
