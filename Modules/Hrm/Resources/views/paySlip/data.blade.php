@extends('resources.views.hrm::layouts.master')

@section('title', 'Employee Contact|WPM')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Pay Slip') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">{{ __('Settings') }}</a></li>
                        <li class="breadcrumb-item active">{{ __('PaySlip') }}</li>
                    </ol>
                </div>
            </div>
            <div class="col-11 mb-2">
                <button class="btn btn-primary float-right" onclick="window.print()">Print All</button>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- ***/Chart of Accounts page inner Content Start-->
    <section class="content">
        <div class="container-fluid">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @foreach ($employees as $employee)

                <div class="row page">
                    <div class="col-11">
                        <div class="subpage">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th class="tg-vb1b" colspan="5"><span
                                                style="font-weight:bold;">Web Point Limited</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="tg-0pky" colspan="3"><span style="font-weight:bold">Salary Slip</span>
                                    </td>
                                    <td class="tg-0pky">Month</td>
                                    <td class="tg-0pky">{{Carbon\Carbon::now()->subMonth()->format('F')}}-22</td>
                                </tr>
                                <tr>
                                    <td class="tg-0pky">Employee Name:</td>
                                    <td class="tg-0pky"><span
                                                style="font-weight:400;font-style:normal">{{$employee->name}}</span>
                                    </td>
                                    <td class="tg-0pky">Date of Joining:</td>
                                    <td class="tg-0pky" colspan="2">{{$employee->officials->joining_date}}</td>
                                </tr>
                                <tr>
                                    <td class="tg-0pky">Employee Code:</td>
                                    <td class="tg-0pky"><span
                                                style="font-weight:400;font-style:normal">{{$employee->employee_no}}</span>
                                    </td>
                                    <td class="tg-0pky">Total Working Days:</td>
                                    <td class="tg-0pky">
                                        @php
                                            $w= $workingDays->where('employee_id',$employee->id);
                                            $d= $days_attend->where('employee_id',$employee->id);
                                            $p= $present->where('employee_id',$employee->id);
                                            $l= $late->where('employee_id',$employee->id);
                                            $a= $absent->where('employee_id',$employee->id);
                                            $e= $earlyLeave->where('employee_id',$employee->id);
                                            $h= $holiday->where('employee_id',$employee->id);
                                            $wO= $weeklyOff->where('employee_id',$employee->id);
                                            $le= $leave->where('employee_id',$employee->id);
                                            $eL= $earlyLeaveLate->where('employee_id',$employee->id);

                                        @endphp
                                        {{ count($w)}} days
                                    </td>
                                    <td class="tg-0pky">{{count($d)}} days</td>
                                </tr>
                                <tr>
                                    <td class="tg-0pky">Designation:</td>
                                    <td class="tg-0pky"><span
                                                style="font-weight:400;font-style:normal">{{$employee->officials->designation->name ?? ''}}</span>
                                    </td>
                                    <td class="tg-0pky" colspan="3">
                                        P:{{ count($p)}},&nbsp;
                                        L: {{ count($l)}},&nbsp;
                                        A: {{ count($a)}},&nbsp;
                                        E: {{ count($e)}},&nbsp;
                                        H: {{ count($h)}},&nbsp;
                                        WO: {{ count($wO)}},&nbsp;
                                        LE: {{ count($le)}},&nbsp;
                                        EL: {{ count($eL)}} </td>

                                </tr>
                                <tr>
                                    <td class="tg-0pky">TIN:</td>
                                    <td class="tg-0pky"></td>
                                    <td class="tg-0pky">Leaves:</td>
                                    <td class="tg-0pky">P</td>
                                    <td class="tg-0pky">S</td>
                                </tr>
                                <tr>
                                    <td class="tg-0pky">Bank Account Number:</td>
                                    <td class="tg-0pky"></td>
                                    <td class="tg-0pky"><span
                                                style="font-weight:400;font-style:normal">Leaves Taken:</span></td>
                                    <td class="tg-0pky"></td>
                                    <td class="tg-0pky"></td>
                                </tr>
                                <tr>
                                    <td class="tg-0pky">Bank Name</td>
                                    <td class="tg-0pky"></td>
                                    <td class="tg-0pky">Balance Leave</td>
                                    <td class="tg-0pky"></td>
                                    <td class="tg-0pky"></td>
                                </tr>
                                <tr>
                                    <td class="tg-0pky" colspan="5"></td>
                                </tr>
                                <tr>
                                    <td class="tg-b1b0" colspan="2"><span style="font-weight:bold;">Income</span></td>
                                    <td class="tg-b1b0" colspan="3"><span style="font-weight:bold;">Deductions</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tg-0pky">Particulars</td>
                                    <td class="tg-0pky">Amount</td>
                                    <td class="tg-0pky"><span
                                                style="font-weight:400;font-style:normal">Particulars</span></td>
                                    <td class="tg-0pky" colspan="2">Amount</td>
                                </tr>
                                <tr>
                                    <td class="tg-0pky">Basic Salary</td>
                                    <td class="tg-0pky">{{$employee->officials->gross}}/-</td>
                                    <td class="tg-0pky"></td>
                                    <td class="tg-dvpl" colspan="2"></td>
                                </tr>
                                <tr>
                                    <td class="tg-kw6a">Total</td>
                                    <td class="tg-kw6a"><span style="font-weight:400;font-style:normal"></span></td>
                                    <td class="tg-yc5w">Total</td>
                                    <td class="tg-dvpl" colspan="2"><span
                                                style="font-weight:400;font-style:normal"></span></td>
                                </tr>
                                <tr>
                                    <td class="tg-b1b0" colspan="3"><span style="font-weight:bold;">Net Salary</span>
                                    </td>
                                    <td class="tg-0pky" colspan="2"><span
                                                style="font-weight:400;font-style:normal"></span></td>
                                </tr>
                                <tr>
                                    <td class="tg-0pky" colspan="5" rowspan="5"></td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                    <td class="tg-0pky" colspan="2">Director Signiture</td>
                                    <td class="tg-0pky" colspan="3">Employee Signiture</td>
                                </tr>
                                </tbody>
                            </table>


                            <div class="row" style="margin-top: 10px">
                                <div class="col-sm-12 col-md-9">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
@stop

@push('style')
    <style>
        .page {
            width: 210mm;
            min-height: 297mm;
            margin: auto;
            /* padding: 20mm;
            margin: 10mm auto; */
            /* border: 1px #D3D3D3 solid; */
            /* border-radius: 5px;
            background: white;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.1); */
        }

        .subpage {
            /* padding: 1mm; */
            background-color: white;
            height: 267mm;
        }

        @page {
            size: A4;
            margin: auto;
        }

        @media print {
            html, body {
                width: 210mm;
                height: 297mm;
            }

            .page {
                margin: auto;
                border: initial;
                border-radius: initial;
                width: initial;
                min-height: initial;
                box-shadow: initial;
                background: initial;
                page-break-after: always;
            }
        }
    </style>
    <style type="text/css">
        .tg {
            border-collapse: collapse;
            border-spacing: 0;
        }

        .tg td {
            font-family: Arial, sans-serif;
            font-size: 14px;
            overflow: hidden;
            padding: 10px 5px;
            word-break: normal;
        }

        .tg th {
            font-family: Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            overflow: hidden;
            padding: 10px 5px;
            word-break: normal;
        }

        .tg .tg-kw6a {
            text-align: left;
            vertical-align: top
        }

        .tg .tg-vb1b {
            font-size: 28px;
            text-align: center;
            vertical-align: top
        }

        .tg .tg-b1b0 {
            text-align: left;
            vertical-align: top
        }

        .tg .tg-yc5w {
            text-align: center;
            vertical-align: top
        }

        .tg .tg-0pky {
            text-align: left;
            vertical-align: top
        }

        .tg .tg-dvpl {
            text-align: right;
            vertical-align: top
        }
    </style>
@endpush

@section('script')
    <script>
        function confirmDelete() {
            let x = confirm('Are you sure you want to delete this account head?');
            return !!x;
        }
    </script>
@stop
