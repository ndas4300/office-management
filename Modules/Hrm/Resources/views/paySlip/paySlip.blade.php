@extends('hrm::layouts.master')

@section('title', 'Employee Contact|WPM')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Pay Slip') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">{{ __('Settings') }}</a></li>
                        <li class="breadcrumb-item active">{{ __('PaySlip') }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <!-- ***/Chart of Accounts page inner Content Start-->
    <section class="content">
        <div class="container-fluid">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <form action="{{ route('paySlip.generate') }}" id="content-form" method="post">
                <div class="row d-flex justify-content-center">
                    @csrf
                    <div class="col-11 mb-2">
                        <button class="btn btn-primary float-right" type="submit">Generate PaySlip</button>
                    </div>
                    <div class="col-11">
                        <div class="card">
                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th><input type="checkbox" id="some1"
                                                    data-check-pattern="[name^='some_key']" /><label
                                                    for="some1"></label>&nbsp;&nbsp;
                                                (<span id="count-checked-checkboxes">0</span>)
                                            </th>
                                            <th>{{ __('Employee No.') }}</th>
                                            <th>{{ __('Name') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($employees as $key => $data)
                                            <tr>
                                                <td><input type="checkbox" name="some_key[{{ $data->id }}]"
                                                        value="{{ $data->id }}" id="some{{ $data->id }}" /><label
                                                        for="some{{ $data->id }}"></label>
                                                </td>
                                                <td>{{ $data->employee_no }}</td>
                                                <td>{{ $data->name }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-sm-12 col-md-9">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            {{-- <div class="row d-flex justify-content-center">
                @csrf
                <div class="col-11 mb-2">
                    <button class="btn btn-primary float-right" type="submit">Generate PaySlip</button>
                </div>
                <div class="col-11">
                    <div class="card">
                        <div class="card-body">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="tg-2n01" colspan="5"><span style="font-weight:bold">Web Point
                                                Limited</span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="tg-0pky" colspan="3"><span style="font-weight:bold">Salary Slip</span>
                                        </td>
                                        <td class="tg-0pky">Month</td>
                                        <td class="tg-0pky">June-22</td>
                                    </tr>
                                    <tr>
                                        <td class="tg-0pky">Employee Name:</td>
                                        <td class="tg-0pky"><span style="font-weight:400;font-style:normal">Niloy Das</span>
                                        </td>
                                        <td class="tg-0pky">Date of Joining:</td>
                                        <td class="tg-0pky" colspan="2">01.01.2022</td>
                                    </tr>
                                    <tr>
                                        <td class="tg-0pky">Employee Code:</td>
                                        <td class="tg-0pky"><span style="font-weight:400;font-style:normal">LP0004</span>
                                        </td>
                                        <td class="tg-0pky">Total Working Days:</td>
                                        <td class="tg-0pky"></td>
                                        <td class="tg-0pky"></td>
                                    </tr>
                                    <tr>
                                        <td class="tg-0pky">Designation:</td>
                                        <td class="tg-0pky"><span style="font-weight:400;font-style:normal">PHP
                                                Programmer</span></td>
                                        <td class="tg-0pky">Number of Working Days Attended:</td>
                                        <td class="tg-0pky" colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td class="tg-0pky">TIN:</td>
                                        <td class="tg-0pky"></td>
                                        <td class="tg-0pky">Leaves:</td>
                                        <td class="tg-0pky">P</td>
                                        <td class="tg-0pky">S</td>
                                    </tr>
                                    <tr>
                                        <td class="tg-0pky">Bank Account Number:</td>
                                        <td class="tg-0pky"></td>
                                        <td class="tg-0pky"><span style="font-weight:400;font-style:normal">Leaves
                                                Taken:</span></td>
                                        <td class="tg-0pky"></td>
                                        <td class="tg-0pky"></td>
                                    </tr>
                                    <tr>
                                        <td class="tg-0pky">Bank Name</td>
                                        <td class="tg-0pky"></td>
                                        <td class="tg-0pky">Balance Leave</td>
                                        <td class="tg-0pky"></td>
                                        <td class="tg-0pky"></td>
                                    </tr>
                                    <tr>
                                        <td class="tg-0pky" colspan="5"></td>
                                    </tr>
                                    <tr>
                                        <td class="tg-2n01" colspan="2"><span style="font-weight:bold">Income</span></td>
                                        <td class="tg-2n01" colspan="3"><span style="font-weight:bold">Deductions</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tg-0pky">Particulars</td>
                                        <td class="tg-0pky">Amount</td>
                                        <td class="tg-0pky"><span
                                                style="font-weight:400;font-style:normal">Particulars</span></td>
                                        <td class="tg-0pky" colspan="2">Amount</td>
                                    </tr>
                                    <tr>
                                        <td class="tg-0pky">Basic Salary</td>
                                        <td class="tg-0pky">10,000/-</td>
                                        <td class="tg-0pky"></td>
                                        <td class="tg-dvpl" colspan="2">0.00/-</td>
                                    </tr>
                                    <tr>
                                        <td class="tg-0pky">Total</td>
                                        <td class="tg-0pky"><span style="font-weight:400;font-style:normal">10,000/-</span>
                                        </td>
                                        <td class="tg-c3ow">Total</td>
                                        <td class="tg-dvpl" colspan="2"><span
                                                style="font-weight:400;font-style:normal">0.00/-</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tg-2n01" colspan="3"><span style="font-weight:bold">Net Salary</span>
                                        </td>
                                        <td class="tg-0pky" colspan="2"><span
                                                style="font-weight:400;font-style:normal">10,000/-</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tg-0pky" colspan="5" rowspan="5"></td>
                                    </tr>
                                    <tr>
                                    </tr>
                                    <tr>
                                    </tr>
                                    <tr>
                                    </tr>
                                    <tr>
                                    </tr>
                                    <tr>
                                        <td class="tg-0pky" colspan="2">Director Signiture</td>
                                        <td class="tg-0pky" colspan="3">Employee Signiture</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="row" style="margin-top: 10px">
                                <div class="col-sm-12 col-md-9">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}


        </div>
    </section>

@stop

@push('style')
    <style type="text/css">
        .tg {
            border-collapse: collapse;
            border-spacing: 0;
        }

        .tg td {
            border-color: black;
            border-style: solid;
            border-width: 1px;
            font-family: Arial, sans-serif;
            font-size: 14px;
            overflow: hidden;
            padding: 10px 5px;
            word-break: normal;
        }

        .tg th {
            border-color: black;
            border-style: solid;
            border-width: 1px;
            font-family: Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            overflow: hidden;
            padding: 10px 5px;
            word-break: normal;
        }

        .tg .tg-2n01 {
            background-color: #3531ff;
            border-color: inherit;
            text-align: left;
            vertical-align: top
        }

        .tg .tg-c3ow {
            border-color: inherit;
            text-align: center;
            vertical-align: top
        }

        .tg .tg-0pky {
            border-color: inherit;
            text-align: left;
            vertical-align: top
        }

        .tg .tg-dvpl {
            border-color: inherit;
            text-align: right;
            vertical-align: top
        }
    </style>
@endpush

@section('script')
    <script>
        function confirmDelete() {
            let x = confirm('Are you sure you want to delete this account head?');
            return !!x;
        }
    </script>

    <script>
        jQuery(function() {
            jQuery('[data-check-pattern]').checkAll();
        });

        ;
        (function($) {
            'use strict';

            $.fn.checkAll = function(options) {
                return this.each(function() {
                    var mainCheckbox = $(this);
                    var selector = mainCheckbox.attr('data-check-pattern');
                    var onChangeHandler = function(e) {
                        var $currentCheckbox = $(e.currentTarget);
                        var $subCheckboxes;

                        if ($currentCheckbox.is(mainCheckbox)) {
                            $subCheckboxes = $(selector);
                            $subCheckboxes.prop('checked', mainCheckbox.prop('checked'));
                        } else if ($currentCheckbox.is(selector)) {
                            $subCheckboxes = $(selector);
                            mainCheckbox.prop('checked', $subCheckboxes.filter(':checked').length ===
                                $subCheckboxes.length);
                        }
                    };

                    $(document).on('change', 'input[type="checkbox"]', onChangeHandler);
                });
            };
        }(jQuery));
    </script>
    <script>
        $('textarea').keyup(function() {

            var characterCount = $(this).val().length,
                current = $('#current'),
                maximum = $('#maximum'),
                theCount = $('#the-count');
            if (characterCount > (characterCount / 159) + 1) {

                let length = (characterCount / 159) + 1
                for (let countMax = 1; countMax < length; countMax++) {
                    console.log(countMax);
                    maximum.text(countMax);

                }
            }

            current.text(characterCount);

        });
    </script>
    <script>
        $(document).ready(function() {

            var $checkboxes = $('#content-form td input[type="checkbox"]');

            $checkboxes.change(function() {
                // console.alert('lkdsjfkla');
                var countCheckedCheckboxes = $checkboxes.filter(':checked').length;
                $('#count-checked-checkboxes').text(countCheckedCheckboxes);

                $('#edit-count-checked-checkboxes').val(countCheckedCheckboxes);
            });

        });
    </script>
@stop
