<table id="example2" class="table table-bordered table-hover text-center">
    <thead>
        <tr>
            <th>{{ __('Sl') }}</th>
            <th>{{ __('Employee No.') }}</th>
            <th>{{ __('Leave Category') }}</th>
            <th>{{ __('Date') }}</th>
            <th>{{ __('Leave From') }}</th>
            <th>{{ __('Leave To') }}</th>
            <th>{{ __('Days') }}</th>
            <th>{{ __('Approved By') }}</th>
            <th>{{ __('Action') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($leaves as $key => $data)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $data->employee ? $data->employee->name : '' }}</td>
                <td>{{ $data->leaveCategory->name }}</td>
                <td>{{ $data->date }}</td>
                <td>{{ $data->leave_from }}</td>
                <td>{{ $data->leave_to }}</td>
                <td>{{ $data->days }}</td>
                <td>{{ $data->approved_by }}</td>
                <td>
                    {{ Form::open(['url'=>['admin/leave',$data->id],'method'=>'delete','onsubmit'=>'return confirmDelete()']) }}
                    <a href="{{ route('leave.edit',$data->id) }}" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a>
                    <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                    {{ Form::close() }}
                </td>
            </tr>
        @endforeach
    </tbody>
</table>