<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label for="employee_id">Employee No.*:</label>
            {{ Form::select('employee_id',$employee,null,['class'=>'form-control select2']) }}
        <small class="text-danger">{{ $errors->first('employee_id') }}</small>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="leave_category_id">Leave Category*:</label>
            {{ Form::select('leave_category_id',$leaveCategories,null,['class'=>'form-control select2']) }}
        <small class="text-danger">{{ $errors->first('leave_category_id') }}</small>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label for="date">Date*:</label>
            {{ Form::date('date',null,['class'=>'form-control','placeholder'=>'Enter Date Here:']) }}
        <small class="text-danger">{{ $errors->first('date') }}</small>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="leave_from">Leave From*:</label>
            {{ Form::date('leave_from',null,['class'=>'form-control','placeholder'=>'Enter Date Here:']) }}
        <small class="text-danger">{{ $errors->first('leave_from') }}</small>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label for="leave_to">Leave To*:</label>
            {{ Form::date('leave_to',null,['class'=>'form-control','placeholder'=>'Enter Date Here:']) }}
        <small class="text-danger">{{ $errors->first('leave_to') }}</small>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="approved_by">Approved By*:</label>
            {{ Form::select('approved_by',$users,null,['class'=>'form-control select2']) }}
        <small class="text-danger">{{ $errors->first('approved_by') }}</small>
        </div>
    </div>
</div>

<div class="col">
    <button type="submit" class="btn btn-success" > Create</button>
    <a href="{{ URL::previous() }}" class="btn btn-danger">Cancel</a>
</div>


    @section('script')
        <script>
            $(function () {
                $('.select2').select2();
            });
        </script>
@stop
