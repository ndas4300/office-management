<table id="example2" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>{{ __('Sl') }}</th>
            <th>{{ __('Employee') }}</th>
            <th>{{ __('Contacts') }}</th>
            <th>{{ __('Designation') }}</th>
            <th>{{ __('Image') }}</th>
            <th>{{ __('Action') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($employees as $key => $employee)
        <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $employee->employee_no}}<br>{{ $employee->name}}</td>
            <td>{{ $employee->employeeAddresses->pr_email}} <br> {{
                $employee->employeeAddresses->pr_phone_one}}</td>

            <td>{{ $employee->officials->department->name ?? ''}} <br> {{
                $employee->officials->designation->name ?? ''}}</td>
            <td><img class=" rounded"
                    src="{{ asset('assets/images/employees/') }}/{{ $employee->image }}"
                    height="50" width="50" alt=""></td>
            <td>
                {{
                Form::open(['url'=>['admin/employee',$employee->id],'method'=>'delete','onsubmit'=>'return
                confirmDelete()']) }}
                <a href="{{ route('employee.show',$employee->id) }}"
                    class="btn btn-info btn-sm"><i class="fas fa-eye"></i></a>
                <a href="{{ route('employee.edit',$employee->id) }}"
                    class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a>
                <button type="submit" class="btn btn-danger btn-sm"><i
                        class="fas fa-trash"></i></button>
                {{ Form::close() }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>