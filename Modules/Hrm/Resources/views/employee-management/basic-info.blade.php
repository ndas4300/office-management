<div class="row">
    <div class="col">
        <div class="form-group">
            <label for="employee_no">Employee No*:</label>
            {{ Form::text('employee_no',null,['class'=>'form-control','placeholder'=>'Enter Employee No:']) }}
        <small class="text-danger">{{ $errors->first('employee_no') }}</small>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="name">Name*:</label>
            {{ Form::text('name',null,['class'=>'form-control','placeholder'=>'Enter Name:']) }}
        <small class="text-danger">{{ $errors->first('name') }}</small>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="bn_name">BN Name*:</label>
            {{ Form::text('bn_name',null,['class'=>'form-control','placeholder'=>'Enter BN. Name:']) }}
        <small class="text-danger">{{ $errors->first('bn_name') }}</small>
        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="form-group">
            <label for="father">Father Name*:</label>
            {{ Form::text('father',null,['class'=>'form-control','placeholder'=>'Enter Father Name:']) }}
        <small class="text-danger">{{ $errors->first('father') }}</small>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="mother">Mother Name:</label>
            {{ Form::text('mother',null,['class'=>'form-control','placeholder'=>'Enter Mother Name:']) }}
        <small class="text-danger">{{ $errors->first('mother') }}</small>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="gender_id">Gender*:</label>
            {{ Form::select('gender_id',$gender,null,['class'=>'form-control select2','placeholder'=>'Select Gender:']) }}
        <small class="text-danger">{{ $errors->first('gender_id') }}</small>
        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="form-group">
            <label for="dob">DOB*:</label>
            {{ Form::date('dob',null,['class'=>'form-control','placeholder'=>'Enter DOB:']) }}
        <small class="text-danger">{{ $errors->first('dob') }}</small>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="marital_status_id">Marital Status*:</label>
            {{ Form::select('marital_status_id',$maritalStatus,null,['class'=>'form-control select2','placeholder'=>'Select Marital Status:']) }}
        <small class="text-danger">{{ $errors->first('marital_status_id') }}</small>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="spouse">Spouse*:</label>
            {{ Form::text('spouse',null,['class'=>'form-control','placeholder'=>'Enter Spouse Name:']) }}
        <small class="text-danger">{{ $errors->first('spouse') }}</small>
        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="form-group">
            <label for="blood_group_id">Blood Group*:</label>
            {{ Form::select('blood_group_id',$bloodGroup,null,['class'=>'form-control select2','placeholder'=>'Select Group:']) }}
        <small class="text-danger">{{ $errors->first('blood_group_id') }}</small>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="religion_id">Religion*:</label>
            {{ Form::select('religion_id',$religion,null,['class'=>'form-control select2','placeholder'=>'Select Religion:']) }}
        <small class="text-danger">{{ $errors->first('religion_id') }}</small>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="passport">Passport:</label>
            {{ Form::text('passport',null,['class'=>'form-control','placeholder'=>'Enter Passport No.']) }}
        <small class="text-danger">{{ $errors->first('passport') }}</small>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="form-group">
            <label for="nid">Nid Front*:</label>
            <input type="file" name="nid" id="nid" class="form-control">
            <img src="" id="nid-tag" width="200px" />
            <small class="text-danger">{{ $errors->first('nid') }}</small>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="nid_back">Nid Back*:</label>
            <input type="file" name="nid_back" id="nid_back" class="form-control">
            <img src="" id="nid_back-tag" width="200px" />
            <small class="text-danger">{{ $errors->first('nid_back') }}</small>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="driving_license">Driving Licence:</label>
            {{ Form::text('driving_license',null,['class'=>'form-control','placeholder'=>'Enter Driving Licence No.']) }}
        <small class="text-danger">{{ $errors->first('driving_license') }}</small>
        </div>
    </div>
</div>
    <div class="col-4">
        <div class="form-group">
            <label for="image">Image*:</label>
            <input type="file" name="image" id="employee-img" class="form-control">
            <img src="" id="employee-img-tag" width="200px" />
            <small class="text-danger">{{ $errors->first('image') }}</small>
        </div>
    </div>


@section('script')
    <script>
        $(function () {
            $('.select2').select2();
        });
    </script>

    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#employee-img-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#employee-img").change(function(){
            readURL(this);
        });
    </script>

@stop
