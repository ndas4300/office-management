<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label for="training_title">Training Title*:</label>
            {{ Form::text('training_title',$employeeTraining ? $employeeTraining->training_title : '',['class'=>'form-control ','placeholder'=>'Enter Training Title Here:']) }}
        <small class="text-danger">{{ $errors->first('training_title') }}</small>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label for="topics_covered">Topics Covered*:</label>
            {{ Form::text('topics_covered',$employeeTraining ? $employeeTraining->topics_covered : '',['class'=>'form-control ','placeholder'=>'Enter Topics Here:']) }}
        <small class="text-danger">{{ $errors->first('topics_covered') }}</small>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="training_institute">Training Institute*:</label>
            {{ Form::text('training_institute',$employeeTraining ? $employeeTraining->training_institute : '',['class'=>'form-control ','placeholder'=>'Enter Institute Here:']) }}
        <small class="text-danger">{{ $errors->first('training_institute') }}</small>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group{{$errors->has('training_year') ? 'has-error' : ''}}">
            {{Form::label('training_year','Training Year:' ,['class'=>'col-sm-9 control-label'])}}
            <div class="col-sm-9">
                {{Form::selectYear('training_year',1950,2050,$employeeTraining ? $employeeTraining->training_year : '' , ['class'=> 'form-control select2', 'required'=>'required' ])}}
                <small class="text-danger">{{ $errors->first('training_year') }}</small>
            </div>
        </div>



    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="training_duration">Training Duration*:</label>
            {{ Form::number('training_duration',$employeeTraining ? $employeeTraining->training_duration : '',['class'=>'form-control ','placeholder'=>'Enter Duration Here:']) }}
        <small class="text-danger">{{ $errors->first('training_duration') }}</small>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label for="training_location">Training Location*:</label>
            {{ Form::text('training_location',$employeeTraining ? $employeeTraining->training_location : '',['class'=>'form-control ','placeholder'=>'Enter Location Here:']) }}
        <small class="text-danger">{{ $errors->first('training_location') }}</small>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group{{$errors->has('error') ? 'has-error' : '' }}">
            {{Form::label('training_country_id','Training Country:',['class'=>'col-sm-9 control-label'])}}
            <div class="col-sm-9">
                {{Form::select('training_country_id',$countries,$employeeTraining? $employeeTraining->training_country_id : '',['class'=>'form-control select2', 'required'=>'required'])}}
                <small class="text-danger">{{ $errors->first('training_country_id') }}</small>
            </div>
        </div>

    </div>
</div>


    @section('script')
        <script>
            $(function () {
                $('.select2').select2();
            });
        </script>
@stop
