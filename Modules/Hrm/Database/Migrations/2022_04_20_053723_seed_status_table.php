<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SeedStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('statues', function (Blueprint $table) {
            $data = [
                ['name'=>'Working','description'=>'','is_active'=>'1'],
                ['name'=>'Resigned','description'=>'','is_active'=>'1'],
                ['name'=>'Fired','description'=>'','is_active'=>'1'],
            ];

            foreach ($data as $d){
                \Modules\OfficeSetup\Entities\Status::query()->create($d);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('statues', function (Blueprint $table) {
            //
        });
    }
}
