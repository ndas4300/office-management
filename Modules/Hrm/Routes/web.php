<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::prefix('hrm')->group(function() {
//    Route::get('/', 'HrmController@index');
//});


use Modules\Hrm\Http\Controllers\ManualController;
use Modules\Hrm\Http\Controllers\PaySlipController;

Route::prefix('admin')->middleware(['auth', 'verified'])->group(function () {
    // Attendance Route
    Route::resource('attendance', AttendanceController::class);

    Route::resource('rawAttendance', RawAttendanceController::class);


    //route for religion
    Route::resource('religion', ReligionController::class);
    Route::put('religion/check-status/{id}', [ReligionController::class, 'status']);
    //route for blood group
    Route::resource('blood-group', BloodGroupController::class);
    Route::put('blood-group/check-status/{id}', [BloodGroupController::class, 'status']);
    //route for marital status
    Route::resource('marital-status', MaritalStatusController::class);
    Route::put('marital-status/check-status/{id}', [MaritalStatusController::class, 'status']);
    //route for gender
    Route::resource('gender', GenderController::class);
    Route::put('gender/check-status/{id}', [GenderController::class, 'status']);
    //route for days
    Route::resource('day', DayController::class);
    //route for weekly off
    Route::resource('weekly-off', WeeklyOffController::class);


    //route for employee management starts here
    Route::resource('employee', EmployeeController::class);
    Route::post('employee-search', [EmployeeController::class, 'search'])->name('employee.search');
    //route for professional certificate
    Route::resource('professional-certificate', ProfessionalCertificateController::class);
    //route for employee experience
    Route::resource('employee-experience', EmployeeExperienceController::class);
    //route for cards
    Route::resource('card', CardController::class);
    Route::put('card/check-status/{id}', [CardController::class, 'status']);
    //route for education
    Route::resource('education', EducationController::class);
    Route::put('education/check-status/{id}', [EducationController::class, 'status']);
    //route for academic results
    Route::resource('academic-result', AcademicResultController::class);
    Route::put('academic-result/check-status/{id}', [AcademicResultController::class, 'status']);
    //route for employee academics
    Route::resource('employee-academic', EmployeeAcademicController::class);
    //route for employee trainings
    Route::resource('employee-training', EmployeeTrainingController::class);
    //route for employee addresses
    Route::resource('employee-address', EmployeeAddressController::class);


    //route for leave management starts here
    Route::resource('leave-category', LeaveCategoryController::class);
    Route::put('leave-category/status/{id}', [LeaveCategoryController::class, 'status']);
    //route for leaves
    Route::resource('leave', LeaveController::class);
    Route::post('leave-search', [LeaveController::class, 'search'])->name('leave.search');
    //route for earn leaves
    Route::resource('earn-leave', EarnLeaveController::class);


    // Manual In/Out
    Route::post('manual-in/search', [ManualController::class, 'searchIn'])->name('searchIn');
    Route::post('manual-in/store', [ManualController::class, 'storeIn'])->name('storeIn');
    Route::get('manual-in', [ManualController::class, 'manualIn'])->name('manualIn');

    Route::post('manual-out/search', [ManualController::class, 'searchOut'])->name('searchOut');
    Route::post('manual-out/store', [ManualController::class, 'storeOut'])->name('storeOut');
    Route::get('manual-out', [ManualController::class, 'manualOut'])->name('manualOut');
    // Manual In/Out

    // PaySlip

    Route::get('pay-slip',[PaySlipController::class,'index'])->name('paySlip.index');
    Route::post('pay-slip/generate',[PaySlipController::class,'generate'])->name('paySlip.generate');

    // PaySlip
    //route for leave management starts here
    Route::resource('attendance-status',AttendanceStatusController::class);
    Route::put('attendance-status/status/{id}',[AttendanceStatusController::class,'status']);

    Route::controller(ReportController::class)->group(function() {
        Route::get('monthlyreport', 'get_monthlyview')->name('monthlyreport');
        Route::post('getmonthdata', 'get_monthdata')->name('get_monthdata');
        Route::get('monthlypdf', 'get_monthlypdf')->name('monthlypdf');
        /* ajex call for emp id */
        Route::get('getemp_id','getemp_id')->name('getemp_id');



        Route::get('dailyintimereport','get_dailyintimeview')->name('dailyintimereport');
        Route::post('dailyintimedata','get_dailyintimedata')->name('get_dailyintimedata');
        Route::get('dailyintimepdf','get_dailyintimepdf')->name('dailyintimepdf');


        Route::get('daily-report','get_dailyview')->name('dailyreport');
        Route::post('daily-data','get_dailydata')->name('get_dailydata');
        Route::get('daily-pdf','get_datelypdf')->name('dailypdf');


    });
});