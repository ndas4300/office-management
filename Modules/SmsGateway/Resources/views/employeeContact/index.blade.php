@extends('smsgateway::layouts.master')

@section('title','Employee Contact|WPM')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{__('Contacts')}}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">{{ __('Settings') }}</a></li>
                    <li class="breadcrumb-item active">{{__('contacts')}}</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>


<!-- ***/Chart of Accounts page inner Content Start-->
<section class="content">
    <div class="container-fluid">
        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif
        <form action="{{route('employeeContact.sms')}}" id="content-form" method="post">
            <div class="row d-flex justify-content-center">
                @csrf
                <div class="col-sm-11">
                    <div class="card mb-10">
                        <div class="card-header" style="border-bottom: none !important;">
                            <div class="row d-flex justifiy-content-center">

                                <div class="col-3"></div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="sms_body">Body*:</label>
                                        {{
                                        Form::textarea('sms_body',null,['class'=>'form-control','id'=>'the-textarea','placeholder'=>'Enter
                                        Sms Body Here: ','rows'=>'4']) }}
                                        <div id="the-count">
                                            <span id="current">0</span>
                                            <span>/ </span>
                                            <span id="maximum">1</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3"></div>
                                <div class="col-3"></div>
                                <div class="col-6">
                                    <button class="btn btn-block btn-primary" type="submit">Send</button>
                                </div>
                                <div class="col-3">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-11">
                    <div class="card">


                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">


                                <thead>
                                    <tr>
                                        <th><input type="checkbox" id="some1"
                                                data-check-pattern="[name^='some_key']" /><label for="some1"></label>&nbsp;&nbsp;
                                            (<span id="count-checked-checkboxes">0</span>)
                                        </th>
                                        <th>{{ __('Employee No.') }}</th>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Number') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($employees as $key => $data)
                                    <tr>

                                        <td><input type="checkbox" name="some_key[{{ $data->id }}]"
                                                value="{{ $data->employeeAddresses->pr_phone_one }}"
                                                id="some{{ $data->id }}" /><label for="some{{ $data->id }}"></label>
                                        </td>


                                        <td>{{ $data->employee_no }}</td>
                                        <td>{{ $data->name }}</td>
                                        <td>{{ $data->employeeAddresses->pr_phone_one }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="row" style="margin-top: 10px">
                                <div class="col-sm-12 col-md-9">
                                    {{-- <div class="dataTables_info" id="example2_info" role="status"
                                        aria-live="polite">
                                        Showing 0 to 0 of 0 entries</div> --}}
                                </div>
                                {{-- <div class="col-sm-12 col-md-3">
                                    {{ $employees->links() }}
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

@stop

@section('script')
<script>
    function confirmDelete() {
        let x = confirm('Are you sure you want to delete this account head?');
        return !!x;
    }
</script>

<script>
    jQuery(function () {
        jQuery('[data-check-pattern]').checkAll();
    });

    ;
    (function ($) {
        'use strict';

        $.fn.checkAll = function (options) {
            return this.each(function () {
                var mainCheckbox = $(this);
                var selector = mainCheckbox.attr('data-check-pattern');
                var onChangeHandler = function (e) {
                    var $currentCheckbox = $(e.currentTarget);
                    var $subCheckboxes;

                    if ($currentCheckbox.is(mainCheckbox)) {
                        $subCheckboxes = $(selector);
                        $subCheckboxes.prop('checked', mainCheckbox.prop('checked'));
                    } else if ($currentCheckbox.is(selector)) {
                        $subCheckboxes = $(selector);
                        mainCheckbox.prop('checked', $subCheckboxes.filter(':checked').length ===
                            $subCheckboxes.length);
                    }
                };

                $(document).on('change', 'input[type="checkbox"]', onChangeHandler);
            });
        };
    }(jQuery));
</script>
<script>
    $('textarea').keyup(function () {

        var characterCount = $(this).val().length,
            current = $('#current'),
            maximum = $('#maximum'),
            theCount = $('#the-count');
        if (characterCount > (characterCount / 159) + 1) {

            let length = (characterCount / 159) + 1
            for (let countMax = 1; countMax < length; countMax++) {
                console.log(countMax);
                maximum.text(countMax);

            }
        }

        current.text(characterCount);

    });
</script>
<script>
    $(document).ready(function(){

var $checkboxes = $('#content-form td input[type="checkbox"]');
    
$checkboxes.change(function(){
    // console.alert('lkdsjfkla');
    var countCheckedCheckboxes = $checkboxes.filter(':checked').length;
    $('#count-checked-checkboxes').text(countCheckedCheckboxes);
    
    $('#edit-count-checked-checkboxes').val(countCheckedCheckboxes);
});

});
</script>
@stop