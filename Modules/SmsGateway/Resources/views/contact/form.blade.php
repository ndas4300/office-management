<div class="row">
    <div class="col-4"></div>
    <div class="col-4">
        <div class="form-group">
            <label for="name">Name*:</label>
            {{ Form::text('name',null,['class'=>'form-control','placeholder'=>'Enter Contact Name Here:']) }}
        <small class="text-danger">{{ $errors->first('name') }}</small>
        </div>
    </div>
    <div class="col-4"></div>
    <div class="col-4"></div>
    <div class="col-4">
        <div class="form-group">
            <label for="number">Number*:</label>
            {{ Form::text('number',null,['class'=>'form-control','placeholder'=>'Enter Contact Number Here: (EX:01XXXXXXXXX)']) }}
        <small class="text-danger">{{ $errors->first('number') }}</small>
        </div>
    </div>
    <div class="col-4"></div>
    <div class="col-4"></div>
    <div class="col-4">
        <div class="form-group">
            <label for="contact_group_id">Group*:</label>
            {{ Form::select('contact_group_id',$contactGroups,null,['class'=>'form-control',]) }}
        <small class="text-danger">{{ $errors->first('contact_group_id') }}</small>
        </div>
    </div>
    <div class="col-4"></div>
</div>
<div class="row">
    <div class="col-4"></div>
    <div class="col-4">
        <button type="submit" class="btn btn-success" > Create</button>
        <a href="{{ URL::previous() }}" class="btn btn-danger">Cancel</a>
    </div>
    <div class="col-4"></div>
</div>

