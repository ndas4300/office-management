@extends('smsgateway::layouts.master')

@section('title','Contact|WPM')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{__('Contacts')}}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">{{ __('Settings') }}</a></li>
                    <li class="breadcrumb-item active">{{__('contacts')}}</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>


<!-- ***/Chart of Accounts page inner Content Start-->
<section class="content">
    <div class="container-fluid">
        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-md-11">
                        <div class="mx-auto pull-right">
                            <div class="input-group mb-3">
                                {{ Form::text('q',null,['class'=>'form-control','id'=>'q','placeholder'=>__('Enter
                                Contact Name/ Contact Number')]) }}
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2"><span
                                            class="fas fa-search"></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <a href="{{ url('admin/contact/create') }}" class="btn btn-info " style="margin-left: 10px;"> {{__('New')}}</a>
                    </div>
                </div>
                <div class="card">
                    {{-- <div class="card-header">
                        <input type="text" name="search" class="my_search float-right" id="search"
                            placeholder="Search here">
                    </div> --}}
                    {{-- <div class="card-header" style="border-bottom: none !important;">
                        <div class="row">
                            <div>
                                <a href="{{ url('admin/contact/create') }}" class="btn btn-info btn-sm"
                                    style="margin-top: 10px; margin-left: 10px;"> <i class="fas fa-plus-circle"></i>
                                    New</a>

                            </div>
                        </div>
                    </div> --}}

                    <div class="card-body" id="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>{{ __('Sl') }}</th>
                                    <th>{{ __('Name') }}</th>
                                    <th>{{ __('Number') }}</th>
                                    <th>{{ __('Group') }}</th>
                                    <th>{{ __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($contacts as $key => $data)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $data->name }}</td>
                                    <td>{{ $data->number }}</td>
                                    <td>{{ $data->contactGroup->name }}</td>
                                    <td>
                                        {{
                                        Form::open(['url'=>['admin/contact',$data->id],'method'=>'delete','onsubmit'=>'return
                                        confirmDelete()']) }}
                                        <a href="{{ route('contact.edit',$data->id) }}"
                                            class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a>
                                        <button type="submit" class="btn btn-danger btn-sm"><i
                                                class="fas fa-trash"></i></button>
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="row" style="margin-top: 10px">
                            <div class="col-sm-12 col-md-9">
                                {{-- <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">
                                    Showing 0 to 0 of 0 entries</div> --}}
                            </div>
                            <div class="col-sm-12 col-md-3">
                                {{ $contacts->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@stop

@section('script')
<script>
    function confirmDelete() {
        let x = confirm('Are you sure you want to delete this account head?');
        return !!x;
    }
</script>
<script>
    $("#q").keyup(function () {
        var q = $(this).val();
        console.log(q);
        var csrf = "{{ @csrf_token() }}"
        $.ajax({
            url: "{{ route('contact.search') }}",
            data: {
                _token: csrf,
                q: q
            },
            type: "POST",
        }).done(function (e) {
            console.log(e);
            $("#card-body").html(e);
        })
    })
</script>
@stop