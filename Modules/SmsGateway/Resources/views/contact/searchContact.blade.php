<table id="example2" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>{{ __('Sl') }}</th>
            <th>{{ __('Name') }}</th>
            <th>{{ __('Number') }}</th>
            <th>{{ __('Group') }}</th>
            <th>{{ __('Action') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($contacts as $key => $data)
        <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $data->name }}</td>
            <td>{{ $data->number }}</td>
            <td>{{ $data->contactGroup->name }}</td>
            <td>
                {{
                Form::open(['url'=>['admin/contact',$data->id],'method'=>'delete','onsubmit'=>'return
                confirmDelete()']) }}
                <a href="{{ route('contact.edit',$data->id) }}" class="btn btn-warning btn-sm"><i
                        class="fas fa-edit"></i></a>
                <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                {{ Form::close() }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>