@extends('smsgateway::layouts.master')

@section('title','CustomSms')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h3>{{ __('Add CustomSms') }}</h3>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">{{ __('Settings') }}</a></li>
                    <li class="breadcrumb-item active">{{__ ('customSms') }}</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    @if($errors->any())
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
    @endif
</section>

<!-- ***/Notices page inner Content Start-->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        {{ Form::open(['route'=>'customSms.store','method'=>'post']) }}
                        @include('smsgateway::customSms.form')
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@push('script')
<script>
    $('textarea').keyup(function () {

        var characterCount = $(this).val().length,
            current = $('#current'),
            maximum = $('#maximum'),
            theCount = $('#the-count');
        if (characterCount > (characterCount / 159) + 1) {

            let length = (characterCount / 159) + 1
            for (let countMax = 1; countMax < length; countMax++) {
                console.log(countMax);
                maximum.text(countMax);

            }
        }

        current.text(characterCount);

    });

    $(document).ready(function () {
        $('#group').val("0");
        $('#number').val("0");
        $('#contact_id').val("0");

        $('#group').change(function () {
            selectVal = $('#group').val();

            if (selectVal == null) {
                $('#number').prop("disabled", false);
                $('#contact_id').prop("disabled", false);
            } else {
                $('#number').prop("disabled", true);
                $('#contact_id').prop("disabled", true);
            }
        })
        $('#number').change(function () {
            selectVal = $('#number').val();

            if (selectVal == null) {
                $('#group').prop("disabled", false);
                $('#contact_id').prop("disabled", false);
            } else {
                $('#group').prop("disabled", true);
                $('#contact_id').prop("disabled", true);
            }
        })
        $('#contact_id').change(function () {
            selectVal = $('#contact_id').val();

            if (selectVal == null) {
                $('#number').prop("disabled", false);
                $('#group').prop("disabled", false);
            } else {
                $('#number').prop("disabled", true);
                $('#group').prop("disabled", true);
            }
        })

    });
</script>
@endpush