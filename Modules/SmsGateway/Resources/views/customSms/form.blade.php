<div class="row">
    <div class="col-4"></div>
    <div class="col-4">
        <div class="form-group">
            <label for="contact_group_id">Group*:</label>
            {{
            Form::select('contact_group_id',$contactGroups,null,['class'=>'form-control','id'=>'group','placeholder'=>'Enter
            Group Here:']) }}
        </div>
    </div>
    <div class="col-4"></div>
    <div class="col-4"></div>
    <div class="col-4">
        <div class="form-group">
            <label for="contact_id">Contacts*:</label>
            {{ Form::select('contact_id',$contacts,null,['class'=>'form-control
            select2','id'=>'contact_id','placeholder'=>'Choose Contacts Here:']) }}
        </div>
    </div>
    <div class="col-4"></div>
    <div class="col-4"></div>
    <div class="col-4">
        <div class="form-group">
            <label for="number">Number*:</label>
            {{ Form::number('number',null,['class'=>'form-control','id'=>'number','placeholder'=>'Enter Customer Number
            Here:
            (EX:018XXXXXXXX)']) }}
        </div>
    </div>
    <div class="col-4"></div>
    <div class="col-4"></div>
    <div class="col-4">
        <div class="form-group">
            <label for="sms_body">Body*:</label>
            {{ Form::textarea('sms_body',null,['class'=>'form-control','id'=>'the-textarea','placeholder'=>'Enter Sms
            Body Here: ']) }}
            <div id="the-count">
                <span id="current">0</span>
                <span>/ </span>
                <span id="maximum">1</span>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-4"></div>
    <div class="col-4">
        <button type="submit" class="btn btn-success"> Send</button>
        <a href="{{ URL::previous() }}" class="btn btn-danger">Cancel</a>
    </div>
    <div class="col-4"></div>
</div>