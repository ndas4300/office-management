<?php

namespace Modules\SmsGateway\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomSms extends Model
{
    use HasFactory;
    protected $fillable = [
        'number',
        'sms_body'
    ];
}
