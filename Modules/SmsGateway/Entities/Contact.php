<?php

namespace Modules\SmsGateway\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'number',
        'contact_group_id'
    ];

    public function contactGroup()
    {
        return $this->belongsTo(ContactGroup::class);
    }
}
