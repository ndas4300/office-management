<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::prefix('smsgateway')->group(function() {
//    Route::get('/', 'SmsGatewayController@index');
//});

use Modules\SmsGateway\Http\Controllers\ContactGroupController;
use Modules\SmsGateway\Http\Controllers\CustomSmsController;
use Modules\SmsGateway\Http\Controllers\EmployeeContactController;

Route::prefix('admin')->middleware(['auth', 'verified'])->group(function () {
// Custom Sms
//    Route::resource('custom-sms',CustomSmsController::class);
    Route::get('custom-sms', [CustomSmsController::class, 'index'])->name('customSms.index');
    Route::get('custom-sms/create', [CustomSmsController::class, 'create'])->name('customSms.create');
    Route::post('custom-sms/store', [CustomSmsController::class, 'store'])->name('customSms.store');
//// Custom Sms

// Contacts
    Route::resource('contact', ContactController::class);
    Route::post('contact-search', [ContactController::class, 'search'])->name('contact.search');
    Route::get('employee-contact', [EmployeeContactController::class, 'index'])->name('employeeContact.index');
    Route::post('employee-sms', [EmployeeContactController::class, 'sms'])->name('employeeContact.sms');
// Contacts

// Contact Group
    Route::get('contact-group', [ContactGroupController::class, 'index'])->name('contactGroup.index');
    Route::get('contact-group/create', [ContactGroupController::class, 'create'])->name('contactGroup.create');
    Route::post('contact-group', [ContactGroupController::class, 'store'])->name('contactGroup.store');
    Route::put('contact-group/{contactGroup}', [ContactGroupController::class, 'update'])->name('contactGroup.update');
    Route::delete('contact-group/{contactGroup}', [ContactGroupController::class, 'destroy'])->name('contactGroup.destroy');
    Route::get('contact-group/{contactGroup}/edit', [ContactGroupController::class, 'edit'])->name('contactGroup.edit');
    Route::get('contact-group/{contactGroup}/show', [ContactGroupController::class, 'show'])->name('contactGroup.show');
// Contact Group
});