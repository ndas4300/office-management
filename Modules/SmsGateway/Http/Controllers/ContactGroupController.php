<?php

namespace Modules\SmsGateway\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\SmsGateway\Entities\ContactGroup;

class ContactGroupController extends Controller
{
    public function index()
    {   
        
        $contactGroups = ContactGroup::query()->withCount(['contacts'])->paginate(10);
        // dd($contactGroups);
        return view('smsgateway::contactGroup.index',compact('contactGroups'));
    }

    public function create()
    {
        return view('smsgateway::contactGroup.add');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name'      => 'required|string',
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }
        
        ContactGroup::query()->create($request->all());
        return redirect()->route('contactGroup.index')->with('success', 'Custom Sms Sent Successfully!');
    }

    public function edit($id)
    {
        $contactGroup = ContactGroup::query()->findOrFail($id);
        return view('smsgateway::contactGroup.edit',compact('contactGroup'));
    }

    public function show(ContactGroup $contactGroup)
    {
        return view('smsgateway::contactGroup.show',compact('contactGroup'));
    }

    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(),[
            
            'name'      => 'required|string',
        ]);
        $contactGroup= ContactGroup::query()->findOrFail($id);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $contactGroup->update($request->all());
        return redirect()->route('contactGroup.index')->with('success', 'ContactGroup Updated Successfully!');
    }

    public function destroy($id)
    {
        $contactGroup = ContactGroup::query()->findOrFail($id);
        $contactGroup->delete();
        return redirect()->route('contactGroup.index')->with('success', 'ContactGroup Deleted Successfully!');
    }

}
