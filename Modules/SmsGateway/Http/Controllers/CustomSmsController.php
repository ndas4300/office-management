<?php

namespace Modules\SmsGateway\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\Hrm\Entities\Employee;
use Modules\SmsGateway\Entities\Contact;
use Modules\SmsGateway\Entities\ContactGroup;

class CustomSmsController extends Controller
{
    // public function index()
    // {

    //     $employees = Employee::query()->whereHas('statuses', function ($q) {
    //             $q->where('status_id', 1);
    //         })->get();

    //     foreach ($employees as $employee) {
    //         $calender_code_id = $employee->officials->calender_code_id;
    //         $holiday = CalendarEvent::query()
    //             ->where('calendar_id', $calender_code_id)
    //             ->whereDate('start', now()->addDay(1)->format('Y-m-d'))
    //             ->where('is_holiday', 1)
    //             ->first();

    //             // dd($employee->employeeAddresses->pr_phone_one);
    //         // dd($holiday != null);


    //         if ($holiday != null) {
    //             $url = "https://sms.solutionsclan.com/api/sms/send";
    //             $data = [
    //                 "apiKey" => "A0001234bd0dd58-97e5-4f67-afb1-1f0e5e83d835",
    //                 "contactNumbers" => "$employee->employeeAddresses->pr_phone_one",
    //                 "senderId" => "WebPointLtd",
    //                 "textBody" => "Dear Employees, Tomorrow is.$holiday->name. in office. "
    //             ];
    //             $ch = curl_init();
    //             curl_setopt($ch, CURLOPT_URL, $url);
    //             curl_setopt($ch, CURLOPT_POST, 1);
    //             curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    //             curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //             curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    //             curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    //             $response = curl_exec($ch);
    //             curl_close($ch);
    //         }
    //     }
    // }

    public function create()
    {
        $contactGroups = ContactGroup::pluck('name', 'id');
        $contacts = Contact::pluck('name', 'id');
        return view('smsgateway::customSms.add', compact('contactGroups', 'contacts'));
    }

    public function store(Request $request)
    {
        // dd($request->contact_group_id == 4);
        // dd($request->all());
        if (!$request->has('contact_id') && !$request->has('contact_group_id')) {
            $validator = Validator::make($request->all(), [
                'number'    => 'required|digits:11',
                'sms_body'  => 'required'
            ]);
        } elseif (!$request->has('number') && !$request->has('contact_group_id')) {
            $validator = Validator::make($request->all(), [
                'contact_id'    => 'required',
                'sms_body'  => 'required'
            ]);
        } elseif (!$request->has('number') && !$request->has('contact_id')) {
            $validator = Validator::make($request->all(), [
                'contact_group_id'    => 'required',
                'sms_body'  => 'required'
            ]);
        }

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }


        if ($request->has('contact_id')) {
            $contact = Contact::where('id', $request->contact_id)->pluck('number')->first();
            // dd($contact);
            $url = "https://sms.solutionsclan.com/api/sms/send";
            $data = [
                "apiKey" => "A0001234bd0dd58-97e5-4f67-afb1-1f0e5e83d835",
                "contactNumbers" => $contact,
                "senderId" => "WebPointLtd",
                "textBody" => $request->sms_body
            ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            $response = curl_exec($ch);
            curl_close($ch);
            // dd('bsdk');
        } elseif ($request->has('number')) {
            $url = "https://sms.solutionsclan.com/api/sms/send";
            $data = [
                "apiKey" => "A0001234bd0dd58-97e5-4f67-afb1-1f0e5e83d835",
                "contactNumbers" => $request->number,
                "senderId" => "WebPointLtd",
                "textBody" => $request->sms_body
            ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            $response = curl_exec($ch);
            curl_close($ch);
        } elseif ($request->contact_group_id == 4) {
            $contact = Contact::pluck('number')->toArray();
            $employee = Employee::with('employeeAddresses')->get()->pluck('employeeAddresses.pr_phone_one')->toArray();
            $all = array_merge($contact, $employee);
            $allContacts = collect($all)->unique();
            foreach ($allContacts as $allContact) {
                $url = "https://sms.solutionsclan.com/api/sms/send";
                $data = [
                    "apiKey" => "A0001234bd0dd58-97e5-4f67-afb1-1f0e5e83d835",
                    "contactNumbers" => $allContact,
                    "senderId" => "WebPointLtd",
                    "textBody" => $request->sms_body
                ];

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                $response = curl_exec($ch);
                curl_close($ch);
            }
        } else {
            $contacts = Contact::where('contact_group_id', $request->contact_group_id)->pluck('number');
            foreach ($contacts as $contact) {
                $url = "https://sms.solutionsclan.com/api/sms/send";
                $data = [
                    "apiKey" => "A0001234bd0dd58-97e5-4f67-afb1-1f0e5e83d835",
                    "contactNumbers" => $contact,
                    "senderId" => "WebPointLtd",
                    "textBody" => $request->sms_body
                ];

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                $response = curl_exec($ch);
                curl_close($ch);
            }
        }

        return redirect()->back()->with('success', 'Custom Sms Sent Successfully!');
    }

    // public function edit($id)
    // {
    //     $customSms = CustomSms::query()->findOrFail($id);
    //     return view('smsgateway::customSms.edit',compact('customSms'));
    // }

    // public function update($id, Request $request)
    // {
    //     $validator = Validator::make($request->all(),[
    //         'name' => 'required'
    //     ]);
    //     $customSms= CustomSms::query()->findOrFail($id);

    //     if($validator->fails()){
    //         return redirect()->back()->withErrors($validator)->withInput($request->all());
    //     }

    //     $customSms->update($request->all());
    //     return redirect('admin/customSms')->with('success', 'CustomSms Updated Successfully!');
    // }

    // public function destroy($id)
    // {
    //     $customSms = CustomSms::query()->findOrFail($id);
    //     $customSms->delete();
    //     return redirect('admin/customSms')->with('success', 'CustomSms Deleted Successfully!');
    // }

}
