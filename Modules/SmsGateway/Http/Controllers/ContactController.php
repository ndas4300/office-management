<?php

namespace Modules\SmsGateway\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\SmsGateway\Entities\Contact;
use Modules\SmsGateway\Entities\ContactGroup;

class ContactController extends Controller
{
    public function index()
    {   

        // $dayOfMonth = cal_days_in_month('1',5,2022);
        // dd($dayOfMonth );
        $contacts = Contact::with('contactGroup')->paginate(10);
        // dd($contacts);
        return view('smsgateway::contact.index',compact('contacts'));
    }

    public function search(Request $request)
    {
        // dd('asdkjaslkj');
        $contacts = Contact::query()
            ->where('name','like','%'.$request->get('q').'%')
            ->orWhere('number','like','%'.$request->get('q').'%')
            ->paginate(10);

        return view('smsgateway::contact.searchContact',compact('contacts'));
    }

    public function create()
    {
        $contactGroups= ContactGroup::pluck('name','id');
        return view('smsgateway::contact.add',compact('contactGroups'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(),[
            'name'      => 'required|string',
            'number'    => 'required|digits:11',
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }
        
        Contact::query()->create($request->all());
        return redirect()->route('contact.index')->with('success', 'Custom Sms Sent Successfully!');
    }

    public function edit($id)
    {
        $contact = Contact::query()->findOrFail($id);
        $contactGroups= ContactGroup::pluck('name','id');
        return view('smsgateway::contact.edit',compact('contact','contactGroups'));
    }

    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(),[
            
            'name'      => 'required|string',
            'number'    => 'required|digits:11',
        ]);
        $contact= Contact::query()->findOrFail($id);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $contact->update($request->all());
        return redirect()->route('contact.index')->with('success', 'Contact Updated Successfully!');
    }

    public function destroy($id)
    {
        $contact = Contact::query()->findOrFail($id);
        $contact->delete();
        return redirect()->route('contact.index')->with('success', 'Contact Deleted Successfully!');
    }


}