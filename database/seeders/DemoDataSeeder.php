<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Hrm\Entities\AcademicResult;
use Modules\Hrm\Entities\BloodGroup;
use Modules\Hrm\Entities\Day;
use Modules\Hrm\Entities\Education;
use Modules\Hrm\Entities\Gender;
use Modules\Hrm\Entities\LeaveCategory;
use Modules\Hrm\Entities\MaritalStatus;
use Modules\Hrm\Entities\Religion;
use Modules\OfficeSetup\Entities\Calender;
use Modules\OfficeSetup\Entities\Department;
use Modules\OfficeSetup\Entities\Designation;
use Modules\OfficeSetup\Entities\Status;

class DemoDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createGenders();
        $this->createMaritalStatuses();
        $this->createBloodGroups();
        $this->createReligions();
        $this->createStatuses();
        $this->createDepartments();
        $this->createDesignations();
        // $this->createShifts();
        $this->createEducations();
        $this->createResults();
        $this->createCalenders();
        $this->createDays();
        $this->createLeaveCategorys();
    }

    private function createGenders()
    {
        $gender=['Male','Female','Others'];
        $length=count($gender);

        for ($i=0; $i < $length ; $i++) {
            Gender::create([
                'name'              => $gender[$i],
                'description'       => '',
                'is_active'         => '1',
            ]);
        }
    }
    private function createMaritalStatuses()
    {
        $maritalStatus=['Single','Married','Divorced'];
        $length=count($maritalStatus);

        for ($i=0; $i < $length; $i++) {
            MaritalStatus::create([
            'name'              => $maritalStatus[$i],
            'description'       => '',
            'is_active'         => '1',
        ]);
        }
    }
    private function createBloodGroups()
    {
        $bloodGroup=['A+','A-','B+','B-','O+','O-','AB+','AB-'];
        $length=count($bloodGroup);

        for ($i=0; $i < $length ; $i++) {
            BloodGroup::create([
                'name'              => $bloodGroup[$i],
                'description'       => '',
                'is_active'         => '1',
            ]);
        }
    }
    private function createReligions()
    {
        $religion=['Muslim','Hindu','Christan','Buddho'];
        $length=count($religion);

        for ($i=0; $i < $length ; $i++) {
            Religion::create([
                'name'              => $religion[$i],
                'description'       => '',
                'is_active'         => '1',
            ]);
        }
    }
    private function createStatuses()
    {
        $status=['Working','Resigned','Fired'];
        $length=count($status);

        for ($i=0; $i < $length ; $i++) {
            Status::create([
                'name'              => $status[$i],
                'description'       => '',
                'is_active'         => '1',
            ]);
        }
    }
    private function createDepartments()
    {
        $department=['department1','department2','department3'];
        $length=count($department);

        for ($i=0; $i < $length ; $i++) {
            Department::create([
                'name'              => $department[$i],
                'description'       => '',
                'is_active'         => '1',
            ]);
        }
    }
    private function createDesignations()
    {
        $designation=['designation1','designation2','designation3'];
        $length=count($designation);

        for ($i=0; $i < $length ; $i++) {
            Designation::create([
                'name'              => $designation[$i],
                'display_name'      => $designation[$i],
                'is_active'         => '1',
            ]);
        }
    }
    // private function createShifts()
    // {
    //     $shift=['shift1','shift2','shift3'];
    //     $length=count($shift);

    //     for ($i=0; $i < $length ; $i++) {
    //         Shift::create([
    //             'name'              => $shift[$i],
    //             'description'       => '',
    //         ]);
    //     }
    // }
    private function createEducations()
    {
        $education=['education1','education2','education3'];
        $length=count($education);

        for ($i=0; $i < $length ; $i++) {
            Education::create([
                'name'              => $education[$i],
                'description'       => '',
                'is_active'         => '1',
            ]);
        }
    }
    private function createResults()
    {
        $result=['result1','result2','result3'];
        $length=count($result);

        for ($i=0; $i < $length ; $i++) {
            AcademicResult::create([
                'name'              => $result[$i],
                'description'       => '',
                'is_active'         => '1',
            ]);
        }
    }
    private function createCalenders()
    {
        $calendar=['calendar1','calendar2','calendar3'];
        $length=count($calendar);

        for ($i=0; $i < $length ; $i++) {
            Calender::create([
                'name'              => $calendar[$i],
                'description'       => '',
                'status'            => '1',
            ]);
        }
    }
    private function createDays()
    {
        $day=['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
        $length=count($day);

        for ($i=0; $i < $length ; $i++) {
            Day::create([
                'name'              => $day[$i],
            ]);
        }
    }
    private function createLeaveCategorys()
    {
        $leaveCategory=['Leave Category 1','Leave Category 2','Leave Category 3'];
        $length=count($leaveCategory);

        for ($i=0; $i < $length ; $i++) {
            LeaveCategory::create([
                'name'              => $leaveCategory[$i],
                'description'       => '',
                'is_active'            => '1',
            ]);
        }
    }

}
