<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\SmsController;
use Database\Seeders\CountrySeeder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[HomeController::class,'dashboard'])->name('home');

Route::prefix('admin')->middleware(['auth', 'verified'])->group(function () {
    // Attendance Route



    //route for api setting starts here
    Route::get('communication/apiSetting',[CommunicationSettingController::class,'index'])->name('communication.apiSetting');
    Route::patch('communication/apiSetting/update',[CommunicationSettingController::class,'update'])->name('apiSetting.update');
    //route for api setting ends here



    /*roles Start*/
    Route::get("/role",[RoleController::class,'index'])->name("role");
    Route::post("/role-insert",[RoleController::class,'store'])->name('role.store');
    Route::get("/role-edit/{id}",[RoleController::class,'edit'])->name('role.edit');
    Route::put("/role-update/{id}",[RoleController::class,'update'])->name('role.update');
    Route::delete("/role-delete/{id}",[RoleController::class,'destroy'])->name('role.destroy');
    /*roles End*/




});

//Route::get('migrate',function(){
//    Artisan::call('migrate');
//    dd('done');
//});

 Route::get('migrate-fresh',function(){
     Artisan::call('migrate:fresh --seed');
     dd(' done fresh');
 });

Route::get('optimize',function(){
    Artisan::call('optimize');
    Artisan::call('optimize:clear');
    dd('done');
});

Route::get('seed',function(){
    Artisan::call('db:seed', [
        '--class' => CountrySeeder::class
    ]);
});

Route::get('rollback',function(){
    Artisan::call('migrate:rollback --step=3');
    dd('done');
});

Route::get('storage-link',function(){
    Artisan::call('storage:link');
    dd('done');
});

// Route::get('drop',function(){
//     Schema::dropIfExists('contacts');
//     dd('done');
// });
// Route::get('add',function(){
//     Schema::table('invoice_customers', function (Blueprint $table) {
//         $table->string('contact_person_mobile')->nullable();
//     });
//     Schema::table('invoice_suppliers', function (Blueprint $table) {
//         $table->string('contact_person_mobile')->nullable();
//     });
//     dd('Added');
// });



