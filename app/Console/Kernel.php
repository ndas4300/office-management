<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use MongoDB\Driver\Command;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        Commands\DomainEmail::class,
        Commands\SteallarData::class,
        Commands\AttendanceData::class,
        Commands\HolidaySms::class,
    ];
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('domain:email')->dailyAt('12.00');
        $schedule->command('attendance:raw')->everyMinute();
        $schedule->command('attendance:daily')->everyMinute();
        $schedule->command('holiday:sms')->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
