<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Hrm\Entities\Employee;
use Modules\OfficeSetup\Entities\CalendarEvent;

class HolidaySms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'holiday:sms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $employees = Employee::query()->whereHas('statuses', function ($q) {
            $q->where('status_id', 1);
        })->get();
        foreach ($employees as $key => $employee) {
            $calender_code_id = $employee->officials->calender_code_id;
            $holiday = CalendarEvent::query()
                ->where('calendar_id', $calender_code_id)
                ->whereDate('start', now()->addDay(1)->format('Y-m-d'))
                ->where('is_holiday', 1)
                ->where('is_active', 1)
                ->first();
            // dd($holiday != null);

            if ($holiday != null) {
                $url = "https://sms.solutionsclan.com/api/sms/send";
                $data = [
                    "apiKey" => "A0001234bd0dd58-97e5-4f67-afb1-1f0e5e83d835",
                    "contactNumbers" => $employee->employeeAddresses->pr_phone_one,
                    "senderId" => "WebPointLtd",
                    "textBody" => "Dear Employees, Tomorrow is.$holiday->name. in office. "
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                $response = curl_exec($ch);
                curl_close($ch);
                $holiday['sms_sent']=1;
                $holiday->save();
            }

            if ($employee->dob->format('m-d') == now()->format('m-d')) {
                $url = "https://sms.solutionsclan.com/api/sms/send";
                $data = [
                    "apiKey" => "A0001234bd0dd58-97e5-4f67-afb1-1f0e5e83d835",
                    "contactNumbers" => $employee->employeeAddresses->pr_phone_one,
                    "senderId" => "WebPointLtd",
                    "textBody" => "Lorem Ipsum Birthday"
                ];

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                $response = curl_exec($ch);
                curl_close($ch);
            }
        }

    }
}
