<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Hrm\Entities\Attendance;
use Modules\Hrm\Entities\Employee;
use Modules\Hrm\Entities\Leave;
use Modules\Hrm\Entities\RawAttendance;
use Modules\OfficeSetup\Entities\CalendarEvent;

class AttendanceData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'attendance:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Daily Attendances';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $today = Carbon::today();
        //$dayOfMonth = cal_days_in_month(null,'4',2022);
        $employees = Employee::whereHas('statuses', function ($q) {
            $q->where('status_id', 1);
        })->whereHas('officials', function ($q) use($today) {
            $q->whereDate('joining_date', '<=',$today);
        })->get();

        foreach ($employees as $employee) {
            $rawData = RawAttendance::query()
                ->where('access_date', $today->format('Y-m-d'))
                ->where('registration_id', $employee->employee_no)
                ->get();

            //dd($rawData->count());

            if ($rawData->isEmpty()) { // record not exists in raw database
                // Checking for leave if any
                // dd('empty');
                $min = null;
                $max = null;
                $leave = Leave::query()
                    ->where('employee_id', $employee->id)
                    ->where('leave_from', '<=', $today->format('Y-m-d'))
                    ->where('leave_to', '>=', $today->format('Y-m-d'))
                    ->exists();

                // Checking of holiday if any
                $calendarId = $employee->officials->calendar_id;
                $holiday = CalendarEvent::query()
                    ->where('calendar_id', $calendarId)
                    ->where('start', '<=', $today->format('Y-m-d'))
                    ->where('end', '>=', $today->format('Y-m-d'))
                    ->where('is_holiday', 1)
                    ->exists();

                // Weekly Off
                foreach ($employee->days as $key => $weeklyOff) {
                    if ($weeklyOff = $weeklyOff->id == $today->format('N')) {
                        break;
                    }
                }

                if ($holiday) {
                    $attendanceStatus = '5';
                } elseif ($leave) {
                    $attendanceStatus = '7';
                } elseif ($weeklyOff == true) {
                    $attendanceStatus = '6';
                } else {
                    $attendanceStatus = '2';
                }
            } else {
                // dd('not empty');
                $min = $rawData->min('access_time')->format('H:i:s');
                $max = $rawData->max('access_time')->format('H:i:s');

                $shift = $employee->officials->first()->shift;
                $shiftIn =  Carbon::parse($shift->in)->addMinutes($shift->grace)->format('H:i:s');
                $shiftOut = Carbon::parse($shift->out)->format('H:i:s');

                if($min >= $shiftIn && $max <= $shiftOut){
                    $attendanceStatus = '8';
                }elseif ($min <= $shiftIn && $max <= $shiftOut) {
                    $attendanceStatus = '4';
                } elseif ($min <= $shiftIn) {
                    $attendanceStatus = '1';
                } elseif ($min > $shiftIn) {
                    $attendanceStatus = '3';
                }
            }

            // $min = $rawData->min('access_time') ?? '00.00';
            // $max = $rawData->max('access_time') ?? '00.00';
            // // dd($max);
            // $shift = $employee->officials->first()->shift;
            // $shiftIn =  Carbon::parse($shift->in)->addMinutes($shift->grace)->format('h:i');
            // $shiftOut = $shift->out;

            // // Weekly Off
            // foreach ($employee->days as $key => $weeklyOff) {
            //     if ($weeklyOff = $weeklyOff->id == $today->format('N')) {
            //         break;
            //     }
            // }
            // // Holiday
            // $calendarId = $employee->officials->calendar_id;
            // $holiday = CalendarEvent::query()
            //     ->where('calendar_id', $calendarId)
            //     ->where('start', '<=', $today->format('Y-m-d'))
            //     ->where('end', '>=', $today->format('Y-m-d'))
            //     ->where('is_holiday', 1)
            //     ->exists();

            // // Leave Management
            // $leave = Leave::query()
            //     ->where('employee_id', $employee->id)
            //     ->where('leave_from', '<=', $today->format('Y-m-d'))
            //     ->where('leave_to', '>=', $today->format('Y-m-d'))
            //     ->exists();

            // // Attendance Status
            // if ($leave) {
            //     $attendanceStatus = '7';
            // } elseif ($min == '00.00') {
            //     $attendanceStatus = '2';
            // } elseif ($min <= $shiftIn && $max <= $shiftOut) {
            //     $attendanceStatus = '4';
            // } elseif ($min > $shiftIn) {
            //     $attendanceStatus = '3';
            // } elseif ($min <= $shiftIn) {
            //     $attendanceStatus = '1';
            // } elseif ($holiday) {
            //     $attendanceStatus = 5;
            // } elseif ($weeklyOff == true) {
            //     $attendanceStatus = 6;
            // }

            // $attendanceWeeklyOffs = DB::table('employee_weekly_off')->where('employee_id', $employee->id)->get();
            // foreach ($attendanceWeeklyOffs as $attendanceWeeklyOff) {
            //     AttendanceWeeklyOff::create([
            //         'day_id' => $attendanceWeeklyOff->day_id,
            //         'employee_id' => $attendanceWeeklyOff->employee_id,
            //     ]);
            // }

            $leaveCategory = Leave::query()
                ->where('employee_id', $employee->id)
                ->where('leave_from', '<=', $today->format('Y-m-d'))
                ->where('leave_to', '>=', $today->format('Y-m-d'))
                ->pluck('leave_category_id');

            $data = [
                'employee_id' => $employee->id,
                'date' => $today->format('Y-m-d'),
                'in_time' => $min,
                'out_time' => $max,
                'department_id' => $employee->officials->first()->department_id,
                'designation_id' => $employee->officials->first()->designation_id,
                'leave_category_id' => $leaveCategory,
                'attendance_status_id' => $attendanceStatus,
                'weekly_off_id' => $employee->officials->first()->weekly_off_id,
            ];

            $attendanceExists = Attendance::query()
                ->where('employee_id', $employee->id)
                ->where('date', $today->format('Y-m-d'))
                ->exists();

            if ($attendanceExists) {
                $attendance = Attendance::query()
                    ->where('employee_id', $employee->id)
                    ->where('date', $today->format('Y-m-d'))
                    ->first();

                $attendance->update($data);
            } else {
                Attendance::create($data);
            }
        }
    }
}
